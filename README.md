# README #


### What is this repository for? ###

Это iOS приложение-клиент для международной биржи криптовалюты Exmo.me.
Для полноценного пользования необходим аккаунт.

demo:

api key: K-87d5155bb723fc2e56c38d59cfdf7a7922693bde

secret: S-1d8265426d5274ad4aca592ca9d0b35f9b1e4dcb

[CryptoExmoProject / QR-CODE DEMO AKK](qr.jpg).

В дальнейшем возможно нетрудозатратное масштабирование - покупка/продажа на крупнейших биржах.

### Проблема с иконками криптовалют ###

Когда происходят большие скачки на рынке, на сайте, откуда скачиваются картинки, ставят ддос защиту, поэтому в такие моменты иконки не загружаются!

### How do I get set up? ###

Для авторизации доступно 2 режима:

1 - Ручной режим. Ввод 42-значного api_key и secret_key.

2 - Автоматический режим. Сканирование QR-кода в личном кабинете биржи.

### Hierarchy ###

Посмотреть видео превью можно здесь:

[CryptoExmoProject / introVideo.mp4](introVideo.mp4).

Посмотреть диаграмму проекта можно здесь:

[CryptoExmoProject / projectDiagram.pdf](projectDiagram.jpg).

fullsize:

[CryptoExmoProject / bigProjectDiagram.pdf](bigProjectDiagram.pdf).
