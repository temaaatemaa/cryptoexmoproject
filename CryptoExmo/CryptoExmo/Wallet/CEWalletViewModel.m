//
//  CEWalletViewModel.m
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 13.12.2017.
//  Copyright © 2017 Artem Zabludovsky. All rights reserved.
//


#import "CEWalletViewModel.h"
#import "CEExmoNetwork.h"
#import "CENetworkService.h"
#import "AppDelegate.h"
#import "CEWalletTableHeaderViewCell.h"
#import "CEMarketAndWalletTableViewCell.h"
#import "CEAuthorisationViewController.h"
#import "CEAuthorisationViewControllerProtocol.h"
#import "UIColor+CEColor.h"


static NSString *const CEHeaderReuseID = @"CEHeaderReusID";
static NSString *const CECellReuseID = @"CECELLReuseID";
static NSString *const CETextWhenTableIsEmpty = @"No wallet data is currently available. \n\nMake sure you log on. \n If you have money, \nplease pull down to refresh the list.";


@interface CEWalletViewModel()<NSFetchedResultsControllerDelegate,CEExmoNetworkDelegate,
    CEAuthorisationViewControllerDelegate>

@property (nonatomic, strong) NSPersistentContainer *container;
@property (nonatomic, strong) CEExmoNetwork *exmo;

@property (nonatomic, strong) NSMutableDictionary *dictOfValueOfEachCurrencyInDollar;
@property (nonatomic, strong) NSMutableDictionary *dictOfRealBalance;

@property (nonatomic, copy) NSArray *realBalanceArray;

@property (nonatomic, strong) NSLock *lock;

@end


@implementation CEWalletViewModel


#pragma mark - Lifecycle

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        _exmo = [[CEExmoNetwork alloc]init];
        _exmo.delegate = self;
    }
    return self;
}


#pragma mark - Custom Accessors

- (void)setDelegate:(id<CEWalletViewModelDelegate>)delegate
{
    _delegate = delegate;
    [self setup];
}

- (NSPersistentContainer *)container
{
    UIApplication *app = [UIApplication sharedApplication];
    NSPersistentContainer *container = ((AppDelegate *)app.delegate).persistentContainer;
    return container;
}


#pragma mark - Public

- (void)showAuthController
{
    CEAuthorisationViewController *authVC = [CEAuthorisationViewController new];
    authVC.delegate = self;
    [self.delegate shouldPresent:self QRCodeViewController:authVC];
}

- (void)getArrayOfCorrenciesWhichNonNul
{
    NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
    if ([userDef objectForKey:@"api_key"])
    {
        [self.exmo getUserInfo];
    }
}

- (UITableViewCell *)getCellForTableView:(UITableView *)tableView forIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
    {
        return [self prepareHeaderCellFor:tableView];
    }
    CEMarketAndWalletTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CECellReuseID];
    NSString *currencyName = self.realBalanceArray[indexPath.row-1];
    if ([currencyName isEqualToString:@"RUB"])
    {
        cell = [self prepareRUBCell:cell];
    }
    else if ([currencyName isEqualToString:@"USD"])
    {
        cell = [self prepareUSDCell:cell];
    }
    else
    {
        cell = [self prepareCell:cell ForCurrency:currencyName];
    }
    cell.volumeLabel.text = self.dictOfRealBalance[currencyName];
    return cell;
}

- (NSUInteger)getNumberRowsInTableView:(UITableView *)tableView
{
    return [self.realBalanceArray count]+1;
}

- (NSUInteger)numberOfSectionInTableView:(UITableView *)tableView
{
    if ([self.realBalanceArray count])
    {
        tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        tableView.backgroundView = nil;
    }
    else
    {
        UILabel *messageLabel = [[UILabel alloc]
                initWithFrame:CGRectMake(0, 0, CGRectGetWidth(tableView.bounds), CGRectGetHeight(tableView.bounds))];
        messageLabel.text = CETextWhenTableIsEmpty;
        messageLabel.textColor = [UIColor ceFontColor];
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = NSTextAlignmentCenter;
        messageLabel.font = [UIFont systemFontOfSize:25 weight:UIFontWeightThin];
        [messageLabel sizeToFit];
        tableView.backgroundView = messageLabel;
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return 1;
}


#pragma mark - Private

- (void)setup
{
    NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
    if (![userDef objectForKey:@"api_key"])
    {
        [self showAuthController];
    }
    else
    {
        [self getArrayOfCorrenciesWhichNonNul];
    }
    _dictOfValueOfEachCurrencyInDollar = [NSMutableDictionary new];
}

- (CGFloat)getSumOfCurr
{
    for (NSString *key in self.realBalanceArray)
    {
        NSError *error;
        CGFloat amountOfCurr;
        CGFloat priceInDollar;
        if ([key isEqualToString:@"RUB"])
        {
            priceInDollar = 1/58.82;
        }
        else if ([key isEqualToString:@"USD"])
        {
            priceInDollar = 1;
        }else
        {
            NSFetchRequest *request = [Currencies fetchRequest];
            request.predicate = [NSPredicate predicateWithFormat:@"firstCurrencyShortName = %@ AND secondCurrencyShortName = %@",key,@"USD"];
            NSArray *matches = [self.container.viewContext executeFetchRequest:request error:&error];
            Currencies *curr = [matches lastObject];
            priceInDollar = [curr.sellPrice floatValue];
        }
        amountOfCurr = [self.dictOfRealBalance[key] floatValue];
        self.dictOfValueOfEachCurrencyInDollar[key] = [NSNumber numberWithFloat:amountOfCurr*priceInDollar];
    }
	
	CGFloat sum = 0;
    for (NSNumber *num in [self.dictOfValueOfEachCurrencyInDollar allValues])
    {
        sum+=num.floatValue;
    }
    return sum;
}

- (UIImage *)imageWithData:(NSData *)data forSize:(CGSize)size
{
    return [self imageWithImage:[UIImage imageWithData:data] forSize:size];
}

- (UIImage *)imageWithImage:(UIImage *)image forSize:(CGSize)size
{
    UIGraphicsBeginImageContextWithOptions(size, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (UITableViewCell *)prepareHeaderCellFor:(UITableView *)tableView
{
    CEWalletTableHeaderViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CEHeaderReuseID];
    cell.CETitleLable.text = @"Wallet";
    cell.CETotalLabel.text = [NSString stringWithFormat:@"Total %.2f $",[self getSumOfCurr]];
    cell.CEImageView.image = [UIImage imageNamed:@"exmo500"];
    return cell;
}

- (CEMarketAndWalletTableViewCell *)prepareRUBCell:(CEMarketAndWalletTableViewCell *)cell
{
    cell.nameLabel.text = @"Ruble (RUB)";
    cell.priceLabel.text = [NSString stringWithFormat:@"%.2f $",
                            [self.dictOfValueOfEachCurrencyInDollar[@"RUB"] floatValue]];
    cell.imageView.image = [self imageWithImage:[UIImage imageNamed:@"rub"] forSize:CGSizeMake(50, 50)];
    return cell;
}

- (CEMarketAndWalletTableViewCell *)prepareUSDCell:(CEMarketAndWalletTableViewCell *)cell
{
    cell.nameLabel.text = @"Dollar (USD)";
    NSString *usd = self.dictOfRealBalance[@"USD"];
    cell.priceLabel.text = [[usd substringToIndex:5] stringByAppendingString:@" $"];
    cell.imageView.image = [self imageWithImage:[UIImage imageNamed:@"usd"] forSize:CGSizeMake(50, 50)];
    return cell;
}

- (CEMarketAndWalletTableViewCell *)prepareCell:(CEMarketAndWalletTableViewCell *)cell ForCurrency:(NSString *)currencyName
{
    NSFetchRequest *request = [Currencies fetchRequest];
    request.predicate = [NSPredicate predicateWithFormat:@"firstCurrencyShortName = %@ ",currencyName];
    NSArray *matches = [self.container.viewContext executeFetchRequest:request error:nil];
    
    Currencies *curr = [matches lastObject];
    
    cell.nameLabel.text = curr.firstCurrencyName;
    cell.priceLabel.text = [NSString stringWithFormat:@"%.2f $",
                            [self.dictOfValueOfEachCurrencyInDollar[currencyName] floatValue]];
    //cell.imageView.image = [self imageWithData:curr.imageData forSize:CGSizeMake(50, 50)];
	cell.imageView.image = [self imageWithImage:[UIImage imageNamed:curr.firstCurrencyShortName.lowercaseString] forSize:CGSizeMake(50, 50)];
    return cell;
}


#pragma mark - CEAuthorisationViewControllerDelegate

- (void)authControllerDidClose
{
    [self getArrayOfCorrenciesWhichNonNul];
}


#pragma mark - CEExmoNetworkDelegate

- (void)userInfoDidRecived:(CEExmoNetwork *)exmo withData:(NSData *)data
{
	if (!data)
	{
		return;
	}
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    if ([dict objectForKey:@"error"])
    {
        NSLog(@"userInfoDidRecived: %@",dict[@"error"]);
        [self.delegate shouldPresent:self AlertWithMessage:dict[@"error"]];
    }
    self.dictOfRealBalance = [[NSMutableDictionary alloc]init];
    for (NSString *key in [dict[@"balances"] allKeys])
    {
        if (![dict[@"balances"][key] isEqualToString:@"0"])
        {
            [self.dictOfRealBalance setObject:dict[@"balances"][key] forKey:key];
        }
    }
    self.realBalanceArray = [self.dictOfRealBalance allKeys];
    [self.delegate dataDidReceived:self withBalance:[self.dictOfRealBalance copy] withArray:self.realBalanceArray];
}


@end
