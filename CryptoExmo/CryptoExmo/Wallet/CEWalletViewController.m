//
//  CEWalletViewController.m
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 11.12.2017.
//  Copyright © 2017 Artem Zabludovsky. All rights reserved.
//


#import "CEWalletViewController.h"
#import <Masonry.h>
#import "CEMarketAndWalletTableViewCell.h"
#import "CEWalletTableHeaderViewCell.h"
#import "CEWalletViewModel.h"
#import "UIColor+CEColor.h"


static NSString *const CEHeaderReuseID = @"CEHeaderReusID";
static NSString *const CECellReuseID = @"CECELLReuseID";

static NSString *const CEErrorForWrongApi = @"Error 40017: Wrong api key";
static NSString *const CEErrorForAuthorizationSignatureError = @"Error 40005: Authorization error, Incorrect signature";
static NSString *const CEErrorForAuthorizationKeyError = @"Error 40003: Authorization error, http header 'Key' not specified";

static CGFloat const CEHeightOfHeader = 150.f;
static CGFloat const CEHeightOfCells = 67.f;


@interface CEWalletViewController ()<UITableViewDataSource,UITableViewDelegate,CEWalletViewModelDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) CEWalletViewModel *walletVM;

@property (nonatomic, strong) NSTimer *timerForRefreshingAnimation;

@end


@implementation CEWalletViewController

#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor ceBackgroundColor];
    
    [self setupStatusBarBackground];
    
    [self setupTableView];
    
    [self setupRefreshControl];
    
    _walletVM = [CEWalletViewModel new];
    _walletVM.delegate = self;
    
    [self updateViewConstraints];
    [self setNeedsStatusBarAppearanceUpdate];
}

- (void)updateViewConstraints
{
    CGRect rectForStatusBar = [UIApplication sharedApplication].statusBarFrame;
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_top).with.offset(CGRectGetHeight(rectForStatusBar));
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.bottom.equalTo(self.view.mas_bottom);
    }];
    [super updateViewConstraints];
}


#pragma mark - Private

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)updateData
{
	self.timerForRefreshingAnimation = [NSTimer scheduledTimerWithTimeInterval:10 repeats:NO block:^(NSTimer * _Nonnull timer)
	{
		[self shouldPresent:nil AlertWithMessage:@"Can't update wallet data\nCheck your Internet connection"];
		[self.tableView.refreshControl endRefreshing];
	}];
    [self.walletVM getArrayOfCorrenciesWhichNonNul];
}

- (void)updateTableView
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
}

- (void)setupStatusBarBackground
{
    CGRect rectForStatusBar = [UIApplication sharedApplication].statusBarFrame;
    UIView *statusBG = [[UIView alloc]
                        initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(rectForStatusBar))];
    statusBG.backgroundColor = [UIColor ceBackgroundColor];
    [self.view addSubview:statusBG];
}

- (void)setupTableView
{
    _tableView = [[UITableView alloc]initWithFrame:CGRectNull style:UITableViewStylePlain];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.allowsSelection = NO;
    _tableView.backgroundColor = [UIColor ceBackgroundColor];
    [_tableView registerClass:[CEMarketAndWalletTableViewCell class] forCellReuseIdentifier:CECellReuseID];
    [_tableView registerClass:[CEWalletTableHeaderViewCell class] forCellReuseIdentifier:CEHeaderReuseID];
    _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.view addSubview:_tableView];
}

- (void)setupRefreshControl
{
    _tableView.refreshControl = [[UIRefreshControl alloc] init];
    _tableView.refreshControl.backgroundColor = [UIColor ceRefreshColor];
    _tableView.refreshControl.tintColor = [UIColor whiteColor];
    [_tableView.refreshControl addTarget:self
                                  action:@selector(updateData)
                        forControlEvents:UIControlEventValueChanged];
}


#pragma mark - TableViewDelegate

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView
                 cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    return [self.walletVM getCellForTableView:tableView forIndexPath:indexPath];
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.walletVM getNumberRowsInTableView:tableView];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
    {
        return CEHeightOfHeader;
    }
    return CEHeightOfCells;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.walletVM numberOfSectionInTableView:tableView];
}


#pragma mark - CEWalletViewModelDelegate

- (void)dataDidReceived:(CEWalletViewModel *)walletVM withBalance:(NSDictionary *)dictionaryOfBalance
                                                        withArray:(NSArray *)arrayOfBalance
{
    dispatch_async(dispatch_get_main_queue(), ^{
		[self.timerForRefreshingAnimation invalidate];
        [self.tableView.refreshControl endRefreshing];
        if (dictionaryOfBalance)
        {
            if ([self.tableView indexPathsForVisibleRows].count == 1)//1 - headerCell
            {
                [self.tableView reloadData];
                return;
            }
			if ([self.tableView numberOfRowsInSection:0]-1 != arrayOfBalance.count)//1 - headerCell
			{
				[self.tableView reloadData];
				return;
			}
            [self.tableView performBatchUpdates:^{
                for (int i = 1; i <= dictionaryOfBalance.count; i++)
                {
                    NSIndexPath *path = [NSIndexPath indexPathForRow:i inSection:0];
                    if ([self.tableView cellForRowAtIndexPath:path])
                    {
                        [self.tableView reloadRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationRight];
                    }
                }
            } completion:^(BOOL finished) {
                [self.tableView reloadData];
            }];
        }
        else
        {
            [self.tableView reloadData];
        }
    });
}

- (void)shouldPresent:(CEWalletViewModel *)walletVM QRCodeViewController:(UIViewController *)vc
{
    [self presentViewController:vc animated:YES completion:nil];
}

- (void)shouldPresent:(CEWalletViewModel *)walletVM AlertWithMessage:(NSString *)message
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error!" message:message
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:okButton];
    if ([message isEqualToString:CEErrorForWrongApi])
    {
        [self addLoginButtonToAlertController:alertController];
    }
    if ([message isEqualToString:CEErrorForAuthorizationSignatureError])
    {
        [self addLoginButtonToAlertController:alertController];
    }
    if ([message isEqualToString:CEErrorForAuthorizationKeyError])
    {
        [self addLoginButtonToAlertController:alertController];
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertController animated:YES completion:nil];
    });
}

- (void)addLoginButtonToAlertController:(UIAlertController *)alertController
{
    UIAlertAction *authButton = [UIAlertAction actionWithTitle:@"Log in" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * _Nonnull action) {
        [self.walletVM showAuthController];
    }];
    [alertController addAction:authButton];
}

@end
