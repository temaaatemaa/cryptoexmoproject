//
//  CEWalletViewModelProtocol.h
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 13.12.2017.
//  Copyright © 2017 Artem Zabludovsky. All rights reserved.
//


#ifndef CEWalletViewModelProtocol_h
#define CEWalletViewModelProtocol_h


@class CEWalletViewModel;


@protocol CEWalletViewModelDelegate

/** Уведомление об обновлении данных баланса
 @param walletVM CEWalletViewModel
 @param dictionaryOfBalance Данные о балансе пользователя
 @param arrayOfBalance Массив валют
 
 вызывается при - (void)getArrayOfCorrenciesWhichNonNul */
- (void)dataDidReceived:(CEWalletViewModel *)walletVM withBalance:(NSDictionary *)dictionaryOfBalance
                                                        withArray:(NSArray *)arrayOfBalance;

/** Уведомление о требовании показать контроллер авторизации
 @param walletVM CEWalletViewModel
 @param vc Контроллер авторизации
 
 вызывается при - (void)showAuthController; */
- (void)shouldPresent:(CEWalletViewModel *)walletVM QRCodeViewController:(UIViewController *)vc;

/** Уведомление об требовании показать алерт
 @param walletVM CEWalletViewModel
 @param message Алерт сообщение */
- (void)shouldPresent:(CEWalletViewModel *)walletVM AlertWithMessage:(NSString *)message;

@end

#endif /* CEWalletViewModelProtocol_h */
