//
//  CEWalletViewModel.h
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 13.12.2017.
//  Copyright © 2017 Artem Zabludovsky. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "CEWalletViewModelProtocol.h"


@interface CEWalletViewModel : NSObject

/** Делегат CEWalletViewModelDelegate
 @abstract Используется для уведомлений о новых данных или о том
    чтобы произвести определенные действия*/
@property (nonatomic, weak) id<CEWalletViewModelDelegate> delegate;

/** Метод, подготавливающий ячейку для TableView.
 @param tableView Используемая TableView
 @param indexPath ячейка для данного indexPath*/
- (UITableViewCell *)getCellForTableView:(UITableView *)tableView forIndexPath:(NSIndexPath *)indexPath;

/** Количество ячеек в TableView
 @param tableView Используемая TableView */
- (NSUInteger)getNumberRowsInTableView:(UITableView *)tableView;

/** Количество секций в TableView
 @param tableView Используемая TableView */
- (NSUInteger)numberOfSectionInTableView:(UITableView *)tableView;

/** Получить массив баланса валют пользователя, который не равен 0
 Делегат:
 - (void)dataDidReceived:(CEWalletViewModel *)walletVM withBalance:(NSDictionary *)dictionaryOfBalance
                                                        withArray:(NSArray *)arrayOfBalance;*/
- (void)getArrayOfCorrenciesWhichNonNul;

/** Метод вызывающий present CEAuthorisationViewController в делегате
 Делегат:
 - (void)shouldPresent:(CEWalletViewModel *)walletVM QRCodeViewController:(UIViewController *)vc;*/
- (void)showAuthController;

@end
