//
//  WalletTableHeaderView.h
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 12.12.2017.
//  Copyright © 2017 Artem Zabludovsky. All rights reserved.
//


#import <UIKit/UIKit.h>


@interface CEWalletTableHeaderViewCell : UITableViewCell

@property (nonatomic, strong) UILabel *CETitleLable;
@property (nonatomic, strong) UIImageView *CEImageView;
@property (nonatomic, strong) UILabel *CETotalLabel;

@end
