//
//  CEMarketViewModel.m
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 12.12.2017.
//  Copyright © 2017 Artem Zabludovsky. All rights reserved.
//


#import "CEMarketViewModel.h"
#import "CENetworkService.h"
#import "AppDelegate.h"
#import "CEExmoNetwork.h"
#import "Currencies+CoreDataClass.h"
#import "CEExmoNetworkProtocol.h"
#import "UIColor+CEColor.h"
#import "CESignsOfCurrencies.h"


static NSTimeInterval const CETimeInterfalForUpdatingMarketData = 10;
static NSTimeInterval const CETimeInterfalForUpdatingGlobalCapitalization = 20;
static CGSize const CESizeOfCryptoIconInCell = {50,50};
static NSString *const CEHeaderReuseID = @"CEHeaderReusID";
static NSString *const CECellReuseID = @"CECELLReuseID";


@interface CEMarketViewModel()<NSFetchedResultsControllerDelegate,CEExmoNetworkDelegate,CENetworkServiceOutputDelegate>

@property (nonatomic, strong) CENetworkService *CENetworkService;
@property (nonatomic, strong) NSPersistentContainer *container;
@property (nonatomic, strong) CEExmoNetwork *exmo;

@property (nonatomic, strong) NSTimer *timerForUpdatingMarketData;
@property (nonatomic, strong) NSTimer *timerForUpdatingGlobalCapitalization;

@property (nonatomic, copy) NSArray *currenciesArray;
@property (nonatomic, assign) NSNumber *globalCapitalization;

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultController;
@end


@implementation CEMarketViewModel


#pragma mark - Lifecycle

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        _CENetworkService = [[CENetworkService alloc]init];
        
        _exmo = [[CEExmoNetwork alloc]init];
        _exmo.delegate = self;
        [_exmo getInfoForMarket];
        
        [self setup];
    }
    return self;
}


#pragma mark - Custom Accessors

- (NSPersistentContainer *)container
{
    UIApplication *app = [UIApplication sharedApplication];
    NSPersistentContainer *container = ((AppDelegate *)app.delegate).persistentContainer;
    return container;
}

- (NSFetchedResultsController *)fetchedResultController
{
    if (!_fetchedResultController) {
        NSFetchRequest *request = [Currencies fetchRequest];
        request.sortDescriptors = @[
									[[NSSortDescriptor alloc]initWithKey:@"secondCurrencyShortName" ascending:NO],
									[[NSSortDescriptor alloc]initWithKey:@"volume24ch" ascending:NO]];
        _fetchedResultController = [[NSFetchedResultsController alloc]initWithFetchRequest:request
                                                        managedObjectContext:self.container.viewContext
                                                          sectionNameKeyPath:nil
                                                                   cacheName:@"CurrencyCach"];
    }
    return _fetchedResultController;
}


#pragma mark - Public

- (CEMarketTableHeaderViewCell *)prepareHeaderCellForTableView:(UITableView *)tableView
{
    CEMarketTableHeaderViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CEHeaderReuseID];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.CETitleLable.text = @"Market";
    NSUInteger integer = self.globalCapitalization.doubleValue/10000000.f;
    CGFloat flot = (double)integer/100.f;
    NSString *capitol = [NSString stringWithFormat:@"Total cap %.2f",flot];
    cell.CELowerLabel.text =  [capitol stringByAppendingString:@" BLN $ "];
    cell.CEImageView.image = [UIImage imageNamed:@"exmo500"];
    return cell;
}

- (CEMarketAndWalletTableViewCell *)prepareCellForIndexPath:(NSIndexPath *)indexPath forTableView:(UITableView *)tableView
{
    CEMarketAndWalletTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CECellReuseID];
    Currencies *curr = self.currenciesArray[indexPath.row-1];
	
	NSString *changedName = [curr.firstCurrencyName stringByReplacingOccurrencesOfString:@"(" withString:@""];
	NSString *changedNameWithoutLastCharacter = [changedName stringByReplacingOccurrencesOfString:@")" withString:@""];
	NSString *changedNameWithSlash = [changedNameWithoutLastCharacter stringByAppendingString:@"/"];
	NSString *nameLabel = [changedNameWithSlash stringByAppendingString:curr.secondCurrencyShortName];
    cell.nameLabel.text = nameLabel;
    cell.priceLabel.text = [NSString stringWithFormat:@"%.2f %@",curr.buyPrice.floatValue,
							[CESignsOfCurrencies changeShortNameForSign: curr.secondCurrencyShortName]];
	
    cell.volumeLabel.text = [NSString stringWithFormat:@"%d %@",curr.volume24ch,
							 [CESignsOfCurrencies changeShortNameForSign: curr.secondCurrencyShortName]];
	
    cell.percetageLabel.text = [NSString stringWithFormat:@"%.2f %%",curr.persentage];
	cell.imageView.image = [self imageWithImage:[UIImage imageNamed:curr.firstCurrencyShortName.lowercaseString] forSize:CESizeOfCryptoIconInCell];
	cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    CEMarketAndWalletTableViewCell *coloredCell = [self setupColorForCurrency:curr inCell:cell];
    return coloredCell;
}

- (CEMarketAndWalletTableViewCell *)setupColorForCurrency:(Currencies *)curr inCell:(CEMarketAndWalletTableViewCell *)cell
{
    if (curr.persentage >= 0)
    {
        cell.percetageLabel.backgroundColor = [UIColor ceGreenColorForPersentageInMarketAndWalletCell];
    }
    else
    {
        cell.percetageLabel.backgroundColor = [UIColor ceRedColorInMarketAndWalletCell];
    }
    if (curr.isPriceIncrease)
    {
        cell.priceLabel.textColor = [UIColor ceGreenColorForPriceInMarketAndWalletCell];
    }
    if (curr.isPriceDecrease)
    {
        cell.priceLabel.textColor = [UIColor ceRedColorInMarketAndWalletCell];
    }
    return cell;
}


#pragma mark - Private

- (void)setup
{
    _timerForUpdatingMarketData = [NSTimer scheduledTimerWithTimeInterval:CETimeInterfalForUpdatingMarketData
                                                                  repeats:YES
                                                                    block:^(NSTimer * _Nonnull timer) {
        [_exmo getInfoForMarket];
    }];
    [_timerForUpdatingMarketData fire];
    
    _timerForUpdatingGlobalCapitalization = [NSTimer scheduledTimerWithTimeInterval:CETimeInterfalForUpdatingGlobalCapitalization
                                                                            repeats:YES
                                                                              block:^(NSTimer * _Nonnull timer) {
        [_exmo getGlobalCapitalization];
    }];
    [_timerForUpdatingGlobalCapitalization fire];
    //[self downloadIcons];
}

- (void)downloadIcons
{
    //[self.CENetworkService downloadIconsForCurrencies];
}

- (UIImage *)imageWithData:(NSData *)data forSize:(CGSize)size
{
    UIImage *img = [UIImage imageWithData:data];
    UIGraphicsBeginImageContextWithOptions(size, NO, 0.0);
    [img drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (void)fetchCurrencyArrayFromCoreData
{
    [self.fetchedResultController performFetch:nil];
    NSArray *currenciesArray = self.fetchedResultController.fetchedObjects;
    [self.delegate marketDataDidReceived:self withCurrencies:currenciesArray];
    self.currenciesArray = currenciesArray;
    
    Currencies *curr = [currenciesArray lastObject];
    if (!curr.imageData)
    {
        [self downloadIcons];
    }
}

- (UIImage *)imageWithImage:(UIImage *)image forSize:(CGSize)size
{
	UIGraphicsBeginImageContextWithOptions(size, NO, 0.0);
	[image drawInRect:CGRectMake(0, 0, size.width, size.height)];
	UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	return newImage;
}


#pragma mark - FetchResultControllerDelegate

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self fetchCurrencyArrayFromCoreData];
    });
}


#pragma mark - CEExmoNetworkDelegate

- (void)globalCapitalizationDidLoad:(CEExmoNetwork *)exmo withData:(NSData *)data
{
	if (!data)
	{
		return;
	}
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    NSNumber *capitalization = dict[@"total_market_cap_usd"];
    self.globalCapitalization = capitalization;
    [self.delegate globalCapitalizationDidReceived:self withCapitalization:capitalization];
}

-(void)iconDidDownload:(CENetworkService *)CENetworkService forCurrency:(Currencies *)currency
{
	[self.delegate marketDataDidReceived:self withCurrencies:self.currenciesArray];
}

@end
