//
//  CEMarketTableHeaderViewCell.m
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 13.12.2017.
//  Copyright © 2017 Artem Zabludovsky. All rights reserved.
//


#import "CEMarketTableHeaderViewCell.h"
#import <Masonry.h>
#import "UIFont+CEFont.h"
#import "UIColor+CEColor.h"


static const CGFloat CEElementsOffset = 7.f;
static const CGFloat CETitleHeight = 80.f;


@implementation CEMarketTableHeaderViewCell


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        self.backgroundColor = [UIColor ceBackgroundColor];
        
        _CETitleLable = [UILabel new];
        _CEImageView = [UIImageView new];
        _CELowerLabel = [UILabel new];
        
        _CELowerLabel.textAlignment = NSTextAlignmentRight;
        
        _CETitleLable.font = [UIFont ceFontForMarketAndWalletTitle];
        _CELowerLabel.font = [UIFont ceFontForGlobalCapitalizationAndTotal];
        
        _CETitleLable.textColor = [UIColor ceFontColorForMarketAndWalletCell];
        _CELowerLabel.textColor = [UIColor ceFontColorForMarketAndWalletCell];
        
        [self.contentView addSubview:_CETitleLable];
        [self.contentView addSubview:_CEImageView];
        [self.contentView addSubview:_CELowerLabel];
        
        [self updateConstraints];
    }
    return self;
}

- (void)updateConstraints
{
    [_CETitleLable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top).with.offset(3*CEElementsOffset);
        make.left.equalTo(self.contentView.mas_left).with.offset(CEElementsOffset);
        make.height.equalTo(@(CETitleHeight));
    }];
    
    [_CEImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView.mas_right).with.offset(-CEElementsOffset);
        make.top.equalTo(self.contentView.mas_top).with.offset(2*CEElementsOffset);
        make.width.equalTo(_CEImageView.mas_height);
        make.height.equalTo(_CETitleLable.mas_height);
    }];
    
    [_CELowerLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.contentView.mas_bottom).with.offset(-5);
        make.right.equalTo(self.contentView.mas_right).with.offset(-5);;
        make.left.equalTo(self.contentView.mas_left).with.offset(CEElementsOffset+4);
        make.top.equalTo(_CETitleLable.mas_bottom);
    }];
    [super updateConstraints];
}

@end
