//
//  CEMarketViewModel.h
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 12.12.2017.
//  Copyright © 2017 Artem Zabludovsky. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "CEMarketViewModelProtocol.h"
#import "CEMarketTableHeaderViewCell.h"
#import "CEMarketAndWalletTableViewCell.h"
#import "Currencies+CoreDataClass.h"

@interface CEMarketViewModel : NSObject

/** Делегат CEMarketViewModel
 @abstract Используется для уведомлений новых данных
 */
@property (nonatomic, weak) id<CEMarketViewModelDelegate> delegate;

/** Создание ячейки в для данной таблице и данного indexpath
 @param indexPath index path для требуемой cell
 @param tableView TableView для которой требуется cell
 @return CEMarketAndWalletTableViewCell*/
- (CEMarketAndWalletTableViewCell *)prepareCellForIndexPath:(NSIndexPath *)indexPath forTableView:(UITableView *)tableView;

/** Создание вверхней ячейки (header типа)
 @param tableView TableView для которой требуется cell
 @return CEMarketTableHeaderViewCell*/
- (CEMarketTableHeaderViewCell *)prepareHeaderCellForTableView:(UITableView *)tableView;

@end
