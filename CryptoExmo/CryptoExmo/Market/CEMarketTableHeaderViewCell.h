//
//  CEMarketTableHeaderViewCell.h
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 13.12.2017.
//  Copyright © 2017 Artem Zabludovsky. All rights reserved.
//


#import <UIKit/UIKit.h>


@interface CEMarketTableHeaderViewCell : UITableViewCell


@property (nonatomic, strong) UILabel *CETitleLable;
@property (nonatomic, strong) UIImageView *CEImageView;
@property (nonatomic, strong) UILabel *CELowerLabel;


@end
