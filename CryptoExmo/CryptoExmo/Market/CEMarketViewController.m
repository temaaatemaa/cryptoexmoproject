//
//  CEMarketViewController.m
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 11.12.2017.
//  Copyright © 2017 Artem Zabludovsky. All rights reserved.
//


#import "CEMarketViewController.h"
#import "AppDelegate.h"
#import <Masonry.h>
#import "CEExmoNetwork.h"
#import "Currencies+CoreDataClass.h"
#import "CEMarketAndWalletTableViewCell.h"
#import "CEMarketViewModel.h"
#import "CEMarketTableHeaderViewCell.h"
#import "UIColor+CEColor.h"
#import "CECurrencyViewController.h"


static CGFloat const CEHeightOfHeader = 150.f;
static CGFloat const CEHeightOfCells = 70.f;
static NSString *const CEHeaderReuseID = @"CEHeaderReusID";
static NSString *const CECellReuseID = @"CECELLReuseID";
static CGSize const CESizeOfAnimatedIconWhenLoading = {50,50};



@interface CEMarketViewController ()<UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate, CEMarketViewModelDelegate, UINavigationControllerDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, copy) NSArray *currenciesArray;
@property (nonatomic,assign) BOOL isTableViewDragging;
@property (nonatomic, strong) CEMarketViewModel *marketVM;

@end


@implementation CEMarketViewController


#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor ceBackgroundColor];
    self.navigationController.navigationBar.hidden = YES;
    self.title = @"Market";
    
    [self setupStatusBarBackground];
    
    [self setupTableView];
    
    self.marketVM = [[CEMarketViewModel alloc]init];
    self.marketVM.delegate = self;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    [self setNeedsStatusBarAppearanceUpdate];
}

- (void)updateViewConstraints
{
    CGRect rectForStatusBar = [UIApplication sharedApplication].statusBarFrame;
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_top).with.offset(CGRectGetHeight(rectForStatusBar));
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.bottom.equalTo(self.view.mas_bottom);
    }];
    [super updateViewConstraints];
}


#pragma mark - Private

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)updateTableView
{
    if (!self.isTableViewDragging)
    {
        [self.tableView reloadData];
    }
}

- (void)setupStatusBarBackground
{
    CGRect rectForStatusBar = [UIApplication sharedApplication].statusBarFrame;
    UIView *statusBG = [[UIView alloc] initWithFrame:
                        CGRectMake(0, 0, CGRectGetWidth(self.view.frame),CGRectGetHeight(rectForStatusBar))];
    statusBG.backgroundColor = [UIColor ceBackgroundColor];
    [self.view addSubview:statusBG];
}

- (void)setupTableView
{
    self.isTableViewDragging = NO;
    self.tableView = [[UITableView alloc]initWithFrame:CGRectNull style:UITableViewStylePlain];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    [self.tableView registerClass:[CEMarketAndWalletTableViewCell class] forCellReuseIdentifier:CECellReuseID];
    [self.tableView registerClass:[CEMarketTableHeaderViewCell class] forCellReuseIdentifier:CEHeaderReuseID];
    [self.view addSubview:self.tableView];
}


#pragma mark - TableViewDelegate

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
    {
        return [self.marketVM prepareHeaderCellForTableView:tableView];
    }
    return [self.marketVM prepareCellForIndexPath:indexPath forTableView:tableView];
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.currenciesArray count]+1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return (indexPath.row == 0) ? CEHeightOfHeader : CEHeightOfCells;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ([self.currenciesArray count])
    {
        tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        tableView.backgroundView = nil;
        return 1;
        
    }
    else
    {
        UIImage *image = [UIImage imageNamed:@"exmoWhite"];
        image = [self imageWithImage:image forSize:CESizeOfAnimatedIconWhenLoading];
       
        UIImageView *imageView = [[UIImageView alloc]initWithImage:image];
        UIView *view = [UIView new];
        [view addSubview:imageView];
        tableView.backgroundView = view;
        tableView.backgroundColor = [UIColor ceBackgroundColor];
        [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.equalTo(@100);
            make.center.equalTo(view);
        }];
        [self runSpinAnimationOnView:imageView duration:1 rotations:5 repeat:5];
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return 1;
}

- (void) runSpinAnimationOnView:(UIView*)view duration:(CGFloat)duration rotations:(CGFloat)rotations repeat:(float)repeat {
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 /* full rotation*/ * rotations * duration ];
    rotationAnimation.duration = duration;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = repeat ? HUGE_VALF : 0;
    
    [view.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}

- (UIImage *)imageWithImage:(UIImage *)image forSize:(CGSize)size
{
    UIGraphicsBeginImageContextWithOptions(size, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
    {
        return;
    }
    Currencies *currency = self.currenciesArray[indexPath.row-1];
    CECurrencyViewController *currencyViewController = [CECurrencyViewController new];
    NSArray *pairArray = @[
                           currency.firstCurrencyShortName,
                           currency.secondCurrencyShortName
                           ];
    currencyViewController.pair = pairArray;
    [self.navigationController pushViewController:currencyViewController animated:YES];
    CEMarketAndWalletTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.selected = NO;
}


#pragma mark - ScrollViewDelegate

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    self.isTableViewDragging = YES;
}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
{
    self.isTableViewDragging = YES;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    self.isTableViewDragging = NO;
    [self updateTableView];
}


#pragma mark - CEMarketViewModelDelegate

- (void)marketDataDidReceived:(CEMarketViewModel *)marketVM withCurrencies:(NSArray *)arrWithCurrencies
{
    self.currenciesArray = arrWithCurrencies;
    [self updateTableView];
}

- (void)globalCapitalizationDidReceived:(CEMarketViewModel *)marketVM withCapitalization:(NSNumber *)globalCap
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.currenciesArray)
        {
            [self updateTableView];
        }
    });
}

@end
