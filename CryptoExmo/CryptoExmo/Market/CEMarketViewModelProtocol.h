//
//  CEMarketViewModelProtocol.h
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 12.12.2017.
//  Copyright © 2017 Artem Zabludovsky. All rights reserved.
//

#ifndef CEMarketViewModelProtocol_h
#define CEMarketViewModelProtocol_h
@class CEMarketViewModel;

@protocol CEMarketViewModelDelegate

/** Уведомление об обновлении данных рынка криптовалют.
@param marketVM CEMarketViewModel
@param arrayWithCurrencies Данные о криптовалютах

вызывается каждые CETimeInterfalForUpdatingMarketData секунд */
- (void)marketDataDidReceived:(CEMarketViewModel *)marketVM withCurrencies:(NSArray *)arrayWithCurrencies;

/** Уведомление об обновлении данных капитализации рынка криптовалют.
 @param marketVM CEMarketViewModel
 @param globalCap Данные о капитализации
 
 вызывается каждые CETimeInterfalForUpdatingGlobalCapitalization секунд */
- (void)globalCapitalizationDidReceived:(CEMarketViewModel *)marketVM withCapitalization:(NSNumber *)globalCap;

@end

#endif /* CEMarketViewModelProtocol_h */
