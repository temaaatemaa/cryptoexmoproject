//
//  Currencies+CoreDataProperties.m
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 31.01.2018.
//  Copyright © 2018 Artem Zabludovsky. All rights reserved.
//
//

#import "Currencies+CoreDataProperties.h"

@implementation Currencies (CoreDataProperties)

+ (NSFetchRequest<Currencies *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Currencies"];
}

@dynamic avgPrice;
@dynamic buyPrice;
@dynamic highPrice;
@dynamic imageData;
@dynamic imageURL;
@dynamic isPriceDecrease;
@dynamic isPriceIncrease;
@dynamic lowPrice;
@dynamic firstCurrencyName;
@dynamic persentage;
@dynamic sellPrice;
@dynamic firstCurrencyShortName;
@dynamic valueInDollar;
@dynamic volume24ch;
@dynamic secondCurrencyName;
@dynamic secondCurrencyShortName;

@end
