//
//  Currencies+CoreDataClass.h
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 11.12.2017.
//  Copyright © 2017 Artem Zabludovsky. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Currencies : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Currencies+CoreDataProperties.h"
