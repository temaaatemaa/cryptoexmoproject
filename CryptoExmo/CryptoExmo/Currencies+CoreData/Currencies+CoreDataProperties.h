//
//  Currencies+CoreDataProperties.h
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 31.01.2018.
//  Copyright © 2018 Artem Zabludovsky. All rights reserved.
//
//

#import "Currencies+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Currencies (CoreDataProperties)

+ (NSFetchRequest<Currencies *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *avgPrice;
@property (nullable, nonatomic, copy) NSString *buyPrice;
@property (nullable, nonatomic, copy) NSString *highPrice;
@property (nullable, nonatomic, retain) NSData *imageData;
@property (nullable, nonatomic, copy) NSURL *imageURL;
@property (nonatomic) BOOL isPriceDecrease;
@property (nonatomic) BOOL isPriceIncrease;
@property (nullable, nonatomic, copy) NSString *lowPrice;
@property (nullable, nonatomic, copy) NSString *firstCurrencyName;
@property (nonatomic) float persentage;
@property (nullable, nonatomic, copy) NSString *sellPrice;
@property (nullable, nonatomic, copy) NSString *firstCurrencyShortName;
@property (nullable, nonatomic, copy) NSString *valueInDollar;
@property (nonatomic) int32_t volume24ch;
@property (nullable, nonatomic, copy) NSString *secondCurrencyName;
@property (nullable, nonatomic, copy) NSString *secondCurrencyShortName;

@end

NS_ASSUME_NONNULL_END
