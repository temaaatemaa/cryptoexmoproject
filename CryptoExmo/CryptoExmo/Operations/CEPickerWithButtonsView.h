//
//  CEPickerWithButtonsView.h
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 16.12.2017.
//  Copyright © 2017 Artem Zabludovsky. All rights reserved.
//


#import <UIKit/UIKit.h>


@class CECryptoOperationsViewModel;


@interface CEPickerWithButtonsView : UIView


@property (nonatomic, strong) UIButton *cancleButton;
@property (nonatomic, strong) UIButton *okButton;
@property (nonatomic, strong) UIPickerView *pickerView;

- (instancetype)initWithViewModel:(CECryptoOperationsViewModel *)viewModel;

@end
