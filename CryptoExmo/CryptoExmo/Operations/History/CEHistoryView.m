//
//  CEHistoryView.m
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 16.12.2017.
//  Copyright © 2017 Artem Zabludovsky. All rights reserved.
//


#import "CEHistoryView.h"
#import "CECryptoOperationsViewModel.h"
#import "CEOrdersTableViewCell.h"
#import <Masonry.h>
#import "UIColor+CEColor.h"


static CGFloat const CEHeightOfOrderCells = 100.f;
static NSString *const CEHistoryCellReuseIndentifier = @"CEHistoryCellReuseIndentifier";


@interface CEHistoryView()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) CECryptoOperationsViewModel *viewModel;

@end


@implementation CEHistoryView


#pragma mark - Lifecycle

- (instancetype)initWithModelView:(CECryptoOperationsViewModel *)viewModel
{
    self = [super init];
    if (self) {
        _viewModel = viewModel;
		[self setupTableView];
		[self setupRefreshControl];
		[self makeConstraints];
    }
    return self;
}

- (void)makeConstraints
{
	[_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
		make.top.equalTo(self.mas_top);
		make.bottom.equalTo(self.mas_bottom);
		make.left.equalTo(self.mas_left);
		make.right.equalTo(self.mas_right);
	}];
}



#pragma mark - Private

- (void)setupTableView
{
	_tableView = [[UITableView alloc]initWithFrame:CGRectNull style:UITableViewStylePlain];
	_tableView.delegate = self;
	_tableView.dataSource = self;
	_tableView.backgroundColor = [UIColor ceBackgroundColor];
	_tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectNull];
	[_tableView registerClass:[CEOrdersTableViewCell class] forCellReuseIdentifier:CEHistoryCellReuseIndentifier];
	_tableView.allowsSelection = NO;
	[self addSubview:_tableView];
}

- (void)setupRefreshControl
{
	_tableView.refreshControl = [[UIRefreshControl alloc] init];
	_tableView.refreshControl.backgroundColor = [UIColor ceRefreshColor];
	_tableView.refreshControl.tintColor = [UIColor whiteColor];
	[_tableView.refreshControl addTarget:self
								  action:@selector(updateData)
						forControlEvents:UIControlEventValueChanged];
}


#pragma mark - Public

- (void)historyDidLoad:(CECryptoOperationsViewModel *)operationsVM withHistoryArray:(NSArray *)historyArray
				 error:(NSString *)error
{
	dispatch_async(dispatch_get_main_queue(), ^{
		[self.tableView reloadData];
		if (_tableView.refreshControl)
		{
			[_tableView.refreshControl endRefreshing];
		}
	});
}

- (void)updateData
{
    [self.viewModel downloadTradesHistory];
}

- (UILabel *)noDataLabel
{
	UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.bounds),
																	  CGRectGetHeight(self.bounds))];
	messageLabel.text = @"No history data is currently available. \n\nIf you have history, \nplease pull down to refresh the list\nor change pair.";
	messageLabel.textColor = [UIColor ceFontColorForMarketAndWalletCell];
	messageLabel.numberOfLines = 0;
	messageLabel.textAlignment = NSTextAlignmentCenter;
	messageLabel.font = [UIFont systemFontOfSize:25 weight:UIFontWeightThin];
	[messageLabel sizeToFit];
	return messageLabel;
}

- (NSDecimalNumber *)roundNSdecimalNumberFromString:(NSString *)numberString
{
	NSDecimalNumberHandler *behavior = [NSDecimalNumberHandler decimalNumberHandlerWithRoundingMode:NSRoundDown
																							  scale:5
																				   raiseOnExactness:NO
																					raiseOnOverflow:NO
																				   raiseOnUnderflow:NO
																				raiseOnDivideByZero:NO];
	NSDecimalNumber *number = [NSDecimalNumber decimalNumberWithString:numberString];
	NSDecimalNumber *roundedNumber = [number decimalNumberByRoundingAccordingToBehavior:behavior];
	return roundedNumber;
}

- (NSString *)translateTimeSinse1970:(NSNumber *)dateSinse1970
{
	NSDate* date = [NSDate dateWithTimeIntervalSince1970:dateSinse1970.doubleValue];
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss zzz"];
	NSString *dateString = [dateFormatter stringFromDate:date];
	return dateString;
}


#pragma mark - TableViewDelegate

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    CEOrdersTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CEHistoryCellReuseIndentifier];
    NSDictionary *dictOfOrder = self.viewModel.ordersHistoryArray[indexPath.row];
    cell.typeLabel.text = ((NSString *)dictOfOrder[@"type"]).uppercaseString;
    cell.timeLabel.text = [self translateTimeSinse1970:dictOfOrder[@"date"]];
	
	NSDecimalNumber *roundedNumberOfQuantity = [self roundNSdecimalNumberFromString:dictOfOrder[@"quantity"]];
    NSDecimalNumber *roundedNumberOfPrice = [self roundNSdecimalNumberFromString:dictOfOrder[@"price"]];
    NSDecimalNumber *roundedNumberOfAmount = [self roundNSdecimalNumberFromString:dictOfOrder[@"amount"]];
    
    cell.amountValueLable.text = [NSString stringWithFormat:@"%@ %@",self.viewModel.pair[0],roundedNumberOfQuantity];
    cell.priceValueLable.text = [NSString stringWithFormat:@"%@ %@",self.viewModel.pair[1],roundedNumberOfPrice];
    cell.totalValueLable.text = [NSString stringWithFormat:@"%@ %@",self.viewModel.pair[1],roundedNumberOfAmount];
    
    return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.viewModel.ordersHistoryArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return CEHeightOfOrderCells;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ([self.viewModel.ordersHistoryArray count])
    {
        
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        self.tableView.backgroundView = nil;
        return 1;
        
    } else
    {
        self.tableView.backgroundView = [self noDataLabel];
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return 0;
}



@end
