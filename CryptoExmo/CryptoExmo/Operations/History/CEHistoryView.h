//
//  CEHistoryView.h
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 16.12.2017.
//  Copyright © 2017 Artem Zabludovsky. All rights reserved.
//


#import <UIKit/UIKit.h>


@class CECryptoOperationsViewModel;
@class CEExmoNetwork;


@interface CEHistoryView : UIView

- (instancetype)initWithModelView:(CECryptoOperationsViewModel *)viewModel;

/** Уведомление о завершении скачивания истории пользовательских ордеров
 @param operationsVM CECryptoOperationsViewModel
 @param historyArray Массив ордеров в истории пользователя
 @param error Ошибка запроса*/
- (void)historyDidLoad:(CECryptoOperationsViewModel *)operationsVM withHistoryArray:(NSArray *)historyArray error:(NSString *)error;

@end
