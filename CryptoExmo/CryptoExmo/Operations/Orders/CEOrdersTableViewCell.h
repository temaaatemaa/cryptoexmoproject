//
//  CEOrdersTableViewCell.h
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 17.12.2017.
//  Copyright © 2017 Artem Zabludovsky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CEOrdersTableViewCell : UITableViewCell


@property (nonatomic, strong) UILabel *typeLabel;
@property (nonatomic, strong) UILabel *timeLabel;

@property (nonatomic, strong) UILabel *amountTextLable;
@property (nonatomic, strong) UILabel *amountValueLable;

@property (nonatomic, strong) UILabel *priceTextLable;
@property (nonatomic, strong) UILabel *priceValueLable;

@property (nonatomic, strong) UILabel *totalTextLable;
@property (nonatomic, strong) UILabel *totalValueLable;

@end
