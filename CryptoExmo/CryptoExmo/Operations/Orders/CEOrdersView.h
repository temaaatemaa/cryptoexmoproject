//
//  CEOrdersView.h
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 16.12.2017.
//  Copyright © 2017 Artem Zabludovsky. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CECryptoOperationsViewModel;
@class CEExmoNetwork;

@interface CEOrdersView : UIView

- (instancetype)initWithModelView:(CECryptoOperationsViewModel *)viewModel;

/** Уведомление о завершении скачивания массива открытых ордеров пользователя
 @param arrayOfOpenOrders Массив открытых ордеров*/
- (void)openOrdersDidLoad:(NSArray *)arrayOfOpenOrders;

/** Уведомление об удалении ордера
 @param operationsVM CECryptoOperationsViewModel
 @param result Результат удаления
 @param error Ошибка при удалении*/
- (void)orderDidDelete:(CECryptoOperationsViewModel *)operationsVM WithResult:(NSString *)result withError:(NSString *)error;

@end
