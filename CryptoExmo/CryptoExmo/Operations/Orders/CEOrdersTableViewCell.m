//
//  CEOrdersTableViewCell.m
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 17.12.2017.
//  Copyright © 2017 Artem Zabludovsky. All rights reserved.
//


#import "CEOrdersTableViewCell.h"
#import <Masonry.h>
#import "UIColor+CEColor.h"
#import "CEDevice.h"

static CGFloat const CETopOffset = 5.f;
static CGFloat const CELeftTopOffset = 20.f;
static CGFloat const CETimeLabelLeftOffset = 25.f;
static CGFloat const CETypeLabelTopOffset = 30.f;
static CGFloat const CETypeLabelLeftOffset = 65.f;
static CGFloat const CETypeLabelWidth = 180.f;


@implementation CEOrdersTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        self.backgroundColor = [UIColor ceBackgroundColor];
        _typeLabel = [UILabel new];
        _timeLabel = [UILabel new];
        _amountTextLable = [UILabel new];
        _amountValueLable = [UILabel new];
        _priceTextLable = [UILabel new];
        _priceValueLable = [UILabel new];
        _totalTextLable = [UILabel new];
        _totalValueLable = [UILabel new];
        
        UIFont *font = [UIFont systemFontOfSize:13 weight:UIFontWeightThin];
        _typeLabel.font = [UIFont systemFontOfSize:29 weight:UIFontWeightThin];
        _timeLabel.font = [UIFont systemFontOfSize:11 weight:UIFontWeightThin];
        _amountTextLable.font = font;
        _amountValueLable.font = font;
        _priceTextLable.font = font;
        _priceValueLable.font = font;
        _totalTextLable.font = font;
        _totalValueLable.font = font;
        
        _typeLabel.textColor = [UIColor ceFontColorForMarketAndWalletCell];
        _timeLabel.textColor = [UIColor ceFontColorForMarketAndWalletCell];
        _amountTextLable.textColor = [UIColor ceFontColorForMarketAndWalletCell];
        _amountValueLable.textColor = [UIColor ceFontColorForMarketAndWalletCell];
        _priceTextLable.textColor = [UIColor ceFontColorForMarketAndWalletCell];
        _priceValueLable.textColor = [UIColor ceFontColorForMarketAndWalletCell];
        _totalTextLable.textColor = [UIColor ceFontColorForMarketAndWalletCell];
        _totalValueLable.textColor = [UIColor ceFontColorForMarketAndWalletCell];
        
        _amountTextLable.text = @"Amount:";
        _priceTextLable.text = @"Price:";
        _totalTextLable.text = @"Total:";
        
        _amountValueLable.text = @"0.00";
        _priceValueLable.text = @"0.00";
        _totalValueLable.text = @"0.00";
        
        _amountTextLable.textAlignment = NSTextAlignmentRight;
        _priceTextLable.textAlignment = NSTextAlignmentRight;
        _totalTextLable.textAlignment = NSTextAlignmentRight;
        
        [self addSubview:_typeLabel];
        [self addSubview:_timeLabel];
        [self addSubview:_amountTextLable];
        [self addSubview:_amountValueLable];
        [self addSubview:_priceTextLable];
        [self addSubview:_priceValueLable];
        [self addSubview:_totalTextLable];
        [self addSubview:_totalValueLable];
        
        
        [_typeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView.mas_top).with.offset(CETypeLabelTopOffset);
            make.left.equalTo(self.contentView.mas_left).with.offset(CETypeLabelLeftOffset);
			if ([CEDevice isIphoneWithSmallScreen]) {
				make.width.equalTo(@(115));
			}
			else
			{
				make.width.equalTo(@(CETypeLabelWidth));
			}
        }];
        
        [_timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_typeLabel.mas_bottom).with.offset(CELeftTopOffset);
            make.left.equalTo(self.contentView.mas_left).with.offset(CETimeLabelLeftOffset);
			if ([CEDevice isIphoneWithSmallScreen]) {
				make.width.equalTo(@(155));
			}
			else
			{
				make.width.equalTo(_typeLabel);
			}
        }];
        
        [_amountTextLable mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView.mas_top).with.offset(CELeftTopOffset);
            make.left.equalTo(_typeLabel.mas_right);
        }];
        
        [_amountValueLable mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_amountTextLable.mas_right);
            make.top.equalTo(self.contentView.mas_top).with.offset(CELeftTopOffset);
        }];
        
        [_priceTextLable mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_amountTextLable.mas_bottom).with.offset(CETopOffset);
            make.left.equalTo(_typeLabel.mas_right);
            make.width.equalTo(_amountTextLable.mas_width);

        }];
        
        [_priceValueLable mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_priceTextLable.mas_right);
            make.top.equalTo(_amountTextLable.mas_bottom).with.offset(CETopOffset);
        }];
        
        [_totalTextLable mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_priceTextLable.mas_bottom).with.offset(CETopOffset);
            make.left.equalTo(_typeLabel.mas_right);
            make.width.equalTo(_amountTextLable.mas_width);
        }];
        
        [_totalValueLable mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_totalTextLable.mas_right);
            make.top.equalTo(_priceTextLable.mas_bottom).with.offset(CETopOffset);
        }];
    }
    return self;
}

@end
