//
//  CEOrdersView.m
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 16.12.2017.
//  Copyright © 2017 Artem Zabludovsky. All rights reserved.
//


#import "CEOrdersView.h"
#import <Masonry.h>
#import "CECryptoOperationsViewModel.h"
#import "CEOrdersTableViewCell.h"
#import "UIColor+CEColor.h"


static NSString *const CEOrderCellReuseIndentifier = @"CEOrderCellReuseIndentifier";
static CGFloat const CEHeightOfOrderCells = 100.f;


@interface CEOrdersView()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) CECryptoOperationsViewModel *viewModel;
@property (nonatomic, strong) NSIndexPath *indexPathForDeletingOrder;

@end


@implementation CEOrdersView


#pragma mark - Lifecycle

- (instancetype)initWithModelView:(CECryptoOperationsViewModel *)viewModel
{
    self = [super init];
    if (self) {
        _viewModel = viewModel;
		
		[self setupTableView];
		[self setupRefreshControl];
    }
    return self;
}

- (void)updateConstraints
{
	[_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
		make.top.equalTo(self.mas_top);
		make.bottom.equalTo(self.mas_bottom);
		make.left.equalTo(self.mas_left);
		make.right.equalTo(self.mas_right);
	}];
	[super updateConstraints];
}


#pragma mark - Public

- (void)openOrdersDidLoad:(NSArray *)arrayOfOpenOrders
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
        if (self.tableView.refreshControl)
        {
            [self.tableView.refreshControl endRefreshing];
        }
    });
}

- (void)orderDidDelete:(CECryptoOperationsViewModel *)operationsVM WithResult:(NSString *)result
             withError:(NSString *)error
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (error.length)
        {
            NSLog(@"ERRORR");
            [self updateData];
        }
        else
        {
			[self removeObjectFromArrayOfOpenOrdersAtIndex:self.indexPathForDeletingOrder.row];
            if (self.viewModel.arrayOfOpenOrders.count == 0)
            {
                [_tableView reloadData];
                return;
            }
			[self.tableView performBatchUpdates:^{
				[_tableView deleteRowsAtIndexPaths:@[self.indexPathForDeletingOrder]
								  withRowAnimation:UITableViewRowAnimationLeft];
			} completion:nil];
        }
    });
}

- (void)removeObjectFromArrayOfOpenOrdersAtIndex:(NSUInteger)index
{
	NSMutableArray *arr = [self.viewModel.arrayOfOpenOrders mutableCopy];
	[arr removeObjectAtIndex:index];
	self.viewModel.arrayOfOpenOrders = [arr copy];
}


#pragma mark - Private

- (void)updateData
{
    [self.viewModel downloadArrayOfUserOpenOrders];
}

- (void)setupTableView
{
	_tableView = [[UITableView alloc]initWithFrame:CGRectNull style:UITableViewStylePlain];
	_tableView.delegate = self;
	_tableView.dataSource = self;
	_tableView.backgroundColor = [UIColor ceBackgroundColor];
	_tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectNull];
	_tableView.allowsSelection = NO;
	[_tableView registerClass:[CEOrdersTableViewCell class] forCellReuseIdentifier:CEOrderCellReuseIndentifier];
	[self addSubview:_tableView];
}

- (void)setupRefreshControl
{
	_tableView.refreshControl = [[UIRefreshControl alloc] init];
	_tableView.refreshControl.backgroundColor = [UIColor ceRefreshColor];
	_tableView.refreshControl.tintColor = [UIColor whiteColor];
	[_tableView.refreshControl addTarget:self
								  action:@selector(updateData)
						forControlEvents:UIControlEventValueChanged];
}

- (UILabel *)noDataLabel
{
	UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.bounds),
																	  CGRectGetHeight(self.bounds))];
	
	messageLabel.text = @"No orders data is currently available. \n\nIf you have orders, \nplease pull down to refresh the list.";
	messageLabel.textColor = [UIColor ceFontColorForMarketAndWalletCell];
	messageLabel.numberOfLines = 0;
	messageLabel.textAlignment = NSTextAlignmentCenter;
	messageLabel.font = [UIFont systemFontOfSize:25 weight:UIFontWeightThin];
	[messageLabel sizeToFit];
	return messageLabel;
}

- (NSString *)translateTimeSinse1970:(NSNumber *)dateSinse1970
{
	NSDate* date = [NSDate dateWithTimeIntervalSince1970:dateSinse1970.doubleValue];
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss zzz"];
	NSString *dateString = [dateFormatter stringFromDate:date];
	return dateString;
}


#pragma mark - TableViewDelegate

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    CEOrdersTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CEOrderCellReuseIndentifier];
    NSDictionary *dictOfOrder = self.viewModel.arrayOfOpenOrders[indexPath.row];
    cell.typeLabel.text = ((NSString *)dictOfOrder[@"type"]).uppercaseString;
	cell.timeLabel.text = [self translateTimeSinse1970:dictOfOrder[@"created"]];
    cell.amountValueLable.text = [NSString stringWithFormat:@"%@ %@",self.viewModel.pair[0],dictOfOrder[@"quantity"]];
    cell.priceValueLable.text = [NSString stringWithFormat:@"%@ %@",self.viewModel.pair[1],dictOfOrder[@"price"]];
    cell.totalValueLable.text = [NSString stringWithFormat:@"%@ %@",self.viewModel.pair[1],dictOfOrder[@"amount"]];
    return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.viewModel.arrayOfOpenOrders count];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ([self.viewModel.arrayOfOpenOrders count])
	{
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        self.tableView.backgroundView = nil;
        return 1;
        
    } else
	{
        self.tableView.backgroundView = [self noDataLabel];
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return CEHeightOfOrderCells;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
											forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *orderID = self.viewModel.arrayOfOpenOrders[indexPath.row][@"order_id"];
    [self.viewModel deleteUserOrder:orderID];
    self.indexPathForDeletingOrder = indexPath;
}


@end
