//
//  CECryptoOperationsViewController.m
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 11.12.2017.
//  Copyright © 2017 Artem Zabludovsky. All rights reserved.
//


#import "CECryptoOperationsViewController.h"
#import "CEButtonDownLineView.h"
#import <Masonry.h>
#import "CETradeView.h"
#import "CEOrdersView.h"
#import "CEHistoryView.h"
#import "CEPickerWithButtonsView.h"
#import "UIColor+CEColor.h"
#import "UIFont+CEFont.h"
#import "CECryptoOperationsViewModelProtocol.h"
#import "CECryptoOperationsViewModel.h"
#import <AudioToolbox/AudioToolbox.h>


static CGFloat const CEPersentaigeHeightOfUpperView = 0.13;
static CGFloat const CEPickerViewHeight = 500.f;
static CGFloat const CEChooseCurrencyButtonOffset = 10.f;

static NSTimeInterval const CEPickerAnimationTime = 0.5;

static NSString *const CENameOFTradeButton = @"TRADE";
static NSString *const CENameOFOrdersButton = @"ORDERS";
static NSString *const CENameOFHistoryButton = @"HISTORY";


static NSString *const CEErrorForWrongApi =
									@"Error 40017: Wrong api key";
static NSString *const CEErrorForAuthorizationSignatureError =
									@"Error 40005: Authorization error, Incorrect signature";
static NSString *const CEErrorForAuthorizationKeyError =
									@"Error 40003: Authorization error, http header 'Key' not specified";


@interface CECryptoOperationsViewController() <CECryptoOperationsViewModelDelegate>

@property (nonatomic, strong) UIView *myUpperView;
@property (nonatomic, strong) UIButton *chooseCurrencyButton;
@property (nonatomic, strong) CEPickerWithButtonsView *pickerWithButtonsView;
@property (nonatomic, strong) UIVisualEffectView *blurEffectView;

@property (nonatomic, strong) UIButton *tradeButton;
@property (nonatomic, strong) UIButton *ordersButton;
@property (nonatomic, strong) UIButton *historyButton;
@property (nonatomic, strong) CEButtonDownLineView *buttonLineView;

@property (nonatomic, strong) CETradeView *tradeView;
@property (nonatomic, strong) CEOrdersView *ordersView;
@property (nonatomic, strong) CEHistoryView *historyView;
@property (nonatomic, strong) id nowTabView;


@property (nonatomic, strong) CECryptoOperationsViewModel *viewModel;
@property (nonatomic, copy) NSArray *pair;
@end


@implementation CECryptoOperationsViewController


#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
	
	[self setupUpperViewWithChooseCurrencyButton];
    
	[self setupTabsButtons];
	
	[self setupTabbarBackground];
	
	
	
	[self setNeedsStatusBarAppearanceUpdate];
	
	_viewModel = [[CECryptoOperationsViewModel alloc]init];
    _tradeView = [[CETradeView alloc]initWithViewModel:_viewModel andViewController:self];
	[self.view addSubview:_tradeView];
	self.nowTabView = _tradeView;
    _viewModel.delegate = self;
	[self makeConstraints];
}

-(void)makeConstraints
{
    [_myUpperView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_top);
        make.right.equalTo(self.view.mas_right);
        make.left.equalTo(self.view.mas_left);
        make.height.equalTo(@(CEPersentaigeHeightOfUpperView*CGRectGetHeight(self.view.frame)));
    }];
    
    [_chooseCurrencyButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(_myUpperView.mas_centerX);
        make.bottom.equalTo(_myUpperView.mas_bottom).with.offset(-CEChooseCurrencyButtonOffset);
    }];
    
    [_tradeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_myUpperView.mas_bottom);
        make.left.equalTo(self.view.mas_left);
    }];
    
    [_ordersButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_myUpperView.mas_bottom);
        make.left.equalTo(_tradeButton.mas_right).with.offset(1);
        make.height.equalTo(_tradeButton.mas_height);
        make.width.equalTo(_tradeButton.mas_width);
    }];
    
    [_historyButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_myUpperView.mas_bottom);
        make.right.equalTo(self.view.mas_right);
        make.left.equalTo(_ordersButton.mas_right).with.offset(1);
        make.height.equalTo(_tradeButton.mas_height);
        make.width.equalTo(_tradeButton.mas_width);
    }];
    
    [_tradeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_tradeButton.mas_bottom);
        make.right.equalTo(self.view.mas_right);
        make.left.equalTo(self.view.mas_left);
        make.bottom.equalTo(self.view.mas_bottom).with.offset(-CGRectGetHeight(self.tabBarController.tabBar.frame));
    }];
    
    [_ordersView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_tradeButton.mas_bottom);
        make.right.equalTo(self.view.mas_right);
        make.left.equalTo(self.view.mas_left);
        make.bottom.equalTo(self.view.mas_bottom);
    }];
}


#pragma mark - IBActions

- (void)openMenuForChooseCurrency
{
	[[[UIApplication sharedApplication] keyWindow] endEditing:YES];
    AudioServicesPlaySystemSound(1520);
    if (self.pickerWithButtonsView)
    {
        [self.pickerWithButtonsView removeFromSuperview];
    }
    self.pickerWithButtonsView = [[CEPickerWithButtonsView alloc]initWithViewModel:self.viewModel];
    
	[self makeBlurEffect];
	
    [self animateShowPickerView];
}

- (void)openOtherTab:(UIButton *)button
{
    [_buttonLineView removeFromSuperview];
    _buttonLineView = [[CEButtonDownLineView alloc]initWithColor:[UIColor ceAdditionalColor] andHeight:@2];
    [button addSubview:_buttonLineView];
    
    UIView *nextView;
    if ([button.titleLabel.text isEqualToString:CENameOFTradeButton])
    {
        if (!self.tradeView)
        {
            self.tradeView = [[CETradeView alloc]init];
        }
        nextView = self.tradeView;
    }
    if ([button.titleLabel.text isEqualToString:CENameOFOrdersButton])
    {
        if (!self.ordersView)
        {
            self.ordersView = [[CEOrdersView alloc]initWithModelView:self.viewModel];
        }
        nextView = self.ordersView;
    }
    if ([button.titleLabel.text isEqualToString:CENameOFHistoryButton])
    {
        if (!self.historyView)
        {
            self.historyView = [[CEHistoryView alloc]initWithModelView:self.viewModel];
        }
        nextView = self.historyView;
    }
    [self.nowTabView removeFromSuperview];
    self.nowTabView = nextView;
    [self.view addSubview:self.nowTabView];
    [self.nowTabView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_tradeButton.mas_bottom);
        make.right.equalTo(self.view.mas_right);
        make.left.equalTo(self.view.mas_left);
        make.bottom.equalTo(self.view.mas_bottom).with.offset(-CGRectGetHeight(self.tabBarController.tabBar.frame));
    }];
    [self.view setNeedsUpdateConstraints];
    [self.view updateConstraintsIfNeeded];
    [self.view layoutIfNeeded];
}

- (void)animateShowPickerView
{
    if (self.blurEffectView)
    {
        [self.view addSubview:self.blurEffectView];
        self.blurEffectView.alpha = 0;
    }
    [self.view addSubview:self.pickerWithButtonsView];
    
    [self.pickerWithButtonsView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.view.mas_bottom).with.offset(CEPickerViewHeight);
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.height.equalTo(@(CEPickerViewHeight));
    }];
    [self.view layoutIfNeeded];
    [UIView animateWithDuration:CEPickerAnimationTime animations:^{
         [self.pickerWithButtonsView mas_updateConstraints:^(MASConstraintMaker *make) {
             make.bottom.equalTo(self.view.mas_bottom);
         }];
         if (self.blurEffectView)
         {
             self.blurEffectView.alpha = 1;
         }
         [self.view layoutIfNeeded];
     }];
    
}


#pragma mark - Private

- (void)setupUpperViewWithChooseCurrencyButton
{
	_myUpperView = [UIView new];
	_chooseCurrencyButton = [UIButton new];
	
	[_chooseCurrencyButton addTarget:self action:@selector(openMenuForChooseCurrency) forControlEvents:UIControlEventTouchDown];
	
	[self.view addSubview:_myUpperView];
	[self.view addSubview:_chooseCurrencyButton];
	_myUpperView.backgroundColor = [UIColor ceMainColor];
	CEButtonDownLineView *line = [[CEButtonDownLineView alloc]initWithColor:[UIColor ceAdditionalColor] andHeight:@2];
	
	[_chooseCurrencyButton addSubview:line];
}

- (void)setupTabbarBackground
{
	CGRect rectForTabBar = self.tabBarController.tabBar.frame;
	UIView *backgroundForTabBarView = [[UIView alloc] initWithFrame:rectForTabBar];
	backgroundForTabBarView.backgroundColor = [UIColor ceBackgroundColor];
	[self.view addSubview:backgroundForTabBarView];
}

- (void)setupTabsButtons
{
	_tradeButton = [UIButton new];
	_ordersButton = [UIButton new];
	_historyButton = [UIButton new];
	
	[self.view addSubview:_tradeButton];
	[self.view addSubview:_ordersButton];
	[self.view addSubview:_historyButton];
	
	_tradeButton.backgroundColor = [UIColor ceMainColor];
	_ordersButton.backgroundColor = [UIColor ceMainColor];
	_historyButton.backgroundColor = [UIColor ceMainColor];
	
	_buttonLineView = [[CEButtonDownLineView alloc]initWithColor:[UIColor ceAdditionalColor] andHeight:@2];
	[_tradeButton addSubview:_buttonLineView];
	
	[self setupFontsForButtons];
	[self setTargestsToTabsButtons];
}

- (void)setTargestsToTabsButtons
{
    [_tradeButton addTarget:self action:@selector(openOtherTab:) forControlEvents:UIControlEventTouchDown];
    [_ordersButton addTarget:self action:@selector(openOtherTab:) forControlEvents:UIControlEventTouchDown];
    [_historyButton addTarget:self action:@selector(openOtherTab:) forControlEvents:UIControlEventTouchDown];
}

- (void)setupFontsForButtons
{
    NSDictionary *attributs = @{
                                NSForegroundColorAttributeName : [UIColor ceFontColor],
                                NSFontAttributeName : [UIFont ceFontForChooseButtonInOperationsVC]
                                };
    
    NSAttributedString *chooseButtonTitle = [[NSAttributedString alloc] initWithString:@"BTC/USD" attributes:attributs];
    [_chooseCurrencyButton setAttributedTitle:chooseButtonTitle forState:UIControlStateNormal];
    
    NSDictionary *attrsDictionary = @{
                                      NSForegroundColorAttributeName : [UIColor whiteColor],
                                      NSFontAttributeName : [UIFont ceFontForTabsButtonInOperationsVC]
                                      };
    
    NSAttributedString *tradeTitle = [[NSAttributedString alloc] 
									  initWithString:CENameOFTradeButton attributes:attrsDictionary];
    NSAttributedString *ordersTitle = [[NSAttributedString alloc]
									   initWithString:CENameOFOrdersButton attributes:attrsDictionary];
    NSAttributedString *historyTitle = [[NSAttributedString alloc]
										initWithString:CENameOFHistoryButton attributes:attrsDictionary];
    
    [_tradeButton setAttributedTitle:tradeTitle forState:UIControlStateNormal];
    [_ordersButton setAttributedTitle:ordersTitle forState:UIControlStateNormal];
    [_historyButton setAttributedTitle:historyTitle forState:UIControlStateNormal];
}

- (void)updateTitleOfChooseButton
{
    NSDecimalNumberHandler *behavior = [NSDecimalNumberHandler decimalNumberHandlerWithRoundingMode:NSRoundDown
                                                                                              scale:3
                                                                                   raiseOnExactness:NO
                                                                                    raiseOnOverflow:NO
                                                                                   raiseOnUnderflow:NO
                                                                                raiseOnDivideByZero:NO];

    NSDecimalNumber *roundedNumberOfCchangeCurr = [self.viewModel.changeCurrency decimalNumberByRoundingAccordingToBehavior:behavior];
    NSDictionary *attributs = @{
                                NSForegroundColorAttributeName : [UIColor ceFontColor],
                                NSFontAttributeName : [UIFont ceFontForChooseButtonInOperationsVC]
                                };

    NSString *title = [NSString stringWithFormat:@"%@/%@ %@",self.pair[0],self.pair[1],roundedNumberOfCchangeCurr];
    NSAttributedString *chooseButtonTitle = [[NSAttributedString alloc] initWithString:title attributes:attributs];
    [_chooseCurrencyButton setAttributedTitle:chooseButtonTitle forState:UIControlStateNormal];
}

- (UIImage *)imageWithImage:(UIImage *)image forSize:(CGSize)size
{
    UIGraphicsBeginImageContextWithOptions(size, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (void)addLoginButtonToAlertController:(UIAlertController *)alertController
{
    UIAlertAction *authButton = [UIAlertAction actionWithTitle:@"Log in" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.viewModel showAuthorisationController];
    }];
    [alertController addAction:authButton];
}

- (void)makeBlurEffect
{
	if (!UIAccessibilityIsReduceTransparencyEnabled())
	{
		self.view.backgroundColor = [UIColor clearColor];
		
		UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleRegular];
		self.blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
		self.blurEffectView.frame = self.view.bounds;
		self.blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	} else
	{
		self.view.backgroundColor = [UIColor blackColor];
	}
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
	return UIStatusBarStyleLightContent;
}


#pragma mark - CECryptoOperationsViewModelDelegate

- (void)pairDidChange:(CEExmoNetwork *)exmo pair:(NSArray *)pair
{
    self.pair = pair;
    [self updateTitleOfChooseButton];
    self.tradeView.pair = pair;
}

- (void)currencyDataDidUpdate:(CECryptoOperationsViewModel *)operationsVM with:(NSArray *)arrayOfCurrency
{
    [self updateTitleOfChooseButton];
}

- (void)shouldPresent:(CECryptoOperationsViewModel *)operationsVM QRCodeViewController:(UIViewController *)vc
{
    [self presentViewController:vc animated:YES completion:nil];
}

- (void)openOrdersDidLoad:(CECryptoOperationsViewModel *)operationsVM withOpenOrders:(NSArray *)arrayOfOpenOrders
{
    [self.ordersView openOrdersDidLoad:arrayOfOpenOrders];
}

- (void)orderDidDelete:(CECryptoOperationsViewModel *)operationsVM WithResult:(NSString *)result withError:(NSString *)error
{
    [self.ordersView orderDidDelete:operationsVM WithResult:result withError:error];
}

- (void)userDataDidUpdate:(CECryptoOperationsViewModel *)operationsVM withBalance:(NSDictionary *)dictionaryOfBalance
                                                                    andCurrencies:(NSArray *)arrayOfCurrencyNonNil
{
    [self.tradeView userDataDidUpdate:dictionaryOfBalance andArray:arrayOfCurrencyNonNil];
}

- (void)notCreateOrder:(CECryptoOperationsViewModel *)operationsVM withError:(NSString *)error
{
    [self.tradeView errorWithCreatingError:error];
}

- (void)orderDidCreated:(CECryptoOperationsViewModel *)operationsVM withResult:(BOOL)result
{
    [self.tradeView orderDidCreatedWithResult:result];
}

- (void)historyDidLoad:(CECryptoOperationsViewModel *)operationsVM withHistoryArray:(NSArray *)historyArray error:(NSString *)error
{
    [self.historyView historyDidLoad:operationsVM withHistoryArray:historyArray error:error];
}

- (void)shouldShowAlert:(CECryptoOperationsViewModel *)operationsVM withMessage:(NSString *)message
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error!" message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:okButton];
    if ([message isEqualToString:CEErrorForWrongApi])
    {
        [self addLoginButtonToAlertController:alertController];
    }
    if ([message isEqualToString:CEErrorForAuthorizationSignatureError])
    {
        [self addLoginButtonToAlertController:alertController];
    }
    if ([message isEqualToString:CEErrorForAuthorizationKeyError])
    {
        [self addLoginButtonToAlertController:alertController];
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertController animated:YES completion:nil];
    });
}

- (void)currencyHistoryDidUpdate:(CECryptoOperationsViewModel *)operationsVM withHistory:(NSDictionary *)historyDictionary
{
    [self.tradeView updateCurrencyHistory:historyDictionary];
}


@end
