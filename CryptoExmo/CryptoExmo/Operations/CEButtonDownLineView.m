//
//  CEButtonDownLineView.m
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 14.12.2017.
//  Copyright © 2017 Artem Zabludovsky. All rights reserved.
//


#import "CEButtonDownLineView.h"
#import <Masonry.h>


@interface CEButtonDownLineView()

@property (nonatomic, strong) NSNumber *height;

@end


@implementation CEButtonDownLineView


- (instancetype)initWithColor:(UIColor *)color andHeight:(NSNumber *)height
{
    self = [super init];
    if (self) {
        self.backgroundColor = color;
        _height = height;
    }
    return self;
}

-(void)updateConstraints
{
    [self mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.superview.mas_bottom);
        make.left.equalTo(self.superview.mas_left);
        make.right.equalTo(self.superview.mas_right);
        make.height.equalTo(self.height);
    }];
    [super updateConstraints];
}

@end
