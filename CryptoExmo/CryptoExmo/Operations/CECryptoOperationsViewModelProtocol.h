//
//  CECryptoOperationsViewModelProtocol.h
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 17.12.2017.
//  Copyright © 2017 Artem Zabludovsky. All rights reserved.
//


#import <UIKit/UIKit.h>


#ifndef CECryptoOperationsViewModelProtocol_h
#define CECryptoOperationsViewModelProtocol_h


@class CEExmoNetwork;
@class CECryptoOperationsViewModel;


@protocol CECryptoOperationsViewModelDelegate

@optional

/** Уведомление об изменении текущей валютной пары
 @param operationsVM CECryptoOperationsViewModel
 @param pair Новая валютная пара */
- (void)pairDidChange:(CECryptoOperationsViewModel *)operationsVM pair:(NSArray *)pair;

/** Уведомление об изменении цен рынка криптовалют
 @param operationsVM CECryptoOperationsViewModel
 @param arrayOfCurrency Массив криптовалют */
- (void)currencyDataDidUpdate:(CECryptoOperationsViewModel *)operationsVM with:(NSArray *)arrayOfCurrency;

/** Уведомление о завершении скачивания массива открытых ордеров пользователя
 @param operationsVM CECryptoOperationsViewModel
 @param arrayOfOpenOrders Массив открытых ордеров
 вызывается при
 - (void)downloadArrayOfUserOpenOrders;*/
- (void)openOrdersDidLoad:(CECryptoOperationsViewModel *)operationsVM withOpenOrders:(NSArray *)arrayOfOpenOrders;

/** Уведомление об удалении ордера
 @param operationsVM CECryptoOperationsViewModel
 @param result Результат удаления
 @param error Ошибка при удалении
 вызывается при
 - (void)deleteUserOrder:(NSString *)orderID;*/
- (void)orderDidDelete:(CECryptoOperationsViewModel *)operationsVM WithResult:(NSString *)result
																	withError:(NSString *)error;

/** Уведомление об обновлении баланса пользователя
 @param operationsVM CECryptoOperationsViewModel
 @param dictionaryOfBalance Dictionary баланса
 @param arrayOfCurrencyNonNil Массив валют баланс которых не равен 0*/
- (void)userDataDidUpdate:(CECryptoOperationsViewModel *)operationsVM withBalance:(NSDictionary *)dictionaryOfBalance
                                                                andCurrencies:(NSArray *)arrayOfCurrencyNonNil;

/** Уведомление об ошибки при создании оредера
 @param operationsVM CECryptoOperationsViewModel
 @param error Ошибка при при создании ордера
 может вызываться при
 - (void)createOrderForType:(NSString *)type withQuantity:(NSDecimalNumber *)quantity withPrice:(NSDecimalNumber *)price;*/
- (void)notCreateOrder:(CECryptoOperationsViewModel *)operationsVM withError:(NSString *)error;

/** Уведомление об успешном создании ордера
 @param operationsVM CECryptoOperationsViewModel
 @param result Результат создания
 вызывается при
 - (void)createOrderForType:(NSString *)type withQuantity:(NSDecimalNumber *)quantity withPrice:(NSDecimalNumber *)price;*/
- (void)orderDidCreated:(CECryptoOperationsViewModel *)operationsVM withResult:(BOOL)result;

/** Уведомление о завершении скачивания истории пользовательских ордеров
 @param operationsVM CECryptoOperationsViewModel
 @param historyArray Массив ордеров в истории пользователя
 @param error Ошибка запроса
 вызывается при - (void)downloadTradesHistory;*/
- (void)historyDidLoad:(CECryptoOperationsViewModel *)operationsVM withHistoryArray:(NSArray *)historyArray
                                                                            error:(NSString *)error;

/** Уведомление о требовании показать алерт
 @param operationsVM CECryptoOperationsViewModel
 @param message Алерт сообщение */
- (void)shouldShowAlert:(CECryptoOperationsViewModel *)operationsVM withMessage:(NSString *)message;

/** Уведомление о требовании показать контроллер авторизации
 @param operationsVM CECryptoOperationsViewModel
 @param vc Контроллер авторизации
 вызывается при - (void)showAuthorisationController; */
- (void)shouldPresent:(CECryptoOperationsViewModel *)operationsVM QRCodeViewController:(UIViewController *)vc;

/** Уведомление об обновлении данных для графика(история валюты)
 @param operationsVM CECryptoOperationsViewModel
 @param historyDictionary Dictionary истории валюты
 вызывается при
 - (void)loadDayHistoryForGraph;
 - (void)loadMinuteHistoryForGraph;
 - (void)loadHourHistoryForGraph;*/
- (void)currencyHistoryDidUpdate:(CECryptoOperationsViewModel *)operationsVM withHistory:(NSDictionary *)historyDictionary;

@end

#endif /* CECryptoOperationsViewModelProtocol_h */
