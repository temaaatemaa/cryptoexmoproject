//
//  CryptoOperationsModelView.m
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 17.12.2017.
//  Copyright © 2017 Artem Zabludovsky. All rights reserved.
//


#import "CECryptoOperationsViewModel.h"
#import "CEExmoNetwork.h"
#import "AppDelegate.h"
#import "CEAuthorisationViewController.h"
#import "CEAuthorisationViewControllerProtocol.h"


@interface CECryptoOperationsViewModel() <CEExmoNetworkDelegate,NSFetchedResultsControllerDelegate,CEAuthorisationViewControllerDelegate>

@property (nonatomic, strong) CEExmoNetwork *exmo;


@property (nonatomic, strong) NSDecimalNumber *amountOfCryptoCurrency;
@property (nonatomic, strong) NSDecimalNumber *amountOfSecondCurrency;

@property (nonatomic, strong) NSDecimalNumber *changeCurrency;

@property (nonatomic, copy) NSArray *arrayOfCurrenciesNameWhichBalanceIsNonNull;
@property (nonatomic, strong) NSMutableDictionary *dictionaryOfBalance;

@property (nonatomic, copy) NSArray *ordersHistoryArray;

@end

@implementation CECryptoOperationsViewModel


#pragma mark - Lifecycle

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        _exmo = [[CEExmoNetwork alloc]init];
        _exmo.delegate = self;
    }
    return self;
}


#pragma mark - Custom Accessors

-(void)setDelegate:(id<CECryptoOperationsViewModelDelegate>)delegate
{
    _delegate = delegate;
	[self.exmo getInfoForMarket];
    [self setup];
}

-(NSDecimalNumber *)amountOfSecondCurrency
{
    if (!_amountOfSecondCurrency)
    {
        return [NSDecimalNumber decimalNumberWithString:@"0.0"];
    }
    return _amountOfSecondCurrency;
}

-(NSDecimalNumber *)amountOfCryptoCurrency
{
	if (!_amountOfCryptoCurrency)
    {
        return [NSDecimalNumber decimalNumberWithString:@"0.0"];
    }
    return _amountOfCryptoCurrency;
}

-(NSDecimalNumber *)changeCurrency
{
    if (!_changeCurrency)
    {
        return [NSDecimalNumber decimalNumberWithString:@"0.0"];
    }
    return _changeCurrency;
}

- (NSPersistentContainer *)container
{
    UIApplication *app = [UIApplication sharedApplication];
    NSPersistentContainer *container = ((AppDelegate *)app.delegate).persistentContainer;
    return container;
}

- (void)setPair:(NSArray *)pair
{
	_pair = pair;
}


#pragma mark - Public

- (void)showAuthorisationController
{
    CEAuthorisationViewController *authVC = [CEAuthorisationViewController new];
    authVC.delegate = self;
    [self.delegate shouldPresent:self QRCodeViewController:authVC];
}

- (void)downloadArrayOfUserOpenOrders
{
    [self.exmo getArrayOfUserOpenOrders];
}

- (void)downloadTradesHistory
{
    NSString *pair = [NSString stringWithFormat:@"%@_%@",self.pair[0],self.pair[1]];
    NSNumber *amount = [NSNumber numberWithInt:25];
    NSNumber *offset = [NSNumber numberWithUnsignedInteger:[self.ordersHistoryArray count]];
    [self.exmo getHistoryForPair:pair amount:amount offset:offset];
}

- (void)deleteUserOrder:(NSString *)orderID
{
    [self.exmo deleteUserOrder:orderID];
}

- (void)pairDidChange:(NSArray *)pair;
{
    self.pair = pair;
    [self.exmo getInfoForMarket];
    if (self.dictionaryOfBalance[self.pair[0]])
    {
        self.amountOfCryptoCurrency = [NSDecimalNumber decimalNumberWithString:self.dictionaryOfBalance[self.pair[0]]];
        self.amountOfSecondCurrency = [NSDecimalNumber decimalNumberWithString:self.dictionaryOfBalance[self.pair[1]]];
    }
    [self.delegate pairDidChange:self pair:pair];
    self.ordersHistoryArray = nil;
    [self historyDidLoad:self.exmo withHistoryArray:self.ordersHistoryArray error:nil];
}

- (void)createOrderForType:(NSString *)type withQuantity:(NSDecimalNumber *)quantity withPrice:(NSDecimalNumber *)price
{
    NSLog(@"\npair -> %@_%@\nquantity -> %@\nprice -> %@\ntype -> %@",self.pair[0],self.pair[1],quantity,price,type);
    [self.exmo createOrderWithPair:self.pair[0] and:self.pair[1] withType:type withQuantity:quantity withPrice:price];
}

- (void)loadMinuteHistoryForGraph
{
    [self.exmo getCurrencyHistoryForPair:self.pair withParametr:@"minute" withLimit:@60];
}

- (void)loadHourHistoryForGraph
{
    [self.exmo getCurrencyHistoryForPair:self.pair withParametr:@"hour" withLimit:@48];
}

- (void)loadDayHistoryForGraph
{
    [self.exmo getCurrencyHistoryForPair:self.pair withParametr:@"day" withLimit:@30];
}


#pragma mark - Private

- (void)setup
{
	[self pairDidChange:@[@"BTC",@"USD"]];
    NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
    if (![userDef objectForKey:@"api_key"])
    {
        [self showAuthorisationController];
    }
    else
    {
        [self getArrayOfCurrenciesAmount];
    }
}

- (void)getArrayOfCurrenciesAmount
{
    NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
    if ([userDef objectForKey:@"api_key"])
    {
        [self.exmo getUserInfo];
    }
}

- (void)findCurrenciesWhichNonNilInUserBalance:(NSDictionary *)balance
{
	self.dictionaryOfBalance = [[NSMutableDictionary alloc]init];
	for (NSString *key in [balance[@"balances"] allKeys])
	{
		if (![balance[@"balances"][key] isEqualToString:@"0"])
		{
			[self.dictionaryOfBalance setObject:balance[@"balances"][key] forKey:key];
		}
	}
	self.arrayOfCurrenciesNameWhichBalanceIsNonNull = [self.dictionaryOfBalance allKeys];
	NSLog(@"Non null currency array did download");
}

- (void)findPairAmountInBalanceInUserBalance:(NSDictionary *)balance
{
	self.dictionaryOfBalance = balance[@"balances"];
	
	self.amountOfCryptoCurrency = [NSDecimalNumber decimalNumberWithString:self.dictionaryOfBalance[self.pair[0]]];
	self.amountOfSecondCurrency = [NSDecimalNumber decimalNumberWithString:self.dictionaryOfBalance[self.pair[1]]];
}

- (NSArray *)sortHistoryOrdersArray:(NSArray *)historyOrdersArray
{
	NSMutableArray *array = [NSMutableArray arrayWithArray:self.ordersHistoryArray];
	[array addObjectsFromArray:historyOrdersArray];
	[array sortUsingComparator:^NSComparisonResult(NSDictionary *obj1, NSDictionary *obj2) {
		NSNumber *date1 = obj1[@"date"];
		NSNumber *date2 = obj2[@"date"];
		if (date1.intValue >= date2.intValue)
		{
			return NSOrderedAscending;
		}
		return NSOrderedDescending;
	}];
	return [array copy];
}

- (NSMutableArray *)getOpenOrdersMutableArrayFromOrdersDictionary:(NSDictionary *)openOrdersDictionary
{
	NSMutableArray *mutableArrayOfOpenOrders = [NSMutableArray new];
	for (NSString *key in [openOrdersDictionary allKeys])
	{
		if ([openOrdersDictionary[key] isKindOfClass:[NSArray class]])
		{
			[mutableArrayOfOpenOrders addObjectsFromArray:openOrdersDictionary[key]];
		}
	}
	return mutableArrayOfOpenOrders;
}

- (NSArray *)sortOpenOrdersArray:(NSMutableArray *)openOrdersArray
{
	NSArray *ar = [openOrdersArray sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *obj1, NSDictionary *obj2) {
		NSNumber *num1 = obj1[@"created"];
		NSNumber *num2 = obj2[@"created"];
		if (num1.doubleValue >= num2.doubleValue)
		{
			return  NSOrderedAscending;
		}
		return NSOrderedDescending;
	}];
	return ar;
}

- (NSArray *)fetchArrayOfCurrenciesWithController:(NSFetchedResultsController *)controller
{
	NSError *error;
	controller.fetchRequest.predicate = nil;
	if (![controller performFetch:&error]) {
		NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
		return nil;
	}
	return controller.fetchedObjects;
}

- (Currencies *)fetchCurrencyWithController:(NSFetchedResultsController *)controller
{
	NSError *error;
	controller.fetchRequest.predicate =
	[NSPredicate predicateWithFormat:@"firstCurrencyShortName = %@ AND secondCurrencyShortName = %@",self.pair[0],self.pair[1]];
	if (![controller performFetch:&error]) {
		NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
		return nil;
	}
	return controller.fetchedObjects.lastObject;
}


#pragma mark - CEAuthorisationViewControllerDelegate

- (void)authControllerDidClose
{
    [self getArrayOfCurrenciesAmount];
}


#pragma mark - FetchResultControllerDelegate

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    dispatch_async(dispatch_get_main_queue(), ^{
		NSArray *currenciesArray = [self fetchArrayOfCurrenciesWithController:controller];
        [self.delegate currencyDataDidUpdate:self with:currenciesArray];
		
		Currencies *currency = [self fetchCurrencyWithController:controller];
        self.changeCurrency = [NSDecimalNumber decimalNumberWithString:currency.buyPrice];
    });
}


#pragma mark - EmoNetworkDelegate

- (void)userInfoDidRecived:(CEExmoNetwork *)exmo withData:(NSData *)data
{
	if (!data)
	{
		return;
	}
    [self downloadArrayOfUserOpenOrders];
    NSDictionary *balance = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    if (balance[@"error"])
    {
        NSLog(@"%@",balance[@"error"]);
    }
	[self findCurrenciesWhichNonNilInUserBalance:balance];
	[self findPairAmountInBalanceInUserBalance:balance];
	[self.delegate userDataDidUpdate:self withBalance:[self.dictionaryOfBalance copy]
					   andCurrencies:self.arrayOfCurrenciesNameWhichBalanceIsNonNull];
}

- (void)openOrdersDidLoad:(CEExmoNetwork *)exmo withOpenOrders:(NSDictionary *)openOrdersDictionary
{
    if (openOrdersDictionary[@"error"])
    {
        NSLog(@"openOrdersDidLoad %@",openOrdersDictionary[@"error"]);
        [self.delegate shouldShowAlert:self withMessage:openOrdersDictionary[@"error"]];
    }
	NSMutableArray *mutableArrayOfOpenOrders = [self getOpenOrdersMutableArrayFromOrdersDictionary:openOrdersDictionary];
    self.arrayOfOpenOrders = [self sortOpenOrdersArray:mutableArrayOfOpenOrders];
	
    [self.delegate openOrdersDidLoad:self withOpenOrders:self.arrayOfOpenOrders];
    NSLog(@"Open orders did download");
}

- (void)orderDidCreated:(CEExmoNetwork *)exmo withOrderID:(NSNumber *)orderId withResult:(NSString *)result
                                                                            withError:(NSString *)error
{
    if (error.length > 0)
    {
        NSLog(@"Order creation did wrong: %@",error);
        [self.delegate notCreateOrder:self withError:error];
        return;
    }
    NSLog(@"Order did create: %@  %@",result,orderId);
    [self.delegate orderDidCreated:self withResult:result];
    [self downloadArrayOfUserOpenOrders];
}

- (void)orderDidDelete:(CEExmoNetwork *)exmo WithResult:(NSString *)result withError:(NSString *)error
{
    if (error.length > 0)
    {
        NSLog(@"Order not delete. Error: %@",error);
    }
    NSLog(@"Order did delete with result: %@",result);
    [self.delegate orderDidDelete:self WithResult:result withError:error];
}

- (void)historyDidLoad:(CEExmoNetwork *)exmo withHistoryArray:(NSArray *)historyArray error:(NSString *)error
{
    if (error.length > 0)
    {
        NSLog(@" historyDidLoad %@",error);
        [self.delegate shouldShowAlert:self withMessage:error];
    }
    NSLog(@"History array did download ");
	
    self.ordersHistoryArray = [self sortHistoryOrdersArray:historyArray];
    [self.delegate historyDidLoad:self withHistoryArray:self.ordersHistoryArray error:error];
}

- (void)currencyHistoryDidLoad:(CEExmoNetwork *)exmo withHistory:(NSDictionary *)historyDictionary forPair:(NSArray *)pair
{
    [self.delegate currencyHistoryDidUpdate:self withHistory:historyDictionary];
}

@end
