//
//  CEButtonDownLineView.h
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 14.12.2017.
//  Copyright © 2017 Artem Zabludovsky. All rights reserved.
//


#import <UIKit/UIKit.h>


@interface CEButtonDownLineView : UIView


- (instancetype)initWithColor:(UIColor *)color andHeight:(NSNumber *)height;

@end
