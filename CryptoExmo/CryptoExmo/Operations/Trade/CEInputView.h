//
//  CEInputView.h
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 14.12.2017.
//  Copyright © 2017 Artem Zabludovsky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Masonry.h>

@interface CEInputView : UIView<NSCopying>

@property (nonatomic, strong) UILabel *thisViewNameLabel;
@property (nonatomic, strong) UILabel *nextViewNameLabel;
@property (nonatomic, strong) UILabel *prevViewNameLabel;

@property (nonatomic, strong) UILabel *leftTextInputLabel;
@property (nonatomic, strong) UILabel *rightTextInputLabel;

@property (nonatomic, strong) UIView *leftFieldView;
@property (nonatomic, strong) UIView *rightFieldView;

@property (nonatomic, strong) UILabel *leftTextFieldLabel;
@property (nonatomic, strong) UILabel *rightTextFieldLabel;

@property (nonatomic, strong) UITextField *leftTextField;
@property (nonatomic, strong) UITextField *rightTextField;

/** Текущая пара валют
 example: pair[0] - BTC pair[1] - USD --->pair = BTC_USD
 @abstract Пара валют*/
@property (nonatomic, copy) NSArray *pair;

- (instancetype)init;

/** Следующий inputView(Market --> Limit) и наоборот*/
- (CEInputView *)createNextInputViewWithCopying;


@end
