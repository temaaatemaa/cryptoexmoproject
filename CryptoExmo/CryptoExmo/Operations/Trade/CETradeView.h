//
//  CETradeView.h
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 16.12.2017.
//  Copyright © 2017 Artem Zabludovsky. All rights reserved.
//


#import <UIKit/UIKit.h>
#include "CECryptoOperationsViewModel.h"


static NSString *const CEStringOfMarket = @"Market";
static NSString *const CEStringOfLimit = @"Limit";


@interface CETradeView : UIView

/** Текущая пара валют
 example: pair[0] - BTC pair[1] - USD --->pair = BTC_USD
 @abstract Пара валют*/
@property (nonatomic, copy) NSArray *pair;

/**  Инициализаци
 @param viewModel CECryptoOperationsViewModel
 @param viewController вьюконтроллер для показа алертов*/
- (instancetype)initWithViewModel:(CECryptoOperationsViewModel *)viewModel
                andViewController:(UIViewController *)viewController;

/**  Уведомление что баланс изменился
 @param dictOfBalance NSdictionary баланса пользователя
 @param arrOfCurrencyNonNil Массив криптовалют, баланс на данный момент которых больше 0*/
- (void)userDataDidUpdate:(NSDictionary *)dictOfBalance andArray:(NSArray *)arrOfCurrencyNonNil;

/**  Уведомление что создание ордера завершилось ошибкой
 @param error Ошибка*/
- (void)errorWithCreatingError:(NSString *)error;

/**  Уведомление что ордер создался успешно
 @param result Результат*/
- (void)orderDidCreatedWithResult:(BOOL)result;

/**  Уведомление о успешной загрузке истории курса валюты
 @param historyDictionary История курса валют*/
- (void)updateCurrencyHistory:(NSDictionary *)historyDictionary;

@end
