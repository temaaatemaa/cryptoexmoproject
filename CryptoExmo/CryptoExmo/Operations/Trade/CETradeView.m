//
//  CETradeView.m
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 16.12.2017.
//  Copyright © 2017 Artem Zabludovsky. All rights reserved.
//


#import "CETradeView.h"
#import <Masonry.h>
#import "CEInputView.h"
#import "CECryptoOperationsViewController.h"
#import "UIColor+CEColor.h"
#import "UIFont+CEFont.h"
#import "CEGraph.h"
#import "CEExmoGraph.h"
#import "CEButtonDownLineView.h"
#import <AudioToolbox/AudioToolbox.h>
#import "CESignsOfCurrencies.h"
#import "CEDevice.h"



static CGFloat const CEElementsOffsetFromRightLeftBounced = 0.f;
static CGFloat const CEBalanceViewHeight = 35.f;
static CGFloat const CEElementsOffset = 7.f;
static CGFloat const CETopElementsOffset = 10.f;
static CGFloat const CEInputViewHeight = 135.f;
static CGFloat const CEButtonsHeight = 35.f;
static NSInteger const CETagOfLeftInputView = 111;
static NSInteger const CETagOfRightInputView = 333;
static NSInteger const CETagOfCenterInputView = 222;
static NSTimeInterval const CETimeOfChangeInputViewAnimation = 0.3;
static CGFloat const CEPersentageOfInputTransitionDone = 0.3;
static CGFloat const CEGraphButtonsHeight = 20.f;
static NSString *const CENameOfMinuteGraphPeriodButton = @"Day";
static NSString *const CENameOfHourGraphPeriodButton = @"Week";
static NSString *const CENameOfDayGraphPeriodButton = @"Month";
static CGFloat const CEHeightOflineForButtons = 2.f;


@interface CETradeView()
@property (nonatomic, strong) UIView *balanceView;
@property (nonatomic, strong) UILabel *balanceLabel;
@property (nonatomic, strong) UILabel *firstCurrBalanceLabel;
@property (nonatomic, strong) UILabel *secondCurrBalanceLabel;

@property (nonatomic, strong) CEInputView *inputView;
@property (nonatomic, strong) CEInputView *nextLeftInputView;
@property (nonatomic, strong) CEInputView *nextRightInputView;
@property (nonatomic, strong) NSNumber *inputViewOffsetFromStandardLocation;

@property (nonatomic, strong) UIButton *rightButton;
@property (nonatomic, strong) UIButton *leftButton;

@property (nonatomic, strong) CEButtonDownLineView *lineForButtons;
@property (nonatomic, strong) UIButton *graphMinuteButton;
@property (nonatomic, strong) UIButton *graphHourButton;
@property (nonatomic, strong) UIButton *graphDayButton;
@property (nonatomic, strong) UIView *graph;
@property (nonatomic, assign,getter=isExmoGraph) BOOL exmoGraph;

@property (nonatomic, assign) BOOL isMooving;
@property (nonatomic, assign) CGPoint beginMovePoint;
@property (nonatomic, assign) CGPoint nowMovePoint;

@property (nonatomic, strong) MASConstraint *leftConstraintOfInputView;

@property (nonatomic, strong) CECryptoOperationsViewModel *viewModel;
@property (nonatomic, weak) UIViewController *viewController;
@end


@implementation CETradeView


#pragma mark - Lifecycle

- (instancetype)initWithViewModel:(CECryptoOperationsViewModel *)viewModel
				andViewController:(UIViewController *)viewController;
{
	self = [super init];
	if (self)
	{
		_lineForButtons = [[CEButtonDownLineView alloc]initWithColor:[UIColor ceAdditionalColor]
														 andHeight:@(CEHeightOflineForButtons)];
		
		_viewModel = viewModel;
		_viewController = viewController;
		
		[self chooseGraph];
		[self setupGraph];
		
		[self setupBalanceView];
		[self setupInputView];
		[self setupBuySellButtons];
		[self setupGraphButtons];
	}
	return self;
}

-(void)didMoveToWindow
{
	if (self.exmoGraph)
	{
		[((CEExmoGraph *)self.graph) loadTimeInterval:@"day" forFirstCurrency:self.pair[0] secondCurrency:self.pair[1]];
	}
}
- (void)updateConstraints
{
	[_balanceView mas_makeConstraints:^(MASConstraintMaker *make) {
		if ([CEDevice isIphoneWithSmallScreen]) {
			make.top.equalTo(self.mas_top).with.offset(CEElementsOffset-3);
		}
		else
		{
			make.top.equalTo(self.mas_top).with.offset(CEElementsOffset);
		}
		make.left.equalTo(self.mas_left).with.offset(CEElementsOffsetFromRightLeftBounced);
		make.right.equalTo(self.mas_right).with.offset(-CEElementsOffsetFromRightLeftBounced);
		make.height.equalTo(@(CEBalanceViewHeight));
	}];
	
	[_balanceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
		make.centerY.equalTo(_balanceView.mas_centerY);
		if ([CEDevice isIphoneWithSmallScreen]) {
			make.left.equalTo(_balanceView.mas_left).with.offset(CEElementsOffset-3);

		}
		else
		{
			make.left.equalTo(_balanceView.mas_left).with.offset(CEElementsOffset);
		}
	}];
	
	[_firstCurrBalanceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
		make.centerY.equalTo(_balanceView.mas_centerY);
		make.centerX.equalTo(_balanceView.mas_centerX);
	}];
	
	[_secondCurrBalanceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
		make.centerY.equalTo(_balanceView.mas_centerY);
		make.right.equalTo(_balanceView.mas_right).with.offset(-CEElementsOffset);
	}];
	
	[_inputView mas_remakeConstraints:^(MASConstraintMaker *make) {
		make.top.equalTo(_balanceView.mas_bottom).with.offset(CEElementsOffset);
		self.leftConstraintOfInputView = make.left.equalTo(self.mas_left).with.offset(self.inputViewOffsetFromStandardLocation.floatValue+
																					  CEElementsOffsetFromRightLeftBounced);

		if ([CEDevice isIphoneWithSmallScreen]) {
			make.height.equalTo(@(CEInputViewHeight-5));

		}
		else
		{
			make.height.equalTo(@(CEInputViewHeight));
		}
		make.width.equalTo(self.mas_width).with.offset(-2*CEElementsOffsetFromRightLeftBounced);
	}];
	
	
	[_leftButton mas_makeConstraints:^(MASConstraintMaker *make) {
		make.left.equalTo(self.mas_left).with.offset(0);

		if ([CEDevice isIphoneWithSmallScreen]) {
			make.height.equalTo(@(CEButtonsHeight-5));
			make.top.equalTo(_inputView.mas_bottom).with.offset(CEElementsOffset-3);
		}
		else
		{
			make.height.equalTo(@(CEButtonsHeight));
			make.top.equalTo(_inputView.mas_bottom).with.offset(CEElementsOffset);
		}
	}];
	[_rightButton mas_makeConstraints:^(MASConstraintMaker *make) {

		if ([CEDevice isIphoneWithSmallScreen]) {
			make.top.equalTo(_inputView.mas_bottom).with.offset(CEElementsOffset-3);

		}
		else
		{
			make.top.equalTo(_inputView.mas_bottom).with.offset(CEElementsOffset);
		}
		make.left.equalTo(_leftButton.mas_right).with.offset(3);
		make.right.equalTo(self.mas_right).with.offset(0);
		
		make.height.equalTo(_leftButton.mas_height);
		make.width.equalTo(_leftButton.mas_width);
	}];
	
	[_graphMinuteButton mas_makeConstraints:^(MASConstraintMaker *make) {
		if ([CEDevice isIphoneWithSmallScreen]) {
			make.top.equalTo(_leftButton.mas_bottom).with.offset(CEElementsOffset-3);

		}
		else
		{
			make.top.equalTo(_leftButton.mas_bottom).with.offset(CEElementsOffset);
		}
		make.left.equalTo(self);
		make.height.equalTo(@(CEGraphButtonsHeight));
	}];
	[_graphHourButton mas_makeConstraints:^(MASConstraintMaker *make) {
		make.top.equalTo(_graphMinuteButton);
		make.left.equalTo(_graphMinuteButton.mas_right).with.offset(1);
		make.width.equalTo(_graphMinuteButton);
		make.height.equalTo(_graphMinuteButton);
	}];
	[_graphDayButton mas_makeConstraints:^(MASConstraintMaker *make) {
		make.top.equalTo(_graphMinuteButton);
		make.left.equalTo(_graphHourButton.mas_right).with.offset(1);
		make.right.equalTo(self);
		make.width.equalTo(_graphMinuteButton);
		make.height.equalTo(_graphMinuteButton);
	}];
	[_graph mas_makeConstraints:^(MASConstraintMaker *make) {
		make.top.equalTo(_graphMinuteButton.mas_bottom).with.offset(0);
		make.right.equalTo(self);
		make.left.equalTo(self);
		make.bottom.equalTo(self);
	}];
	[super updateConstraints];
}


#pragma mark - Custom Accessors

- (void)setPair:(NSArray *)pair
{
	_pair = pair;
	self.inputView.pair = pair;
	[self updatedBalanceLabels];
	[self setMinuteGraph];
}


#pragma mark - Public

- (void)updateCurrencyHistory:(NSDictionary *)historyDictionary
{
	if (!self.isExmoGraph)
	{
		((CEGraph *)self.graph).historyData = historyDictionary;
	}
}

- (void)orderDidCreatedWithResult:(BOOL)result
{
	[self showAlert:@"Order created!" message:@"Now you can check created order in ""orders"" tab.\nGood luck!"];
}

- (void)errorWithCreatingError:(NSString *)error
{
	[self showAlert:@"Order error!" message:error];
}

- (void)userDataDidUpdate:(NSDictionary *)dictOfBalance andArray:(NSArray *)arrOfCurrencyNonNil
{
	[self updatedBalanceLabels];
}


#pragma mark - Private

- (void)chooseGraph
{
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	
	if ([[userDefaults objectForKey:@"graph"]isEqualToString:@"exmo"])
	{
		self.exmoGraph = YES;
	}
	else
	{
		self.exmoGraph = NO;
	}
}

- (void)setupGraph
{
	if (self.isExmoGraph)
	{
		_graph = [[CEExmoGraph alloc]initWithFrame:CGRectNull];
		//[()_graph loadTimeInterval:@"day" forFirstCurrency:self.pair[0] secondCurrency:self.pair[1]];
	}
	else
	{
		_graph = [[CEGraph alloc]initWithFrame:CGRectNull];
	}
	
	[self addSubview:_graph];
}

- (void)setupBalanceView
{
	_balanceView = [UIView new];
	_balanceLabel = [UILabel new];
	_firstCurrBalanceLabel = [UILabel new];
	_secondCurrBalanceLabel = [UILabel new];
	
	[self addSubview:_balanceView];
	[_balanceView addSubview:_balanceLabel];
	[_balanceView addSubview:_firstCurrBalanceLabel];
	[_balanceView addSubview:_secondCurrBalanceLabel];
	
	_balanceView.backgroundColor = [UIColor ceMainColor];
	_balanceView.layer.cornerRadius = 0;
	_balanceView.layer.borderWidth = 0;
	_balanceView.layer.borderColor = [UIColor blueColor].CGColor;
	_balanceLabel.text = @"Balance:";
	_balanceLabel.textColor = [UIColor ceFontColor];
	_balanceLabel.font = [UIFont systemFontOfSize:17 weight:UIFontWeightLight];
	_firstCurrBalanceLabel.text = @"฿ 0.000000";
	_secondCurrBalanceLabel.text = @"$ 0.00";
	_firstCurrBalanceLabel.font = [UIFont systemFontOfSize:17 weight:UIFontWeightLight];
	_secondCurrBalanceLabel.font = [UIFont systemFontOfSize:17 weight:UIFontWeightLight];
	_firstCurrBalanceLabel.textColor = [UIColor ceFontColor];
	_secondCurrBalanceLabel.textColor = [UIColor ceFontColor];
}

- (void)setupInputView
{
	_inputView = [CEInputView new];
	_inputViewOffsetFromStandardLocation = @0;
	[self addSubview:_inputView];
	_inputView.backgroundColor = [UIColor ceMainColor];
	_inputView.tag = CETagOfCenterInputView;
}

- (void)setupBuySellButtons
{
	_rightButton = [UIButton new];
	_leftButton = [UIButton new];
	
	[self addSubview:_rightButton];
	[self addSubview:_leftButton];
	
	[_rightButton setTitle:@"SELL" forState:UIControlStateNormal];
	[_leftButton setTitle:@"BUY" forState:UIControlStateNormal];
	_rightButton.backgroundColor = [UIColor ceMainColor];
	_leftButton.backgroundColor = [UIColor ceMainColor];
	_rightButton.layer.cornerRadius = 2;
	_leftButton.layer.cornerRadius = 2;
	
	[_leftButton addTarget:self action:@selector(buyButtonClicked) forControlEvents:UIControlEventTouchDown];
	[_rightButton addTarget:self action:@selector(sellButtonClicked) forControlEvents:UIControlEventTouchDown];
}

- (void)setupGraphButtons
{
	_graphMinuteButton = [UIButton new];
	_graphHourButton = [UIButton new];
	_graphDayButton = [UIButton new];
	
	[self addSubview:_graphMinuteButton];
	[self addSubview:_graphHourButton];
	[self addSubview:_graphDayButton];
	[_graphMinuteButton addSubview:_lineForButtons];
	
    _graphMinuteButton.backgroundColor = [UIColor ceMainColor];
    _graphHourButton.backgroundColor = [UIColor ceMainColor];
    _graphDayButton.backgroundColor = [UIColor ceMainColor];
    
    NSDictionary *attrsDictionary = @{
                                      NSForegroundColorAttributeName : [UIColor whiteColor],
                                      NSFontAttributeName : [UIFont ceFontForGraphPeriodButtonsInTradeView]
                                      };
	NSAttributedString *minTitle;
	NSAttributedString *hourTitle;
	NSAttributedString *dayTitle;
	
	if (self.exmoGraph)
	{
		minTitle = [[NSAttributedString alloc] initWithString:CENameOfMinuteGraphPeriodButton
												   attributes:attrsDictionary];
		hourTitle = [[NSAttributedString alloc] initWithString:CENameOfHourGraphPeriodButton
													attributes:attrsDictionary];
		dayTitle = [[NSAttributedString alloc] initWithString:CENameOfDayGraphPeriodButton
												   attributes:attrsDictionary];
	}
	else
	{
		minTitle = [[NSAttributedString alloc] initWithString:@"Min"
												   attributes:attrsDictionary];
		hourTitle = [[NSAttributedString alloc] initWithString:@"Hour"
													attributes:attrsDictionary];
		dayTitle = [[NSAttributedString alloc] initWithString:@"Day"
												   attributes:attrsDictionary];
	}
    
    [_graphMinuteButton setAttributedTitle:minTitle forState:UIControlStateNormal];
    [_graphHourButton setAttributedTitle:hourTitle forState:UIControlStateNormal];
    [_graphDayButton setAttributedTitle:dayTitle forState:UIControlStateNormal];
    
    [_graphMinuteButton addTarget:self action:@selector(setMinuteGraph) forControlEvents:UIControlEventTouchDown];
    [_graphHourButton addTarget:self action:@selector(setHourGraph) forControlEvents:UIControlEventTouchDown];
    [_graphDayButton addTarget:self action:@selector(setDayGraph) forControlEvents:UIControlEventTouchDown];
}

- (void)putLineToButton:(UIButton *)graphButton
{
	[self.lineForButtons removeFromSuperview];
	self.lineForButtons = [[CEButtonDownLineView alloc]initWithColor:[UIColor ceAdditionalColor]
														 andHeight:@(CEHeightOflineForButtons)];
	[graphButton addSubview:self.lineForButtons];
	[self setNeedsLayout];
	AudioServicesPlaySystemSound(1519);
}

- (void)createTwoInputViewForAmimation
{
    self.nextRightInputView = [self.inputView createNextInputViewWithCopying];
    self.nextLeftInputView = [self.inputView createNextInputViewWithCopying];
    
    self.nextRightInputView.tag = CETagOfRightInputView;
    self.nextLeftInputView.tag = CETagOfLeftInputView;
    
    [self addSubview:self.nextRightInputView];
    [self addSubview:self.nextLeftInputView];
    
    self.nextRightInputView.backgroundColor = [UIColor ceMainColor];
    self.nextLeftInputView.backgroundColor = [UIColor ceMainColor];
    
    [self.nextRightInputView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.inputView.mas_centerY);
        make.left.equalTo(self.inputView.mas_right).with.offset(CEElementsOffsetFromRightLeftBounced);
        make.size.equalTo(self.inputView);
    }];
    [self.nextLeftInputView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.inputView.mas_centerY);
        make.size.equalTo(self.inputView);
        make.right.equalTo(self.inputView.mas_left).with.offset(-CEElementsOffsetFromRightLeftBounced);
    }];
	[self.inputView.thisViewNameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
		make.top.equalTo(self.inputView.mas_top).with.offset(CETopElementsOffset);
		make.centerX.equalTo(self.mas_centerX);
	}];
	
	[self.nextRightInputView.thisViewNameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
		make.top.equalTo(self.nextRightInputView.mas_top).with.offset(CETopElementsOffset);
		make.centerX.equalTo(self.mas_centerX);
	}];
	
	[self.nextLeftInputView.thisViewNameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
		make.top.equalTo(self.nextLeftInputView.mas_top).with.offset(CETopElementsOffset);
		make.centerX.equalTo(self.mas_centerX);
	}];
	
}

/** Смена inputView
 @param isRight = YES => новый inputView - правый;isRight = NO => новый inputView - левый*/
- (void)changeInputViewToRightView:(BOOL)isRight
{
    __block CEInputView *view;
    __block CEInputView *deleteView;
    if (isRight)
    {
        view = self.nextRightInputView;
        deleteView = self.nextLeftInputView;
    }
    else
    {
        view = self.nextLeftInputView;
        deleteView = self.nextRightInputView;
    }
    [view mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_balanceView.mas_bottom).with.offset(CEElementsOffset);
        make.left.equalTo(self.mas_left).with.offset(CEElementsOffsetFromRightLeftBounced);
        make.height.equalTo(@(CEInputViewHeight));
        make.width.equalTo(self.mas_width).with.offset(-2*CEElementsOffsetFromRightLeftBounced);
    }];
    [self.inputView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(view.mas_centerY);
        make.size.equalTo(view);
        if (isRight)
        {
            make.right.equalTo(view.mas_left).with.offset(-CEElementsOffsetFromRightLeftBounced);
        }
        else
        {
            make.left.equalTo(view.mas_right).with.offset(CEElementsOffsetFromRightLeftBounced);
        }
    }];
    [UIView animateWithDuration:CETimeOfChangeInputViewAnimation animations:^{
        [self layoutIfNeeded];
        view.thisViewNameLabel.alpha = 1;
        view.nextViewNameLabel.alpha = 1;
        view.prevViewNameLabel.alpha = 1;
    } completion:^(BOOL finished) {
        if (finished)
        {
            [self deleteUnnessesaryView:deleteView thenSetNewInputView:view];
            [self updateConstraints];
            [self layoutIfNeeded];
        }
    }];
}

- (void)deleteUnnessesaryView:(CEInputView *) deleteView thenSetNewInputView:(CEInputView *)view
{
	self.inputView.tag = CETagOfLeftInputView;
	id tmp = self.inputView;
	
	[deleteView removeFromSuperview];
	
	self.inputView = nil;
	self.inputView = view;
	self.inputView.tag = CETagOfCenterInputView;
	
	[tmp removeFromSuperview];
	
	deleteView = nil;
	tmp = nil;
	
	[self deleteViewByTags];
	
	self.nextRightInputView = nil;
	self.nextLeftInputView = nil;
}

- (void)deleteViewByTags
{
	for(UIView *subview in [self subviews])
	{
		if((subview.tag==CETagOfLeftInputView)||(subview.tag==CETagOfRightInputView))
		{
			[subview removeFromSuperview];
		}
	}
}

- (int)checkUserInput:(NSString *)textFieldText
{
    if ([textFieldText componentsSeparatedByString:@","].count > 2)
    {
        return 0;
    }
    if (textFieldText.length < 1)
    {
        return 0;
    }
    if ([textFieldText isEqualToString:@","])
    {
        return 0;
    }
    return 1;
}

- (void)showAlert:(NSString *)title message:(NSString *)message
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:okButton];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.viewController presentViewController:alertController animated:YES completion:nil];
    });
}

- (void)showWrongInputAlert
{
    [self showAlert:@"Wrong number" message:@"Check for correction of input number!"];
}

- (void)updatedBalanceLabels
{
    NSDecimalNumberHandler *behavior = [NSDecimalNumberHandler decimalNumberHandlerWithRoundingMode:NSRoundDown
                                                                                              scale:5
                                                                                   raiseOnExactness:NO
                                                                                    raiseOnOverflow:NO
                                                                                   raiseOnUnderflow:NO
                                                                                raiseOnDivideByZero:NO];
    
    NSDecimalNumber *roundedNumberOfCrypta = [self.viewModel.amountOfCryptoCurrency decimalNumberByRoundingAccordingToBehavior:behavior];
    NSDecimalNumber *roundedNumberOfDollar = [self.viewModel.amountOfSecondCurrency decimalNumberByRoundingAccordingToBehavior:behavior];
    
    NSString *leftCurrencyDisplaySign = [CESignsOfCurrencies changeShortNameForSign:self.pair[0]];
    NSString *rightCurrencyDisplaySign = [CESignsOfCurrencies changeShortNameForSign:self.pair[1]];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.firstCurrBalanceLabel.text = [NSString stringWithFormat:@"%@ %@",leftCurrencyDisplaySign,roundedNumberOfCrypta];
        self.secondCurrBalanceLabel.text = [NSString stringWithFormat:@"%@ %@",rightCurrencyDisplaySign,roundedNumberOfDollar];
    });
}

- (void)changingInputViewsAlphaByMoovingTouch
{
	CGFloat alphaForInputViewNameLabels = 1-2*fabs(self.inputViewOffsetFromStandardLocation.doubleValue/CGRectGetWidth(self.frame));
	self.inputView.prevViewNameLabel.alpha = alphaForInputViewNameLabels;
	self.inputView.nextViewNameLabel.alpha = alphaForInputViewNameLabels;
	self.inputView.thisViewNameLabel.alpha = alphaForInputViewNameLabels;
	
	self.nextRightInputView.thisViewNameLabel.alpha = -(self.inputViewOffsetFromStandardLocation.doubleValue+
														CGRectGetWidth(self.frame)/2)/CGRectGetWidth(self.frame);
	self.nextLeftInputView.thisViewNameLabel.alpha = (self.inputViewOffsetFromStandardLocation.doubleValue-
													  CGRectGetWidth(self.frame)/2)/CGRectGetWidth(self.frame);
	self.nextLeftInputView.nextViewNameLabel.alpha = self.nextLeftInputView.thisViewNameLabel.alpha;
	self.nextRightInputView.prevViewNameLabel.alpha = self.nextRightInputView.thisViewNameLabel.alpha;
}

+ (BOOL)requiresConstraintBasedLayout
{
	return YES;
}


#pragma mark - TouchesEvents

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint point = [touch locationInView:self];
    if (CGRectContainsPoint(self.inputView.frame, point))
    {
        self.isMooving = YES;
        [self createTwoInputViewForAmimation];
		[self changingInputViewsAlphaByMoovingTouch];
    }
}

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    if (self.isMooving)
    {
        UITouch *touch = [touches anyObject];
        CGPoint nowPoint = [touch locationInView:self];
        CGPoint lastPoint = [touch previousLocationInView:self];
        NSNumber *raznica = @(nowPoint.x - lastPoint.x);
        self.inputViewOffsetFromStandardLocation = @(raznica.doubleValue + self.inputViewOffsetFromStandardLocation.doubleValue);

		[self changingInputViewsAlphaByMoovingTouch];
        

        [self setNeedsUpdateConstraints];
        [self updateConstraintsIfNeeded];
        [self layoutIfNeeded];
    }
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    //CLOSE KEYBORD
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
    
    if (self.isMooving)
    {
        self.isMooving = NO;
        CGFloat persentage = self.inputViewOffsetFromStandardLocation.doubleValue/CGRectGetWidth(self.frame);
        if (persentage < -CEPersentageOfInputTransitionDone)
        {
            [self changeInputViewToRightView:YES];
        }else if (persentage > CEPersentageOfInputTransitionDone)
        {
            [self changeInputViewToRightView:NO];
        }else
        {
            self.leftConstraintOfInputView.offset = CEElementsOffsetFromRightLeftBounced;
            [UIView animateWithDuration:CETimeOfChangeInputViewAnimation animations:^{
                self.inputView.thisViewNameLabel.alpha = 1;
                self.inputView.nextViewNameLabel.alpha = 1;
                self.inputView.prevViewNameLabel.alpha = 1;
                [self layoutIfNeeded];
			} completion:^(BOOL finished) {
				[self deleteViewByTags];
			}];
        }
        self.inputViewOffsetFromStandardLocation = @0;
    }
}


#pragma mark - ButtonsEvents

- (void)sellButtonClicked
{
    [self createOrderToSell:YES];
    AudioServicesPlaySystemSound(1520);
}

- (void)buyButtonClicked
{
    [self createOrderToSell:NO];
    AudioServicesPlaySystemSound(1520);
}

- (void)createOrderToSell:(BOOL)isSelling
{
    if ([self.inputView.thisViewNameLabel.text isEqualToString:CEStringOfMarket])
    {
        [self marketBuyOrSellWithIsSelling:isSelling];
    }
    if ([self.inputView.thisViewNameLabel.text isEqualToString:CEStringOfLimit])
    {
        [self limitBuyOrSellWithIsSelling:isSelling];
    }
}

- (void)limitBuyOrSellWithIsSelling:(BOOL)isSelling
{
    NSString *type = isSelling ? @"sell" : @"buy";
    
    if (![self checkUserInput:self.inputView.leftTextField.text])
    {
        [self showWrongInputAlert];
        return;
    }
    if (![self checkUserInput:self.inputView.rightTextField.text])
    {
        [self showWrongInputAlert];
        return;
    }
    NSString *quantityStr = [self.inputView.leftTextField.text stringByReplacingOccurrencesOfString:@","
                                                                                         withString:@"."];
    NSString *priceStr = [self.inputView.rightTextField.text stringByReplacingOccurrencesOfString:@","
                                                                                       withString:@"."];
    NSDecimalNumber *quantity = [NSDecimalNumber decimalNumberWithString:quantityStr];
    NSDecimalNumber *price = [NSDecimalNumber decimalNumberWithString:priceStr];
    [self.viewModel createOrderForType:type withQuantity:quantity withPrice:price];
}

- (void)marketBuyOrSellWithIsSelling:(BOOL)isSelling
{
    NSString *type;
    NSString *inputString;
    if (isSelling)
    {
        type = @"market_sell";
        inputString = self.inputView.rightTextField.text;
    }
    else
    {
        type = @"market_buy_total";
        inputString = self.inputView.leftTextField.text;
    }
    if (![self checkUserInput:inputString])
    {
        [self showWrongInputAlert];
        return;
    }
    NSString *quantityStr = [inputString stringByReplacingOccurrencesOfString:@","
                                                                   withString:@"."];
    NSDecimalNumber *quantity = [NSDecimalNumber decimalNumberWithString:quantityStr];
    NSDecimalNumber *price = [NSDecimalNumber decimalNumberWithString:@"0"];
    [self.viewModel createOrderForType:type withQuantity:quantity withPrice:price];
}

- (void)setMinuteGraph
{
	if (self.isExmoGraph)
	{
		[(CEExmoGraph *)_graph loadTimeInterval:@"day" forFirstCurrency:self.pair[0] secondCurrency:self.pair[1]];
	}
	else
	{
		[self.viewModel loadMinuteHistoryForGraph];
	}
    [self putLineToButton:self.graphMinuteButton];
}

- (void)setHourGraph
{
	if (self.isExmoGraph)
	{
		[(CEExmoGraph *)_graph loadTimeInterval:@"week" forFirstCurrency:self.pair[0] secondCurrency:self.pair[1]];
	}
	else
	{
		[self.viewModel loadHourHistoryForGraph];
	}
   [self putLineToButton:self.graphHourButton];
}

- (void)setDayGraph
{
	if (self.isExmoGraph)
	{
		[(CEExmoGraph *)_graph loadTimeInterval:@"month" forFirstCurrency:self.pair[0] secondCurrency:self.pair[1]];
	}
	else
	{
		[self.viewModel loadDayHistoryForGraph];
	}
    [self putLineToButton:self.graphDayButton];
}


@end
