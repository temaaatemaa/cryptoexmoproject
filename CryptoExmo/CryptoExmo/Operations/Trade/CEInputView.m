//
//  CEInputView.m
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 14.12.2017.
//  Copyright © 2017 Artem Zabludovsky. All rights reserved.
//

#import "CEInputView.h"
#import "UIColor+CEColor.h"
#import "CETradeView.h"

static CGFloat const CEElementsTopOffset = 10.f;
static CGFloat const CEFieldViewOffsetFromLeftRight = 10.f;
static CGFloat const CEFieldTextLabelOffsetLeft = 10.f;
static CGFloat const CEFieldTextLabelOffsetTop = 3.f;
static CGFloat const CETextFieldOffsetTopBottom = 5.f;
static CGFloat const CETextFieldOffsetLeftRight = 10.f;
static CGFloat const CENextPrevNameLabelOffsetLeftRight = 10.f;
static CGFloat const CEInputLabelOffsetLeftRight = 15.f;

@interface CEInputView ()<UITextViewDelegate>
@end

@implementation CEInputView


#pragma mark - Lifecycle

- (instancetype)init
{
	self = [super init];
	if (self)
	{
		self.layer.borderColor = [UIColor blueColor].CGColor;
		
		_thisViewNameLabel 		=   [UILabel new];
		_nextViewNameLabel 		=   [UILabel new];
		_prevViewNameLabel 		=   [UILabel new];
		_leftTextInputLabel 	=   [UILabel new];
		_rightTextInputLabel 	=  	[UILabel new];
		_leftFieldView 			=   [UIView new];
		_rightFieldView 		=   [UIView new];
		_leftTextFieldLabel 	=   [UILabel new];
		_rightTextFieldLabel	= 	[UILabel new];
		_leftTextField 			=	[UITextField new];
		_rightTextField 		=	[UITextField new];
		
		_thisViewNameLabel.text = CEStringOfMarket;
		_nextViewNameLabel.text = CEStringOfLimit;
		_prevViewNameLabel.text = _nextViewNameLabel.text;
		_thisViewNameLabel.font = [UIFont systemFontOfSize:17 weight:UIFontWeightMedium];
		_nextViewNameLabel.font = [UIFont systemFontOfSize:13 weight:UIFontWeightLight];
		_prevViewNameLabel.font = _nextViewNameLabel.font;
		_thisViewNameLabel.textColor = [UIColor ceFontColor];
		_nextViewNameLabel.textColor = [UIColor ceFontColor];
		_prevViewNameLabel.textColor = [UIColor ceFontColor];
		
		_leftTextInputLabel.text = @"USD to spend";
		_rightTextInputLabel.text = @"BTC to sell";
		_leftTextInputLabel.font = [UIFont systemFontOfSize:17 weight:UIFontWeightLight];
		_rightTextInputLabel.font = [UIFont systemFontOfSize:17 weight:UIFontWeightLight];
		_leftTextInputLabel.textColor = [UIColor ceFontColor];
		_rightTextInputLabel.textColor = [UIColor ceFontColor];
		
		_leftFieldView.backgroundColor = [UIColor whiteColor];
		_rightFieldView.backgroundColor = [UIColor whiteColor];
		_leftFieldView.layer.cornerRadius = 2;
		_rightFieldView.layer.cornerRadius = 2;
		
		_leftTextFieldLabel.text = @"USD";
		_rightTextFieldLabel.text = @"BTC";
		_leftTextFieldLabel.font = [UIFont systemFontOfSize:10 weight:UIFontWeightLight];
		_rightTextFieldLabel.font = [UIFont systemFontOfSize:10 weight:UIFontWeightLight];
		
		
		_leftTextField.backgroundColor = nil;
		_rightTextField.backgroundColor = nil;
		_leftTextField.text = @"0.00";
		_rightTextField.text = @"0.00";
		_leftTextField.clearsOnBeginEditing = YES;
		_rightTextField.clearsOnBeginEditing = YES;
		[[_leftTextField valueForKey:@"textInputTraits"] setValue:[UIColor ceAdditionalColor] forKey:@"insertionPointColor"];
		[[_rightTextField valueForKey:@"textInputTraits"] setValue:[UIColor ceAdditionalColor] forKey:@"insertionPointColor"];
		_leftTextField.font = [UIFont systemFontOfSize:17 weight:UIFontWeightMedium];
		_rightTextField.font = [UIFont systemFontOfSize:17 weight:UIFontWeightMedium];
		_leftTextField.delegate = (id<UITextFieldDelegate>)self;
		_rightTextField.delegate = (id<UITextFieldDelegate>)self;
		_leftTextField.keyboardType = UIKeyboardTypeDecimalPad;
		_rightTextField.keyboardType = UIKeyboardTypeDecimalPad;
		_leftTextField.keyboardAppearance = UIKeyboardAppearanceDark;
		_rightTextField.keyboardAppearance = UIKeyboardAppearanceDark;
		
		[self addSubview:_thisViewNameLabel];
		[self addSubview:_nextViewNameLabel];
		[self addSubview:_prevViewNameLabel];
		[self addSubview:_leftTextInputLabel];
		[self addSubview:_rightTextInputLabel];
		[self addSubview:_leftFieldView];
		[self addSubview:_rightFieldView];
		[_leftFieldView addSubview:_leftTextFieldLabel];
		[_rightFieldView addSubview:_rightTextFieldLabel];
		[_leftFieldView addSubview:_leftTextField];
		[_rightFieldView addSubview:_rightTextField];
		[self updateConstraints];
	}
	return self;
}


#pragma mark - Custom Accessors

-(void)setPair:(NSArray *)pair;
{
	_pair = pair;
	if ([self.thisViewNameLabel.text isEqualToString:CEStringOfMarket])
	{
		self.leftTextInputLabel.text = [NSString stringWithFormat:@"%@ to spend",pair[1]];
		self.rightTextInputLabel.text = [NSString stringWithFormat:@"%@ to sell",pair[0]];
		
		self.leftTextFieldLabel.text = pair[1];
		self.rightTextFieldLabel.text = pair[0];
	}
	else
	{
		self.leftTextInputLabel.text = pair[0];
		self.rightTextInputLabel.text = pair[1];
	}
}


#pragma mark - Public

- (CEInputView *)createNextInputViewWithCopying
{
	CEInputView *copy = [self copy];
	copy.thisViewNameLabel.text = self.nextViewNameLabel.text;
	copy.nextViewNameLabel.text = self.thisViewNameLabel.text;
	copy.prevViewNameLabel.text = self.thisViewNameLabel.text;
	copy.pair = self.pair;
	if ([self.thisViewNameLabel.text isEqualToString:CEStringOfMarket])
	{
		copy.leftTextInputLabel.text = @"Amount";
		copy.rightTextInputLabel.text = @"Price";
		copy.leftTextFieldLabel.text = self.pair[0];
		copy.rightTextFieldLabel.text = self.pair[1];
	}
	else
	{
		copy.leftTextInputLabel.text = [NSString stringWithFormat:@"%@ to spend",self.pair[1]];
		copy.rightTextInputLabel.text = [NSString stringWithFormat:@"%@ to sell",self.pair[0]];
		copy.leftTextFieldLabel.text = self.pair[1];
		copy.rightTextFieldLabel.text = self.pair[0];;
	}
	return copy;
}


#pragma mark - Private

- (void)updateConstraints
{
    self.translatesAutoresizingMaskIntoConstraints = NO;
    
    [_thisViewNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).with.offset(CEElementsTopOffset);
        make.centerX.equalTo(self.mas_centerX);
    }];

    [_nextViewNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).with.offset(CEElementsTopOffset);
        make.right.equalTo(self.mas_right).with.offset(-CENextPrevNameLabelOffsetLeftRight);
        make.height.equalTo(_thisViewNameLabel.mas_height);
    }];
    [_prevViewNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).with.offset(CEElementsTopOffset);
        make.left.equalTo(self.mas_left).with.offset(CENextPrevNameLabelOffsetLeftRight);
        make.height.equalTo(_thisViewNameLabel);
    }];
    
    [_leftTextInputLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_thisViewNameLabel.mas_bottom).with.offset(CEElementsTopOffset);
        make.left.equalTo(self.mas_left).with.offset(CEInputLabelOffsetLeftRight);
    }];
    
    [_rightTextInputLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_thisViewNameLabel.mas_bottom).with.offset(CEElementsTopOffset);
        make.left.equalTo(_rightFieldView.mas_left).with.offset(CEInputLabelOffsetLeftRight);
    }];
    
    [_leftFieldView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_leftTextInputLabel.mas_bottom).with.offset(CEElementsTopOffset);
        make.left.equalTo(self.mas_left).with.offset(CEFieldViewOffsetFromLeftRight);
    }];
    
    [_rightFieldView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_rightTextInputLabel.mas_bottom).with.offset(CEElementsTopOffset);
        make.right.equalTo(self.mas_right).with.offset(-CEFieldViewOffsetFromLeftRight);
        make.left.equalTo(_leftFieldView.mas_right).with.offset(1);
        make.height.equalTo(_leftFieldView.mas_height);
        make.width.equalTo(_leftFieldView.mas_width);
    }];
    
    [_leftTextFieldLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_leftFieldView.mas_left).with.offset(CEFieldTextLabelOffsetLeft);
        make.top.equalTo(_leftFieldView.mas_top).with.offset(CEFieldTextLabelOffsetTop);
    }];

    [_rightTextFieldLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_rightFieldView.mas_left).with.offset(CEFieldTextLabelOffsetLeft);
        make.top.equalTo(_leftFieldView.mas_top).with.offset(CEFieldTextLabelOffsetTop);
    }];
    
    [_leftTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(_leftFieldView.mas_bottom).with.offset(-CETextFieldOffsetTopBottom);
        make.left.equalTo(_leftFieldView.mas_left).with.offset(CETextFieldOffsetLeftRight);
        make.right.equalTo(_leftFieldView.mas_right).with.offset(-CETextFieldOffsetLeftRight);
        make.top.equalTo(_leftTextFieldLabel.mas_bottom).with.offset(CETextFieldOffsetTopBottom);
    }];
    
    [_rightTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(_rightFieldView.mas_bottom).with.offset(-CETextFieldOffsetTopBottom);
        make.left.equalTo(_rightFieldView.mas_left).with.offset(CETextFieldOffsetLeftRight);
        make.right.equalTo(_rightFieldView.mas_right).with.offset(-CETextFieldOffsetLeftRight);
        make.height.equalTo(_leftTextField.mas_height);
        make.top.equalTo(_rightTextFieldLabel.mas_bottom).with.offset(CETextFieldOffsetTopBottom);
    }];
    [super updateConstraints];
}

#pragma mark - NSCoping

-(id)copyWithZone:(NSZone *)zone
{
	CEInputView *copy = [CEInputView new];
	return copy;
}

@end
