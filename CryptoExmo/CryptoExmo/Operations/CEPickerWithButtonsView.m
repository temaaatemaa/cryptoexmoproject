//
//  CEPickerWithButtonsView.m
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 16.12.2017.
//  Copyright © 2017 Artem Zabludovsky. All rights reserved.
//


#import "CEPickerWithButtonsView.h"
#import <Masonry.h>
#import "CECryptoOperationsViewController.h"
#import "CECryptoOperationsViewModel.h"
#import "UIColor+CEColor.h"
#import "Currencies+CoreDataClass.h"
#import <AudioToolbox/AudioToolbox.h>


static CGFloat const CEElementsOffetFromLeftRight = 30.f;
static CGFloat const CEElementsOffetFromBottom = 100.f;
static CGFloat const CEPickerRowsHeight = 50.f;
static CGFloat const CEPickerButtonsWidth = 70.f;
static CGFloat const CEPickerViewHeight = 300.f;
static NSTimeInterval const CERemovingViewAnimationTime = 0.5;


@interface CEPickerWithButtonsView()<UIPickerViewDelegate,UIPickerViewDataSource,UIPickerViewDelegate>

@property (nonatomic, strong) CECryptoOperationsViewModel *viewModel;
@property (nonatomic, copy) NSArray *pairsArray;

@end

@implementation CEPickerWithButtonsView


#pragma mark - Lifecycle

- (instancetype)initWithViewModel:(CECryptoOperationsViewModel *)viewModel
{
    self = [super init];
    if (self)
	{
        self.translatesAutoresizingMaskIntoConstraints = NO;
        _viewModel = viewModel;
		[self setupPickerView];
		[self setupButtons];
    }
    return self;
}

- (void)updateConstraints
{
	[_pickerView mas_makeConstraints:^(MASConstraintMaker *make) {
		make.top.equalTo(self.mas_top);
		make.left.equalTo(self.mas_left).with.offset(CEElementsOffetFromLeftRight);
		make.right.equalTo(self.mas_right).with.offset(-CEElementsOffetFromLeftRight);
		make.height.equalTo(@(CEPickerViewHeight));
	}];
	[_okButton mas_makeConstraints:^(MASConstraintMaker *make) {
		make.bottom.equalTo(self.mas_bottom).with.offset(-CEElementsOffetFromBottom);
		make.right.equalTo(self.mas_right).with.offset(-CEElementsOffetFromLeftRight);
		make.height.equalTo(_okButton.mas_width);
		make.width.equalTo(@(CEPickerButtonsWidth));
	}];
	
	[_cancleButton mas_makeConstraints:^(MASConstraintMaker *make) {
		make.bottom.equalTo(self.mas_bottom).with.offset(-CEElementsOffetFromBottom);
		make.left.equalTo(self.mas_left).with.offset(CEElementsOffetFromLeftRight);
		make.height.equalTo(_cancleButton.mas_width);
		make.width.equalTo(@(CEPickerButtonsWidth));
	}];
	[super updateConstraints];
}


#pragma mark - Custom Accessors

- (NSArray *)pairsArray
{
    if (!_pairsArray) {
        NSArray *arrOfCurr = [self.viewModel.container.viewContext executeFetchRequest:[Currencies fetchRequest] error:nil];
        NSMutableArray *mutablePairsArray = [NSMutableArray new];
        for (Currencies *curr in arrOfCurr) {
            [mutablePairsArray addObject:[NSString stringWithFormat:@"%@/%@",curr.firstCurrencyShortName,curr.secondCurrencyShortName]];
        }
		[mutablePairsArray sortUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
			NSString *first = obj1;
			NSString *second = obj2;
			if ([first substringToIndex:1]>[second substringToIndex:1])
			{
				return NSOrderedDescending;
			}
			return NSOrderedAscending;
		}];
        _pairsArray = [mutablePairsArray copy];
    }
    return _pairsArray;
}


#pragma mark - IBActions

- (void)userDidChosePair
{
    
    NSInteger selectedRow = [self.pickerView selectedRowInComponent:0];
    if (selectedRow != -1) {
        NSString *str = self.pairsArray[selectedRow];
        NSArray *arrOfChoosenCurrencies = [str componentsSeparatedByString:@"/"];
        [self.viewModel pairDidChange:arrOfChoosenCurrencies];
    }
    [self userCancleDidTouch];
}

- (void)userCancleDidTouch
{
    AudioServicesPlaySystemSound(1520);
    UIVisualEffectView *blurView;
    NSArray *arrOfSubviews = self.superview.subviews;
    for (id subview in arrOfSubviews) {
        if ([subview isKindOfClass:[UIVisualEffectView class]]) {
            blurView = subview;
            break;
        }
    }
    [UIView animateWithDuration:CERemovingViewAnimationTime animations:^{
        [self mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.superview.mas_bottom).with.offset(500);
        }];
        blurView.alpha = 0;
        [self.superview layoutIfNeeded];
    } completion:^(BOOL finished) {
        [blurView removeFromSuperview];
    }];
}


#pragma mark - Private

- (void)setupPickerView
{
	_pickerView = [UIPickerView new];
	_pickerView.delegate = self;
	_pickerView.dataSource = self;
	_pickerView.backgroundColor = [UIColor whiteColor];
	_pickerView.layer.cornerRadius = 25;
	_pickerView.showsSelectionIndicator = YES;
	_pickerView.backgroundColor = [UIColor ceMainColor];
	[self addSubview:_pickerView];
}

- (void)setupButtons
{
	_okButton = [UIButton new];
	_cancleButton = [UIButton new];
	
	[self addSubview:_okButton];
	[self addSubview:_cancleButton];
	
	_okButton.backgroundColor = [UIColor ceMainColor];
	_cancleButton.backgroundColor = [UIColor blackColor];
	
	_okButton.layer.cornerRadius = CEPickerButtonsWidth/2;
	_cancleButton.layer.cornerRadius = CEPickerButtonsWidth/2;
	
	[_okButton setImage:[UIImage imageNamed:@"okIcon"] forState:UIControlStateNormal];
	[_cancleButton setImage:[self imageWithImage:[UIImage imageNamed:@"cancleIcon"] forSize:CGSizeMake(CEPickerButtonsWidth-5, CEPickerButtonsWidth-5)] forState:UIControlStateNormal];
	
	[_okButton addTarget:self action:@selector(userDidChosePair) forControlEvents:UIControlEventTouchDown];
	[_cancleButton addTarget:self action:@selector(userCancleDidTouch) forControlEvents:UIControlEventTouchDown];
}


#pragma mark - PickerViewDelegate

- (NSInteger)numberOfComponentsInPickerView:(nonnull UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(nonnull UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [self.pairsArray count];
}

- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row
					  													forComponent:(NSInteger)component
{
    UIFont *font = [UIFont systemFontOfSize:20 weight:UIFontWeightLight];
    NSDictionary *attributs = @{
                                NSForegroundColorAttributeName : [UIColor ceFontColor],
                                NSFontAttributeName : font
                                };
    
    NSAttributedString *title = [[NSAttributedString alloc] initWithString:self.pairsArray[row] attributes:attributs];
    return title;
}

-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return CEPickerRowsHeight;
}

- (UIImage *)imageWithImage:(UIImage *)image forSize:(CGSize)size
{
	UIGraphicsBeginImageContextWithOptions(size, NO, 0.0);
	[image drawInRect:CGRectMake(0, 0, size.width, size.height)];
	UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	return newImage;
}
@end
