//
//  CryptoOperationsModelView.h
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 17.12.2017.
//  Copyright © 2017 Artem Zabludovsky. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "CECryptoOperationsViewModelProtocol.h"
#import <CoreData/CoreData.h>


@interface CECryptoOperationsViewModel : NSObject

/** Делегат CECryptoOperationsViewModelDelegate
 @abstract Используется для уведомлений о новых данных или о том
 чтобы произвести определенные действия*/
@property (nonatomic, weak) id<CECryptoOperationsViewModelDelegate> delegate;

/** NSPersistentContainer
 @abstract CoreData container*/
@property (strong,nonatomic,readonly) NSPersistentContainer *container;

/** Количество криптовалюты, с которой имеем дело в данный момент
 @abstract Количество криптовалюты на балансе*/
@property (strong,nonatomic,readonly) NSDecimalNumber *amountOfCryptoCurrency;
/** Количество долларов на балансе
 @abstract Количество долларов на балансе*/
@property (strong,nonatomic,readonly) NSDecimalNumber *amountOfSecondCurrency;

/** Цена криптовалюты
 @abstract Цена обмена криптовалюты*/
@property (strong,nonatomic,readonly) NSDecimalNumber *changeCurrency;

/** Пара валют, которая используется в данный момент
 example: pair[0] - BTC pair[1] - USD --->pair = BTC_USD
 @abstract Пара валют*/
@property (nonatomic, copy, readonly) NSArray *pair;

/** Массив криптовалют, баланс на данный момент которых больше 0
 @abstract Массив криптовалют, баланс на данный момент которых больше 0*/
@property (nonatomic, copy, readonly) NSArray *arrayOfCurrenciesNameWhichBalanceIsNonNull;
/** Данные баланса
 @abstract NSdictionary баланса пользователя*/
@property (strong,nonatomic,readonly) NSMutableDictionary *dictionaryOfBalance;

/** Массив открытых ордеров
 @abstract Массив открытых ордеров*/
@property (nonatomic, copy) NSArray *arrayOfOpenOrders;
/** Массив истории ордеров пользователя
 @abstract Массив истории ордеров*/
@property (nonatomic, copy, readonly) NSArray *ordersHistoryArray;


/**  Следует вызывать при смене пары валют
 @param pair Новая пара валют
 Делегат:
 - (void)pairDidChange:(CECryptoOperationsViewModel *)operationsVM pair:(NSArray *)pair;*/
- (void)pairDidChange:(NSArray *)pair;

/**  Запрос на открытые ордера пользователя
 Делегат:
 - (void)arrayOfOpenOrdersDidLoad:(NSArray *)arrayOfOpenOrders;*/
- (void)downloadArrayOfUserOpenOrders;

/** Запрос на удаление ордера
 @param orderID ID ордера которого нужно удалить
 Делегат:
 - (void)orderDidDelete:(CEExmoNetwork *)exmo WithResult:(NSString *)result withError:(NSString *)error;*/
- (void)deleteUserOrder:(NSString *)orderID;

/** Запрос на создание ордера
 @param type Тип нового ордера (buy/sell/market_buy/market_sell)
 @param quantity Количество валюты выставляемой в ордер
 @param price Цена по которой будет составлен ордер
 Делегат:
 - (void)orderDidCreatedWithResult:(BOOL)result;*/
- (void)createOrderForType:(NSString *)type withQuantity:(NSDecimalNumber *)quantity withPrice:(NSDecimalNumber *)price;

/** Запрос на получение истории пользователя
 Делегат:
 - (void)historyDidLoad:(CEExmoNetwork *)exmo withHistoryArray:(NSArray *)historyArray error:(NSString *)error;*/
- (void)downloadTradesHistory;

/** Метод который показывает CEAuthorisationViewController в ViewControllere
 Делегат:
- (void)shouldPresentQRCodeViewController:(UIViewController *)vc;*/
- (void)showAuthorisationController;

/** Запрос на данные по истории валюты(поминутной) для графика
 Делегат:
- (void)currencyHistoryDidUpdate:(CECryptoOperationsViewModel *)operationsVM withHistory:(NSDictionary *)historyDictionary;*/
- (void)loadMinuteHistoryForGraph;

/** Запрос на данные по истории валюты(почасовой) для графика
 Делегат:
 - (void)currencyHistoryDidUpdate:(CECryptoOperationsViewModel *)operationsVM withHistory:(NSDictionary *)historyDictionary;*/
- (void)loadHourHistoryForGraph;

/** Запрос на данные по истории валюты(по дням) для графика
 Делегат:
 - (void)currencyHistoryDidUpdate:(CECryptoOperationsViewModel *)operationsVM withHistory:(NSDictionary *)historyDictionary;*/
- (void)loadDayHistoryForGraph;

@end
