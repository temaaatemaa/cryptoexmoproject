//
//  CEDevice.m
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 19.02.2018.
//  Copyright © 2018 Artem Zabludovsky. All rights reserved.
//

#import "CEDevice.h"

@implementation CEDevice

+ (NSString *) deviceName
{
	struct utsname systemInfo;
	uname(&systemInfo);
	
	return [NSString stringWithCString:systemInfo.machine
							  encoding:NSUTF8StringEncoding];
}

+ (BOOL)isIphoneWithSmallScreen
{
	NSString *name = [CEDevice deviceName];
	if (([name containsString:@"iPhone5"])||
		([name containsString:@"iPhone4"])||
		([name containsString:@"iPhone6"])||
		([name containsString:@"iPhone3"])||
		([name containsString:@"iPhone8,4"]))
	{
		return YES;
	}
	return NO;
}

@end
