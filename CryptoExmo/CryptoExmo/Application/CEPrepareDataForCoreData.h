//
//  CEPrepareDataForCoreData.h
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 24.12.2017.
//  Copyright © 2017 Artem Zabludovsky. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "Currencies+CoreDataClass.h"


@interface CEPrepareDataForCoreData : NSObject

/** Делегат CEPrepareDataForCoreData
 @abstract Используется для уведомлений о завершении сохранении данных */
@property (nonatomic,assign,nullable) id<NSFetchedResultsControllerDelegate> delegate;


/** Подготавлиает данные и сохраняет их в CoreData.
 @param tickerDictionary Загруженные данные об валютах
 @param currenciesNameData Полные данные о наименованиях криптовалют
 Используется NSFetchedResultsController.
 Делегаты - NSFetchedResultsControllerDelegate
 */
- (void)saveInCoreDataWith:(NSDictionary * _Nonnull)tickerDictionary withCurrenciesNameData:(NSData *_Nonnull)currenciesNameData;

@end
