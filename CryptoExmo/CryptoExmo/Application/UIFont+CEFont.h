//
//  UIFont+CEFont.h
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 21.12.2017.
//  Copyright © 2017 Artem Zabludovsky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont(CEFont)

//for CEMarketTableHeaderViewCell and CEWalletTableHeaderViewCell
+ (UIFont *)ceFontForMarketAndWalletTitle;
+ (UIFont *)ceFontForGlobalCapitalizationAndTotal;

//For CryptoOperationViewController
+ (UIFont *)ceFontForChooseButtonInOperationsVC;
+ (UIFont *)ceFontForTabsButtonInOperationsVC;

//for tradeview
+ (UIFont *)ceFontForGraphPeriodButtonsInTradeView;

+ (UIFont *)ceFontForCECurrencyViewControllerLabels;

@end
