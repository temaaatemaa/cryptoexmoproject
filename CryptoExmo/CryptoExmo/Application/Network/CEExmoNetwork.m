//
//  CEExmoNetwork.m
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 11.12.2017.
//  Copyright © 2017 Artem Zabludovsky. All rights reserved.
//


#import "CEExmoNetwork.h"
#import "AppDelegate.h"
#import <CommonCrypto/CommonDigest.h>
#import <CommonCrypto/CommonHMAC.h>
#import "CEPrepareDataForCoreData.h"


static NSString *const CETickerUrlString = @"https://api.exmo.com/v1/ticker/";
static NSString *const CECurrencysNameUrlString = @"https://min-api.cryptocompare.com/data/all/coinlist";
static NSString *const CEGlobalCapitalizationUrlString = @"https://api.coinmarketcap.com/v1/global/";
static NSString *const CEApiUrlString = @"https://api.exmo.com/v1/";
static NSString *const CECurrencyHistoryUrlString = @"https://min-api.cryptocompare.com/data/";

static NSString *const CEErrorForTimeOut = @"The request timed out.";
static NSString *const CEErrorForOfflineInternet = @"The Internet connection appears to be offline.";


@interface CEExmoNetwork()<NSURLSessionDataDelegate>

@property (nonatomic, copy) NSString *api_key;
@property (nonatomic, copy) NSString *secret_key;
@property (nonatomic, strong) NSPersistentContainer *container;

@property (nonatomic, strong) NSURLSession *session;

@property (nonatomic, strong) CEPrepareDataForCoreData *prepareDataForCoreData;
@property (nonatomic, strong) NSLock *lock;
@end


@implementation CEExmoNetwork


#pragma mark - Lifecycle

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        _api_key = [userDefaults objectForKey:@"api_key"];
        _secret_key = [userDefaults objectForKey:@"api_secret"];
		
        UIApplication *app = [UIApplication sharedApplication];
		_container = ((AppDelegate *)app.delegate).persistentContainer;
		
        _prepareDataForCoreData = [[CEPrepareDataForCoreData alloc]init];
		NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
		configuration.requestCachePolicy = NSURLRequestReloadIgnoringCacheData;
		_session = [NSURLSession sessionWithConfiguration:configuration
												 delegate:self
											delegateQueue:nil];
    }
    return self;
}


#pragma mark - Custom Accessors

- (void)setDelegate:(id<CEExmoNetworkDelegate,NSFetchedResultsControllerDelegate>)delegate
{
    _delegate = delegate;
    _prepareDataForCoreData.delegate = delegate;
}


#pragma mark - Public

-(void)getInfoForMarket
{
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:CETickerUrlString]];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request];
    [task resume];
}

- (void)getUserInfo
{
    NSMutableDictionary *postDictionary = [NSMutableDictionary new];
    NSURLRequest *request = [self createPostRequestFor:postDictionary method:@"user_info"];

    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request
                                                 completionHandler:^(NSData * _Nullable data,
                                                                     NSURLResponse * _Nullable response,
                                                                     NSError * _Nullable error){
        if (![self handleErrorFromDataTask:error withData:data])
        {
            return;
        }
        [self.delegate userInfoDidRecived:self withData:data];
    }];
    [task resume];
}

- (void)getGlobalCapitalization
{
    NSURL *url = [NSURL URLWithString:CEGlobalCapitalizationUrlString];
    NSURLRequest *req = [NSURLRequest requestWithURL:url];
    
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:req
                                                 completionHandler:^(NSData * _Nullable data,
                                                                     NSURLResponse * _Nullable response,
                                                                     NSError * _Nullable error){
        if (![self handleErrorFromDataTask:error withData:data])
        {
            return;
        }
        [self.delegate globalCapitalizationDidLoad:self withData:data];
    }];
    [task resume];
}

- (void)createOrderWithPair:(NSString *)first and:(NSString *)second withType:(NSString *)type
               withQuantity:(NSDecimalNumber *)quantity
                  withPrice:(NSDecimalNumber *)price
{
    NSMutableDictionary *postDictionary = [NSMutableDictionary new];
    [postDictionary setObject:price forKey:@"price"];
    [postDictionary setObject:quantity forKey:@"quantity"];
    [postDictionary setObject:type forKey:@"type"];
    [postDictionary setObject:[NSString stringWithFormat:@"%@_%@",first,second] forKey:@"pair"];
    
    NSURLRequest *request = [self createPostRequestFor:postDictionary method:@"order_create"];
    
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request
                                                 completionHandler:^(NSData * _Nullable data,
                                                                     NSURLResponse * _Nullable response,
                                                                     NSError * _Nullable error){
        if (![self handleErrorFromDataTask:error withData:data])
        {
            return;
        }
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        [self.delegate orderDidCreated:self withOrderID:dict[@"order_id"] withResult:dict[@"result"] withError:dict[@"error"]];
    }];
    [task resume];
}

- (void)getArrayOfUserOpenOrders
{
    NSMutableDictionary *postDictionary = [NSMutableDictionary new];
    NSURLRequest *request = [self createPostRequestFor:postDictionary method:@"user_open_orders"];
    
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request
                                                 completionHandler:^(NSData * _Nullable data,
                                                                     NSURLResponse * _Nullable response,
                                                                     NSError * _Nullable error){
         if (![self handleErrorFromDataTask:error withData:data])
         {
             return;
         }
         NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
         [self.delegate openOrdersDidLoad:self withOpenOrders:dict];
     }];
    [task resume];
}

- (void)deleteUserOrder:(NSString *)orderID
{
    NSMutableDictionary *postDictionary = [NSMutableDictionary new];
    [postDictionary setObject:orderID forKey:@"order_id"];
    
    NSURLRequest *request = [self createPostRequestFor:postDictionary method:@"order_cancel"];
    
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request
                                                 completionHandler:^(NSData * _Nullable data,
                                                                     NSURLResponse * _Nullable response,
                                                                     NSError * _Nullable error){
        if (![self handleErrorFromDataTask:error withData:data])
        {
            return;
        }
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        [self.delegate orderDidDelete:self WithResult:dict[@"result"] withError:dict[error]];
    }];
    [task resume];
}

- (void)getHistoryForPair:(NSString *)pair amount:(NSNumber *)amount offset:(NSNumber *)offset
{
    NSMutableDictionary *postDictionary = [NSMutableDictionary new];
    [postDictionary setObject:pair forKey:@"pair"];
    [postDictionary setObject:offset forKey:@"offset"];
    [postDictionary setObject:amount forKey:@"limit"];
    
    NSURLRequest *request = [self createPostRequestFor:postDictionary method:@"user_trades"];
    
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request
                                                 completionHandler:^(NSData * _Nullable data,
                                                                     NSURLResponse * _Nullable response,
                                                                     NSError * _Nullable error){
        if (![self handleErrorFromDataTask:error withData:data])
        {
            return;
        }
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        [self.delegate historyDidLoad:self withHistoryArray:dict[pair] error:dict[@"error"]];
    }];
    [task resume];
}

- (void)getCurrencyHistoryForPair:(NSArray *)pair withParametr:(NSString *)parametr withLimit:(NSNumber *)limit
{
    NSString *post = [NSString stringWithFormat:@"histo%@?fsym=%@&tsym=%@&limit=%@",parametr,pair[0],pair[1],limit];
    NSString *URLString = [CECurrencyHistoryUrlString stringByAppendingString:post];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:URLString]];
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request
                                                 completionHandler:^(NSData * _Nullable data,
                                                                     NSURLResponse * _Nullable response,
                                                                     NSError * _Nullable error) {
        if (![self handleErrorFromDataTask:error withData:data])
        {
            return;
        }
        NSDictionary *historyDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        [self.delegate currencyHistoryDidLoad:self withHistory:historyDictionary forPair:pair];
    }];
    [task resume];
}


#pragma mark - Private

- (NSString *)getNonce
{
    [self.lock lock];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];

	NSDateFormatter *dateFormatter = [NSDateFormatter new];
	[dateFormatter setDateFormat:@"yyyy-MM-dd"];
	dateFormatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
	NSDate *date = [dateFormatter dateFromString:@"2011-04-10"];
	NSTimeInterval timeStamp = [[NSDate date] timeIntervalSinceDate:date];
	NSNumber *nowNonce = [NSNumber numberWithDouble: timeStamp];

	NSNumber *lastNonce = ((NSNumber *)[userDefaults objectForKey:@"nonce"]);
	
	NSNumber  *currentNonce;
	if (nowNonce.integerValue <= lastNonce.integerValue)
	{
		currentNonce = [NSNumber numberWithInt:lastNonce.intValue+1];
	}
	else
	{
		currentNonce = nowNonce;
	}
	[userDefaults setObject:currentNonce forKey:@"nonce"];
	[self.lock unlock];
	return [NSString stringWithFormat:@"%i",currentNonce.intValue];
}

- (NSString *)hmacForKey:(NSString *)key andData:(NSString *)data
{
    if (!key)
    {
        return nil;
    }
    if (!data)
    {
        return nil;
    }
    const char *cKey = [key cStringUsingEncoding:NSASCIIStringEncoding];
    const char *cData = [data cStringUsingEncoding:NSASCIIStringEncoding];
    unsigned char cHMAC[CC_SHA512_DIGEST_LENGTH];
    CCHmac(kCCHmacAlgSHA512, cKey, strlen(cKey), cData, strlen(cData), cHMAC);
    NSMutableString *hashString = [NSMutableString stringWithCapacity:sizeof(cHMAC) * 2];
    for (int i = 0; i < sizeof(cHMAC); i++)
    {
        [hashString appendFormat:@"%02x", cHMAC[i]];
    }
    return hashString;
}

- (NSURLRequest *)createPostRequestFor:(NSDictionary *)postDictionary method:(NSString *)methodName
{
    NSString *post;
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	_api_key = [userDefaults objectForKey:@"api_key"];
	_secret_key = [userDefaults objectForKey:@"api_secret"];

    int i = 0;
    for (NSString *key in [postDictionary allKeys])
    {
        NSString *value = [postDictionary objectForKey:key];
        if (i==0)
        {
            post = [NSString stringWithFormat:@"%@=%@", key, value];
        }
        else
        {
            post = [NSString stringWithFormat:@"%@&%@=%@", post, key, value];
        }
        i++;
    }
    post = [NSString stringWithFormat:@"%@&nonce=%@", post, [self getNonce]];
    
    
    NSString *signedPost = [self hmacForKey:_secret_key andData:post];
    NSString *url = [CEApiUrlString stringByAppendingString:methodName];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]
                                    initWithURL:[NSURL URLWithString: url]];
    [request setHTTPMethod:@"POST"];
    [request setValue:_api_key forHTTPHeaderField:@"Key"];
    [request setValue:signedPost forHTTPHeaderField:@"Sign"];
    [request setHTTPBody:[post dataUsingEncoding: NSUTF8StringEncoding]];
    return [request copy];
}

- (int)handleErrorFromDataTask:(NSError *)error withData:(NSData *)data
{
    if (error)
    {
        NSLog(@"%@, %@",error,[error userInfo]);
        NSString *description = error.userInfo[@"NSLocalizedDescription"];
        if ([description isEqualToString:CEErrorForTimeOut])
        {
            NSLog(@"TIMEOUT");
            return 0;
        }
        if ([description isEqualToString:CEErrorForOfflineInternet])
        {
            NSLog(@"NO INTERNET");
            return 0;
        }
    }
    if (!data)
    {
        return 0;
    }
    return 1;
}


#pragma mark - NSURLSession Delegate

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveData:(NSData *)data
{
    if (!data)
    {
        return;
    }
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    NSData *currenciesNameData = [NSData dataWithContentsOfURL:[NSURL URLWithString:CECurrencysNameUrlString]];
    if (!currenciesNameData)
    {
        return;
    }
    [self.prepareDataForCoreData saveInCoreDataWith:dict withCurrenciesNameData:currenciesNameData];
}


@end
