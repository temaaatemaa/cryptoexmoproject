//
//  CEExmoNetwork.h
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 11.12.2017.
//  Copyright © 2017 Artem Zabludovsky. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "Currencies+CoreDataClass.h"
#import "CEExmoNetworkProtocol.h"


@interface CEExmoNetwork : NSObject

/** Делегат CEExmoNetwork
 @abstract Используется для уведомлений о завершении загрузки / ошибок
 */
@property (nonatomic, weak) id<CEExmoNetworkDelegate,NSFetchedResultsControllerDelegate> delegate;


/** Получить данные о балансе пользователя.
 Метод делегата:
 - (void)userInfoDidRecived:(CEExmoNetwork *)exmo withData:(NSData *)data;*/
- (void)getUserInfo;

/** Получить все открытые ордера для пользователя.
 Метод делегата:
 - (void)openOrdersDidLoad:(CEExmoNetwork *)exmo withOpenOrders:(NSDictionary *)openOrdersDictionary;*/
- (void)getArrayOfUserOpenOrders;

/** Создание нового ордера.
 @param first Первая валюта в паре
 @param second Вторая валюта в паре
 @param type Тип нового ордера (buy/sell/market_buy/market_sell)
 @param quantity Количество валюты выставляемой в ордер
 @param price Цена по которой будет составлен ордер
 Метод делегата:
 - (void)orderDidCreated:(CEExmoNetwork *)exmo withOrderID:(NSNumber *)orderId withResult:(NSString *)result
                                                                                withError:(NSString *)error;*/
- (void)createOrderWithPair:(NSString *)first and:(NSString *)second withType:(NSString *)type
               withQuantity:(NSDecimalNumber *)quantity withPrice:(NSDecimalNumber *)price;

/** Удаление ордера.
 @param orderID ID ордера который нужно удалить
 Метод делегата:
 - (void)orderDidDelete:(CEExmoNetwork *)exmo WithResult:(NSString *)result withError:(NSString *)error;*/
- (void)deleteUserOrder:(NSString *)orderID;

/*
 Данные об текущей стоимости, названиях и других данных по валютам.
 Методы делегата NSFetchResultController*/
- (void)getInfoForMarket;

/** Данные об капитализации рынка криптовалюты.
 Метод делегата:
 - (void)globalCapitalizationDidLoad:(CEExmoNetwork *)exmo withData:(NSData *)data;
 */
- (void)getGlobalCapitalization;

/** Данные по истории ордеров пользователя.
 @param pair Пара, история ордеров для которой требуется
 @param amount Количество ордеров
 @param offset Отступ от конца
 Метод делегата:
 - (void)historyDidLoad:(CEExmoNetwork *)exmo withHistoryArray:(NSArray *)historyArray error:(NSString *)error;*/
- (void)getHistoryForPair:(NSString *)pair amount:(NSNumber *)amount offset:(NSNumber *)offset;

/** Данные по истории курса пары
 @param pair Пара, история курса для которой требуется
 @param parametr minute/hour/day - строка
 @param limit Количество данных, которое требуется
 Метод делегата:
- (void)currencyHistoryDidLoad:(CEExmoNetwork *)exmo withHistory:(NSDictionary *)historyDictionary forPair:(NSArray *)pair*/
- (void)getCurrencyHistoryForPair:(NSArray *)pair withParametr:(NSString *)parametr withLimit:(NSNumber *)limit;

@end


