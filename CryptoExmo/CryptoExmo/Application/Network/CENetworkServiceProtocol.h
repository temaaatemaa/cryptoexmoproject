//
//  CENetworkServiceProtocol.h
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 12.12.2017.
//  Copyright © 2017 Artem Zabludovsky. All rights reserved.
//
#include "Currencies+CoreDataClass.h"
#ifndef CENetworkServiceProtocol_h
#define CENetworkServiceProtocol_h
@class CENetworkService;

@protocol CENetworkServiceOutputDelegate

@optional

/** Уведомление о завершенной загрузке иконки для валюты
 @param CENetworkService CENetworkService
 @param currency Валюта
 
 вызывается при
- (void)downloadIconsForCurrencies;*/
- (void)iconDidDownload:(CENetworkService *)CENetworkService forCurrency:(Currencies *)currency;


@end

#endif /* CENetworkServiceProtocol_h */
