//
//  CENetworkService.m
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 12.12.2017.
//  Copyright © 2017 Artem Zabludovsky. All rights reserved.
//

#import "CENetworkService.h"
#import "Currencies+CoreDataClass.h"
#import "AppDelegate.h"


@interface CENetworkService()
@property (nonatomic, strong) NSPersistentContainer *container;
@property (nonatomic, strong) NSURLSession *session;
@end

@implementation CENetworkService


#pragma mark - Lifecycle

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        UIApplication *app = [UIApplication sharedApplication];
		_container = ((AppDelegate *)app.delegate).persistentContainer;
		
		NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
		config.requestCachePolicy = NSURLRequestReloadIgnoringCacheData;
		_session = [NSURLSession sessionWithConfiguration:config];
    }
    return self;
}


#pragma mark - Public

- (void)downloadIconsForCurrencies
{
    dispatch_async(dispatch_get_main_queue(), ^{
        NSError *error;
        NSArray *arrayOfCurrencies = [self.container.viewContext executeFetchRequest:[Currencies fetchRequest] error:&error];
        for (Currencies *currency in arrayOfCurrencies)
        {
			if (!currency.imageURL)
			{
				return;
			}
            dispatch_async(dispatch_get_global_queue(0, 0), ^{
                [self downloadIconForCurrency:currency inContext:self.container.viewContext];
            });
        }
    });
}


#pragma mark - Private

- (void)downloadIconForCurrency:(Currencies *)currency inContext:(NSManagedObjectContext *)context
{
    NSURLSessionDataTask *task = [self.session dataTaskWithURL:currency.imageURL completionHandler:^(NSData * _Nullable data,
                                                            NSURLResponse * _Nullable response,
                                                            NSError * _Nullable error)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (error)
            {
                NSLog(@"%@",error);
            }
			currency.imageData = data;
			NSError *saveError;
			[context save:&saveError];
			if (saveError)
			{
				NSLog(@"%@",saveError);
			}
			if ([self.delegate respondsToSelector:@selector(iconDidDownload:forCurrency:)])
			{
				   [self.delegate iconDidDownload:self forCurrency:currency];
			}
			
        });
    }];
    [task resume];
}


@end
