//
//  CEExmoNetworkProtocol.h
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 24.12.2017.
//  Copyright © 2017 Artem Zabludovsky. All rights reserved.
//

#ifndef CEExmoNetworkProtocol_h
#define CEExmoNetworkProtocol_h


@class CEExmoNetwork;


@protocol CEExmoNetworkDelegate


@optional

/** Данные о балансе пользователя.
 @param exmo CEExmoNetwork
 @param data Полученные данные

 вызывается при
 - (void)getUserInfo;*/
- (void)userInfoDidRecived:(CEExmoNetwork *)exmo withData:(NSData *)data;

/** Данные по капитализации рынка криптовалют.
 @param exmo CEExmoNetwork
 @param data Полученные данные
 
 вызывается при
- (void)getGlobalCapitalization*/
- (void)globalCapitalizationDidLoad:(CEExmoNetwork *)exmo withData:(NSData *)data;

/** Данные о созданном ордере.
 @param exmo CEExmoNetwork
 @param orderId ID созданного ордера
 @param result Результат создания ордера
 @param error Ошибка при создании ордера
 
 вызывается при
- (void)getRequiredAmountForTransactionWith:(NSString *)firstCurrency and:(NSString *)secondCurrency*/
- (void)orderDidCreated:(CEExmoNetwork *)exmo withOrderID:(NSNumber *)orderId withResult:(NSString *)result
                                                                            withError:(NSString *)error;

/** Открытые ордера пользователя
 @param exmo CEExmoNetwork
 @param openOrdersDictionary Данные об всех открытых ордерах пользователя
 
 вызывается при
- (void)getArrayOfUserOpenOrders*/
- (void)openOrdersDidLoad:(CEExmoNetwork *)exmo withOpenOrders:(NSDictionary *)openOrdersDictionary;

/** Открытые ордера пользователя
 @param exmo CEExmoNetwork
 @param result Результат удаления ордера
 @param error Ошибка при удалении ордера
 
 вызывается при
- (void)deleteUserOrder:(NSString *)orderID*/
- (void)orderDidDelete:(CEExmoNetwork *)exmo WithResult:(NSString *)result withError:(NSString *)error;

/** История ордеров пользователя
 @param exmo CEExmoNetwork
 @param historyArray История ордеров пользователя
 @param error Ошибка при загрузке ордеров пользователя
 
 вызывается при
- (void)getHistoryForPair:(NSString *)pair amount:(NSNumber *)amount offset:(NSNumber *)offset;*/
- (void)historyDidLoad:(CEExmoNetwork *)exmo withHistoryArray:(NSArray *)historyArray error:(NSString *)error;

/** История курса пары валют
 @param exmo CEExmoNetwork
 @param historyDictionary История курса
 @param pair пара валют
 
 вызывается при
- (void)getCurrencyHistoryForPair:(NSArray *)pair withParametr:(NSString *)parametr withLimit:(NSNumber *)limit*/
- (void)currencyHistoryDidLoad:(CEExmoNetwork *)exmo withHistory:(NSDictionary *)historyDictionary forPair:(NSArray *)pair;

@end

#endif /* CEExmoNetworkProtocol_h */
