//
//  CENetworkService.h
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 12.12.2017.
//  Copyright © 2017 Artem Zabludovsky. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "CENetworkServiceProtocol.h"


@interface CENetworkService : NSObject

/** Делегат CENetworkService
 @abstract Используется для уведомлений о завершении загрузки */
@property (nonatomic, weak) id<CENetworkServiceOutputDelegate,NSFetchedResultsControllerDelegate> delegate;


/** Загрузка иконок для всех криптовалют
 Метод делегата:
 - (void)iconDidDownload:(CENetworkService *)CENetworkService forCurrency:(Currencies *)currency; */
- (void)downloadIconsForCurrencies;


@end
