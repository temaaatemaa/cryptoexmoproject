//
//  CEExmoGraph.m
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 16.02.2018.
//  Copyright © 2018 Artem Zabludovsky. All rights reserved.
//

#import "CEExmoGraph.h"
#import <Masonry.h>

@interface CEExmoGraph() <UIWebViewDelegate>
@property (strong, nonatomic) UIWebView *webView;
@end
@implementation CEExmoGraph

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	if (self)
	{
		self.backgroundColor = [UIColor blackColor];
		
		_webView = [[UIWebView alloc]initWithFrame:CGRectMake(0, 0, 200, 200)];
		_webView.delegate = self;
		_webView.opaque = NO;
		
		_webView.scalesPageToFit = YES;
		[self addSubview:_webView];
		
		[_webView mas_makeConstraints:^(MASConstraintMaker *make) {
			make.left.equalTo(self);
			make.right.equalTo(self);
			make.top.equalTo(self);
			make.bottom.equalTo(self);
		}];
	}
	return self;
}
- (void)loadTimeInterval:(NSString *)time forFirstCurrency:(NSString *)firstCurrency secondCurrency:(NSString *)secondCurrency
{
	NSString *URLString = [NSString stringWithFormat:@"https://exmo.me/ctrl/getTemplate?name=main_big2&para=%@_%@&period=%@&lang=ru&release=1518620707",firstCurrency,secondCurrency,time];
	dispatch_async(dispatch_get_global_queue(0, 0), ^{
		NSURL *url = [NSURL URLWithString:URLString];
		NSString *string = [self changeGraphColorOnURL:url];
		dispatch_async(dispatch_get_main_queue(), ^{
			[_webView loadHTMLString:string baseURL:url];
		});
	});
}
- (NSString *)changeGraphColorOnURL:(NSURL *)url
{
	NSString *str = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:nil];
	str = [str stringByReplacingOccurrencesOfString:@"rgba(255, 255, 255, 0.1)" withString:@"rgba(0, 0, 0, 1)"];
	str = [str stringByReplacingOccurrencesOfString:@"#eaeaea" withString:@"#ffffff"];//линия у yAxis
	str = [str stringByReplacingOccurrencesOfString:@"#ddd" withString:@"#ffffff"];
	str = [str stringByReplacingOccurrencesOfString:@"gridLineDashStyle: 'ShortDot'," withString:@"gridLineDashStyle: 'Solid',"];
	str = [str stringByReplacingOccurrencesOfString:@"gridLineWidth: 1," withString:@"gridLineWidth: 0.5,"];
	str = [str stringByReplacingOccurrencesOfString:@"tickPixelInterval: 29," withString:@"tickPixelInterval: 59,"];
	str = [str stringByReplacingOccurrencesOfString:@"898989" withString:@"ffffff"];
	str = [str stringByReplacingOccurrencesOfString:@"fontWeight: \"300\"," withString:@"fontWeight: \"300\", color:\"#ffffff\","];
	str = [str stringByReplacingOccurrencesOfString:@"x: -5," withString:@"x: -5, style:{ color:\"#ffffff\"},"];

	return str;
}
- (void)webViewDidFinishLoad:(UIWebView *)aWebView
{
	aWebView.scrollView.scrollEnabled = NO;    // Property available in iOS 5.0 and later
	CGRect frame = aWebView.frame;
	
	//iphone plus
	frame.size.width = 660;
	frame.size.height = 310;
	
	if (self.frame.size.width == 320)
	{//iphone5 5c ce
		frame.size.width = 500;
		frame.size.height = 180;//280
	}
	if (self.frame.size.width == 375)
	{//iphone6 7 8
		frame.size.width = 590;
		frame.size.height = 210;
		if (self.frame.size.height == 310)
		{//iphone x
			frame.size.width = 590;
			frame.size.height = 310;
		}
		if (self.frame.size.height > 330)
		{//iphone x
			frame.size.width = 590;
			frame.size.height = 310;
		}
	}
	
	
	aWebView.frame = frame;
	frame.size.height = aWebView.scrollView.contentSize.height;
	aWebView.frame = frame;
	//[self setNeedsLayout];
}
- (void)webViewDidStartLoad:(UIWebView *)webView
{
	webView.backgroundColor = [UIColor blackColor];
}
@end
