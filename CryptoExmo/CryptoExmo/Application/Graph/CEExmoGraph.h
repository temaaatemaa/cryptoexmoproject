//
//  CEExmoGraph.h
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 16.02.2018.
//  Copyright © 2018 Artem Zabludovsky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CEExmoGraph : UIView

- (void)loadTimeInterval:(NSString *)time forFirstCurrency:(NSString *)firstCurrency secondCurrency:(NSString *)secondCurrency;

@end
