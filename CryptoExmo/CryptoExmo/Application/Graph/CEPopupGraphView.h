//
//  CEPopupGraphView.h
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 25.12.2017.
//  Copyright © 2017 Artem Zabludovsky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CEPopupGraphView : UIView

/** Лэйблы для Х и У осей на всплывающем окошке
 @abstract Настройте лэйблы для отображения на всплывающем окне
 */
@property (nonatomic, strong) UILabel *yValueLabel;
@property (nonatomic, strong) UILabel *xValueLabel;

@end
