//
//  CEPopupGraphView.m
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 25.12.2017.
//  Copyright © 2017 Artem Zabludovsky. All rights reserved.
//


#import "CEPopupGraphView.h"
#import <Masonry.h>
#import "UIColor+CEColor.h"


@implementation CEPopupGraphView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.backgroundColor = [UIColor ceColorForBackgroundViewOfPopoverViewInCEGraph];
        self.layer.cornerRadius = 5;
        
        _xValueLabel = [UILabel new];
        _yValueLabel = [UILabel new];
        
        [self addSubview:_xValueLabel];
        [self addSubview:_yValueLabel];
        
        _xValueLabel.font = [UIFont systemFontOfSize:15 weight:UIFontWeightThin];
        _yValueLabel.font = [UIFont systemFontOfSize:15 weight:UIFontWeightLight];
        
        _xValueLabel.textAlignment = NSTextAlignmentCenter;
        _yValueLabel.textAlignment = NSTextAlignmentCenter;

        [_xValueLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self);
            make.left.equalTo(self);
            make.right.equalTo(self);
        }];
        [_yValueLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_xValueLabel.mas_bottom);
            make.left.equalTo(self);
            make.right.equalTo(self);
            make.bottom.equalTo(self);
        }];
    }
    return self;
}

@end
