//
//  CEGraph.m
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 25.12.2017.
//  Copyright © 2017 Artem Zabludovsky. All rights reserved.
//


#import "CEGraph.h"
#import "BEMSimpleLineGraphView.h"
#import <Masonry.h>
#import "UIColor+CEColor.h"
#import "CEPopupGraphView.h"


static CGRect const CERectForPopUpView = {0,0,100,40};
static CGFloat const CEMinHeightForShowAxis = 250.f;
static CGFloat const CEWidthOfGraphLine = 2.f;
static CGFloat const CEStaticPaddingForGraph = 30.f;
static NSInteger const CENumberOfGapsForMin = 10;
static NSInteger const CENumberOfGapsForHour = 2;
static NSInteger const CENumberOfGapsForDay = 3;
static NSInteger const CENumberOfYAxisLabels = 5;


@interface CEGraph()<BEMSimpleLineGraphDataSource, BEMSimpleLineGraphDelegate>

@property (nonatomic, strong) BEMSimpleLineGraphView *graphView;
@property (nonatomic, copy) NSArray<NSNumber *> *arr;
@property (nonatomic, copy) NSArray *valuesForGraph;
@property (nonatomic, copy) NSString *intervalString;// min hour day

@end


@implementation CEGraph


#pragma mark - Lifecycle

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.backgroundColor = [UIColor ceMainColor];
        _graphView = [[BEMSimpleLineGraphView alloc]initWithFrame:frame];
        _graphView.dataSource = self;
        _graphView.delegate = self;
        [self addSubview:_graphView];
        
        _graphView.widthLine = CEWidthOfGraphLine;
        _graphView.enablePopUpReport = YES;
        _graphView.enableReferenceYAxisLines = YES;
        _graphView.enableBezierCurve = YES;
        [self setupColorForGraph];
    }
    return self;
}

- (void)updateConstraints
{
    [_graphView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self);
        make.bottom.equalTo(self);
        make.right.equalTo(self);
        make.left.equalTo(self);
    }];
    if (CGRectGetHeight(self.frame) < CEMinHeightForShowAxis)
    {
        _graphView.enableYAxisLabel = NO;
        _graphView.enableXAxisLabel = NO;
    }
    else
    {
        _graphView.enableYAxisLabel = YES;
        _graphView.enableXAxisLabel = YES;
    }
    [super updateConstraints];
}


#pragma mark - Custom Accessors

- (void)setHistoryData:(NSDictionary *)historyData
{
    self.valuesForGraph = historyData[@"Data"];
    switch ([self.valuesForGraph count])
    {
        case 61:
            self.intervalString = @"min";
            break;
        case 49:
            self.intervalString = @"hour";
            break;
        case 31:
            self.intervalString = @"day";
            break;
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [self updateConstraints];
        [self.graphView reloadGraph];
    });
}


#pragma mark - Private

- (NSString *)convertUnixTimeInNormal:(NSNumber *)unixTime withFormat:(NSString *)format
{
    NSDate* date = [NSDate dateWithTimeIntervalSince1970:unixTime.doubleValue];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:format];
    NSString *dateString = [dateFormatter stringFromDate:date];
    return dateString;
}

- (void)setupColorForGraph
{
    _graphView.colorTop = [UIColor ceMainColor];
    _graphView.colorBottom = [UIColor ceMainColor];
    _graphView.colorXaxisLabel = [UIColor ceFontColor];
    _graphView.colorYaxisLabel = [UIColor ceFontColor];
	_graphView.colorLine = [UIColor ceAdditionalColor];
	_graphView.colorReferenceLines = [UIColor whiteColor];
	_graphView.colorTouchInputLine = [UIColor ceAdditionalColor];
    
    NSArray *colors = @[
                        (id)[UIColor whiteColor].CGColor,
                        (id)[UIColor ceMainColor].CGColor
                        ];
    CGColorSpaceRef space = CGColorSpaceCreateDeviceRGB();
    CGGradientRef gradient = CGGradientCreateWithColors(space, (CFArrayRef)colors, NULL);
    _graphView.gradientBottom = gradient;
}


#pragma mark - BEMSimpleLineGraphDelegate

- (NSString *)noDataLabelTextForLineGraph:(BEMSimpleLineGraphView *)graph
{
    return @"Loading data...";
}

-(BOOL)noDataLabelEnableForLineGraph:(BEMSimpleLineGraphView *)graph
{
    return YES;
}

- (CGFloat)staticPaddingForLineGraph:(BEMSimpleLineGraphView *)graph
{
    return CEStaticPaddingForGraph;
}

-(NSString *)yAxisSuffixOnLineGraph:(BEMSimpleLineGraphView *)graph
{
    return @"  ";
}

- (NSInteger)numberOfGapsBetweenLabelsOnLineGraph:(BEMSimpleLineGraphView *)graph
{
    if ([self.intervalString isEqualToString:@"min"])
    {
        return CENumberOfGapsForMin;
    }
    else if ([self.intervalString isEqualToString:@"hour"])
    {
        return CENumberOfGapsForHour;
    }
    return CENumberOfGapsForDay;
}

- (NSInteger)numberOfYAxisLabelsOnLineGraph:(BEMSimpleLineGraphView *)graph
{
    return CENumberOfYAxisLabels;
}

- (UIView *)popUpViewForLineGraph:(BEMSimpleLineGraphView *)graph
{
    CEPopupGraphView *view = [[CEPopupGraphView alloc]initWithFrame:CERectForPopUpView];
    return view;
}


#pragma mark - BEMSimpleLineGraphDelegate

- (CGFloat)lineGraph:(nonnull BEMSimpleLineGraphView *)graph valueForPointAtIndex:(NSInteger)index
{
    NSNumber *closeNumber = self.valuesForGraph[index][@"close"];
    return closeNumber.floatValue;
}

- (NSInteger)numberOfPointsInLineGraph:(nonnull BEMSimpleLineGraphView *)graph
{
    return [self.valuesForGraph count];
}

- (NSString *)lineGraph:(BEMSimpleLineGraphView *)graph labelOnXAxisForIndex:(NSInteger)index
{
    NSNumber *date = self.valuesForGraph[index][@"time"];
    if ([self.intervalString isEqualToString:@"min"])
    {
        return [self convertUnixTimeInNormal:date withFormat:@"HH:mm"];
    }
    else if ([self.intervalString isEqualToString:@"hour"])
    {
        return [self convertUnixTimeInNormal:date withFormat:@"HH:mm"];
    }
    return [self convertUnixTimeInNormal:date withFormat:@"dd.MM"];
}

- (UIView *)lineGraph:(BEMSimpleLineGraphView *)graph modifyPopupView:(UIView *)popupView forIndex:(NSUInteger)index
{
    CEPopupGraphView *view = (CEPopupGraphView *)popupView;
    NSString *date = [self convertUnixTimeInNormal:self.valuesForGraph[index][@"time"]
                                        withFormat:@"MM.dd HH.mm"];
    view.xValueLabel.text = date;
    NSString *yValue = [NSString stringWithFormat:@"%@",self.valuesForGraph[index][@"close"]];
    view.yValueLabel.text = yValue;
    return view;
}


@end
