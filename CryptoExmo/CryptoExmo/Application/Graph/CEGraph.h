//
//  CEGraph.h
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 25.12.2017.
//  Copyright © 2017 Artem Zabludovsky. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface CEGraph : UIView

/** Данные которые будут отображены на графе
 @abstract Установите данный словарь, для отображение на графике
 */
@property (nonatomic, strong) NSDictionary *historyData;

@end

