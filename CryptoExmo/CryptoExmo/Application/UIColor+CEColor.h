//
//  UIColor+CEColor.h
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 19.12.2017.
//  Copyright © 2017 Artem Zabludovsky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor(CEColor)

+ (UIColor *)ceMainColor;

+ (UIColor *)ceAdditionalColor;

+ (UIColor *)ceFontColor;

+ (UIColor *)ceRefreshColor;

+ (UIColor *)ceGreenColorForPersentageInMarketAndWalletCell;

+ (UIColor *)ceGreenColorForPriceInMarketAndWalletCell;

+ (UIColor *)ceRedColorInMarketAndWalletCell;

+ (UIColor *)ceColorForTabBarItems;

+ (UIColor *)ceFontColorForMarketAndWalletCell;

+ (UIColor *)ceBackgroundColor;

+ (UIColor *)ceColorForTabBarController;

+ (UIColor *)ceColorForTabBarTint;

+ (UIColor *)ceColorForBackgroundViewOfPopoverViewInCEGraph;

@end
