//
//  CESignsOfCurrencies.m
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 01.02.2018.
//  Copyright © 2018 Artem Zabludovsky. All rights reserved.
//

#import "CESignsOfCurrencies.h"

@implementation CESignsOfCurrencies

+ (NSString *)changeShortNameForSign:(NSString *)name
{
	if ([name isEqualToString:@"USD"])
	{
		return @"$";
	}
	if ([name isEqualToString:@"BTC"])
	{
		return @"₿";
	}
	if ([name isEqualToString:@"EUR"])
	{
		return @"€";
	}
	if ([name isEqualToString:@"UAH"])
	{
		return @"₴";
	}
	if ([name isEqualToString:@"USDT"])
	{
		return @"₮";
	}
	if ([name isEqualToString:@"RUB"])
	{
		return @"₽";
	}
	return name;
}

@end
