//
//  UIColor+CEColor.m
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 19.12.2017.
//  Copyright © 2017 Artem Zabludovsky. All rights reserved.
//

#import "UIColor+CEColor.h"

@implementation UIColor(CEColor)

+ (UIColor *)ceMainColor
{
    return [UIColor blackColor];
}

+ (UIColor *)ceFontColor
{
    return [UIColor whiteColor];
}

+ (UIColor *)ceRefreshColor
{
    return [UIColor ceAdditionalColor];
}

+ (UIColor *)ceGreenColorForPersentageInMarketAndWalletCell
{
    return [UIColor colorWithRed:0.f/255.f green:228.f/255.f blue:102.f/255.f alpha:1];
}

+ (UIColor *)ceGreenColorForPriceInMarketAndWalletCell
{
    return [UIColor colorWithRed:0.f/255.f green:180.f/255.f blue:70.f/255.f alpha:1];
}

+ (UIColor *)ceRedColorInMarketAndWalletCell
{
    return [UIColor redColor];
}

+ (UIColor *)ceAdditionalColor
{
    return [UIColor orangeColor];
}

+ (UIColor *)ceColorForTabBarItems
{
    return [UIColor colorWithRed:0 green:126.f/255.f blue:255.f/255.f alpha:1];
}

+ (UIColor *)ceFontColorForMarketAndWalletCell
{
    return [UIColor ceFontColor];
}

+ (UIColor *)ceBackgroundColor
{
    return [UIColor blackColor];
}

+ (UIColor *)ceColorForTabBarController
{
    return [UIColor colorWithRed:0 green:126.f/255.f blue:255.f/255.f alpha:1];
}

+ (UIColor *)ceColorForTabBarTint
{
    return [UIColor whiteColor];
}

+ (UIColor *)ceColorForBackgroundViewOfPopoverViewInCEGraph
{
    return [UIColor colorWithWhite:1 alpha:0.9];
}
@end
