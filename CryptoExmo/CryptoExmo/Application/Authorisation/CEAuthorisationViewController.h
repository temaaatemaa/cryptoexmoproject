//
//  CEAuthorisationViewController.h
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 17.12.2017.
//  Copyright © 2017 Artem Zabludovsky. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "CEAuthorisationViewControllerProtocol.h"


@interface CEAuthorisationViewController : UIViewController

/** Делегат AuthorisationViewController
 @abstract Используется для уведомления, что контроллер был закрыт
 */
@property (nonatomic, weak) id<CEAuthorisationViewControllerDelegate> delegate;

@end
