//
//  CEManualAuthView.h
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 20.12.2017.
//  Copyright © 2017 Artem Zabludovsky. All rights reserved.
//


#import <UIKit/UIKit.h>


@protocol CEManualAuthViewDelegate
/** Метод вызывается когда апи ключ и сикрет были сохранены
 @abstract Используется для уведомления что данные были сохранены
 */
- (void)inputsDataDidSave;

@end


@interface CEManualAuthView : UIView

/** Делегат ManualAuthView
 @abstract Используется для уведомления что данные были сохранены
 */
@property (nonatomic, weak) id<CEManualAuthViewDelegate> delegate;

@end
