//
//  CEAuthorisationViewController.m
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 17.12.2017.
//  Copyright © 2017 Artem Zabludovsky. All rights reserved.
//


#import "CEAuthorisationViewController.h"
#import <QRCodeReaderViewController.h>
#import <Masonry.h>
#import "UIColor+CEColor.h"
#import "CEManualAuthView.h"


static CGFloat const CEButtonCornerRadius = 2.f;


@interface CEAuthorisationViewController ()<QRCodeReaderDelegate,CEManualAuthViewDelegate>

@property (nonatomic, strong) QRCodeReader *qrCodeReader;
@property (nonatomic, strong) QRCodeReaderViewController *qrCodeReaderViewController;

@property (nonatomic, strong) UILabel *messageLabel;
@property (nonatomic, strong) UIButton *registerButton;
@property (nonatomic, strong) UILabel *messageHowToLoginLabel;

@property (nonatomic, strong) UIButton *qrButton;
@property (nonatomic, strong) UIButton *selfInputButton;
@property (nonatomic, strong) UIButton *cancleButton;

@property (nonatomic, strong) CEManualAuthView *manualAuthView;

@end


@implementation CEAuthorisationViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor ceMainColor];
    
    _messageLabel = [UILabel new];
	_messageLabel.text = @"You need to log in to have access to your Wallet and Operations.\n If you have not EXMO account, click next button:";
    _messageLabel.font = [UIFont systemFontOfSize:25 weight:UIFontWeightThin];
	if (self.view.frame.size.width<350)
	{
		 _messageLabel.font = [UIFont systemFontOfSize:20 weight:UIFontWeightThin];
	}
    _messageLabel.numberOfLines = 4;
    _messageLabel.textAlignment = NSTextAlignmentCenter;
    _messageLabel.textColor = [UIColor ceFontColor];
    [self.view addSubview:_messageLabel];
	
	_messageHowToLoginLabel = [UILabel new];
	_messageHowToLoginLabel.text = @"To login go to\n Exmo Site ->Settings -> Api";
	_messageHowToLoginLabel.font = [UIFont systemFontOfSize:25 weight:UIFontWeightThin];
	if (self.view.frame.size.width<350)
	{
		_messageHowToLoginLabel.font = [UIFont systemFontOfSize:20 weight:UIFontWeightThin];
	}
	_messageHowToLoginLabel.numberOfLines = 2;
	_messageHowToLoginLabel.textAlignment = NSTextAlignmentCenter;
	_messageHowToLoginLabel.textColor = [UIColor ceFontColor];
	[self.view addSubview:_messageHowToLoginLabel];
	
    [self setupButtons];
    
    [self setNeedsStatusBarAppearanceUpdate];
    [self updateViewConstraints];
}

- (void)updateViewConstraints
{
    [_messageLabel mas_makeConstraints:^(MASConstraintMaker *make) {
		make.centerX.equalTo(_qrButton);
        make.top.equalTo(self.view.mas_top).with.offset(self.view.frame.size.height/6);
        make.width.equalTo(self.view);
    }];
	
	[_registerButton mas_makeConstraints:^(MASConstraintMaker *make) {
		make.top.equalTo(_messageLabel.mas_bottom).with.offset(10);
		make.size.equalTo(_qrButton);
		make.centerX.equalTo(_qrButton);
	}];
	
	[_messageHowToLoginLabel mas_makeConstraints:^(MASConstraintMaker *make) {
		make.centerX.equalTo(self.view);
		make.centerY.equalTo(self.view).with.offset(10);
		make.width.equalTo(self.view);
	}];
	
    [_qrButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
		make.top.equalTo(_messageHowToLoginLabel.mas_bottom).with.offset(10);
		make.height.equalTo(@70);
		if (self.view.frame.size.width<350)
		{
			make.height.equalTo(@45);
		}
        make.width.equalTo(self.view).with.offset(-40);
    }];
    
    [_selfInputButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(_qrButton);
        make.top.equalTo(_qrButton.mas_bottom).with.offset(10);
        make.size.equalTo(_qrButton);
    }];
    
    [_cancleButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.view);
        make.centerX.equalTo(_qrButton);
        make.width.equalTo(self.view);
    }];
    [super updateViewConstraints];
}


#pragma mark - IBActions

- (void)qrauth
{
    [self autharization];
}

- (void)manualMode
{
    self.manualAuthView = [[CEManualAuthView alloc]initWithFrame:self.view.frame];
    self.manualAuthView.delegate = self;
    UIVisualEffectView *blurEffectView;
    if (!UIAccessibilityIsReduceTransparencyEnabled())
    {
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        blurEffectView.frame = self.view.bounds;
        blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    } else
    {
        self.view.backgroundColor = [UIColor blackColor];
    }
    
    if (blurEffectView) {
        [self.view addSubview:blurEffectView];
        blurEffectView.alpha = 0;
    }
    [self.view addSubview:self.manualAuthView];
    
    [self.manualAuthView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(self.view);
        make.centerX.equalTo(self.view);
        make.top.equalTo(self.view.mas_bottom);
    }];
    [self.view layoutIfNeeded];
    [UIView animateWithDuration:0.3 animations:^
     {
         [self.manualAuthView mas_remakeConstraints:^(MASConstraintMaker *make) {
             make.size.equalTo(self.view);
             make.centerY.equalTo(self.view);
             make.centerX.equalTo(self.view);
         }];
         if (blurEffectView)
         {
             blurEffectView.alpha = 1;
         }
         [self.view layoutIfNeeded];
     }];
}

- (void)cancelDidTouch
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Private

- (void)setupButtons
{
	_registerButton = [UIButton new];
    _qrButton = [UIButton new];
    _selfInputButton = [UIButton new];
    _cancleButton = [UIButton new];
	
	[_registerButton addTarget:self action:@selector(openRegister) forControlEvents:UIControlEventTouchDown];
    [_qrButton addTarget:self action:@selector(qrauth) forControlEvents:UIControlEventTouchDown];
    [_selfInputButton addTarget:self action:@selector(manualMode) forControlEvents:UIControlEventTouchDown];
    [_cancleButton addTarget:self action:@selector(cancelDidTouch) forControlEvents:UIControlEventTouchDown];
    
    [self setupFontsForButtons];
	
	_registerButton.layer.cornerRadius = CEButtonCornerRadius;
    _qrButton.layer.cornerRadius = CEButtonCornerRadius;
    _selfInputButton.layer.cornerRadius = CEButtonCornerRadius;
	
	_registerButton.backgroundColor = [UIColor whiteColor];
    _qrButton.backgroundColor = [UIColor whiteColor];
    _selfInputButton.backgroundColor = [UIColor whiteColor];
    _cancleButton.backgroundColor = [UIColor redColor];
	
	[self.view addSubview:_registerButton];
    [self.view addSubview:_qrButton];
    [self.view addSubview:_selfInputButton];
    [self.view addSubview:_cancleButton];
}

- (void)openRegister
{
	NSURL *url = [NSURL URLWithString:@"https://exmo.me/?ref=495137"];
	[[UIApplication sharedApplication] openURL:url options:@{} completionHandler:^(BOOL success) {
		NSLog(@"123");
	}];
}

- (void)setupFontsForButtons
{

    UIFont *fontForButtons = [UIFont systemFontOfSize:23 weight:UIFontWeightThin];
    NSDictionary *attrsDictionary = @{
                                      NSForegroundColorAttributeName : [UIColor blackColor],
                                      NSFontAttributeName : fontForButtons
                                      };
    
    NSDictionary *attrsDictionaryForCancle = @{
                                      NSForegroundColorAttributeName : [UIColor whiteColor],
                                      NSFontAttributeName : fontForButtons
                                      };
	NSAttributedString *registerTitle = [[NSAttributedString alloc] initWithString:@"Register EXMO account"
																		attributes:attrsDictionary];
    NSAttributedString *qrTitle = [[NSAttributedString alloc] initWithString:@"QR code auth"
                                                                  attributes:attrsDictionary];
    NSAttributedString *selfInputTitle = [[NSAttributedString alloc] initWithString:@"Manual mode"
                                                                         attributes:attrsDictionary];
    NSAttributedString *cancleTitle = [[NSAttributedString alloc] initWithString:@"Cancle"
                                                                      attributes:attrsDictionaryForCancle];
	
	[_registerButton setAttributedTitle:registerTitle forState:UIControlStateNormal];
    [_qrButton setAttributedTitle:qrTitle forState:UIControlStateNormal];
    [_selfInputButton setAttributedTitle:selfInputTitle forState:UIControlStateNormal];
    [_cancleButton setAttributedTitle:cancleTitle forState:UIControlStateNormal];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)autharization
{
    self.qrCodeReader = [QRCodeReader readerWithMetadataObjectTypes:@[AVMetadataObjectTypeQRCode]];
    self.qrCodeReaderViewController = [QRCodeReaderViewController readerWithCancelButtonTitle:@"Cancel"
                                                           codeReader:_qrCodeReader
                                                  startScanningAtLoad:YES
                                               showSwitchCameraButton:YES
                                                      showTorchButton:YES];
    _qrCodeReaderViewController.modalPresentationStyle = UIModalPresentationFormSheet;
    _qrCodeReaderViewController.delegate = self;
    [self presentViewController:_qrCodeReaderViewController animated:YES completion:nil];
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
}

- (void)inputsDataDidSave
{
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.delegate authControllerDidClose];
}


#pragma mark - QRCodeReaderDelegate

- (void)reader:(QRCodeReaderViewController *)reader didScanResult:(NSString *)resultAsString
{
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *keyString = [resultAsString substringWithRange:NSMakeRange(5, 42)];
        NSString *secret = [resultAsString substringWithRange:NSMakeRange(48, 42)];
        
        NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
        [userDef setObject:keyString forKey:@"api_key"];
        [userDef setObject:secret forKey:@"api_secret"];
        
        [self dismissViewControllerAnimated:YES completion:^{
            [self dismissViewControllerAnimated:YES completion:nil];
            [self.delegate authControllerDidClose];
        }];
    });
}

- (void)readerDidCancel:(QRCodeReaderViewController *)reader
{
    [reader dismissViewControllerAnimated:YES completion:nil];
}


@end

