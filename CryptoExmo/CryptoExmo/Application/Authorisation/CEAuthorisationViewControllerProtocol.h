//
//  CEAuthorisationViewControllerProtocol.h
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 17.12.2017.
//  Copyright © 2017 Artem Zabludovsky. All rights reserved.
//

#ifndef CEAuthorisationViewControllerProtocol_h
#define CEAuthorisationViewControllerProtocol_h

@protocol CEAuthorisationViewControllerDelegate

/** Метод вызывается когда контроллер был закрыт
 @abstract Используется для уведомления о том, что контроллер был закрыт
 */
- (void)authControllerDidClose;

@end

#endif /* Header_h */
