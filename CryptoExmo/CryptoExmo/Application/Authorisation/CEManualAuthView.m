//
//  CEManualAuthView.m
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 20.12.2017.
//  Copyright © 2017 Artem Zabludovsky. All rights reserved.
//


#import "CEManualAuthView.h"
#import <Masonry.h>
#import "UIColor+CEColor.h"


static NSTimeInterval const CERemovingViewAnimationTime = 0.5;
static CGFloat const CERadiusCorner = 5.f;
static NSInteger const CELengthOfKeyAndSecret = 42;
static CGFloat const CEShakingDistance = 15.f;
static CGFloat const CETitleFromBottomOffcet = 30.f;
static CGFloat const CETextFieldHeight = 40.f;
static CGFloat const CEElementsOffset = 20.f;


@interface CEManualAuthView()

@property (nonatomic, strong) UIView *backgroundView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UITextField *keyTextField;
@property (nonatomic, strong) UITextField *secretTextField;
@property (nonatomic, strong) UIButton *okButton;
@property (nonatomic, strong) UIButton *cancleButton;

@end

@implementation CEManualAuthView


#pragma mark - Lifecycle

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        _backgroundView = [UIView new];
        _titleLabel = [UILabel new];
        _keyTextField = [UITextField new];
        _secretTextField = [UITextField new];
        _okButton = [UIButton new];
        _cancleButton = [UIButton new];
        
        _backgroundView.backgroundColor = [UIColor ceMainColor];
        _titleLabel.textColor = [UIColor ceFontColor];
        _keyTextField.backgroundColor = [UIColor whiteColor];
        _secretTextField.backgroundColor = [UIColor whiteColor];
        
        _titleLabel.font = [UIFont systemFontOfSize:25 weight:UIFontWeightLight];
        _keyTextField.font = [UIFont systemFontOfSize:17 weight:UIFontWeightThin];
        _secretTextField.font = [UIFont systemFontOfSize:17 weight:UIFontWeightThin];
        
        _keyTextField.layer.cornerRadius = CERadiusCorner;
        _secretTextField.layer.cornerRadius = CERadiusCorner;
        _backgroundView.layer.cornerRadius = CERadiusCorner;
        
        _keyTextField.clearsOnBeginEditing = YES;
        _secretTextField.clearsOnBeginEditing = YES;
        
        _titleLabel.text = @"Authtorisation";
        _keyTextField.text = @"api_key";
        _secretTextField.text = @"api_secret";
        
        [self setupFontsForButtons];
        
        [self addSubview:_backgroundView];
        [self addSubview:_titleLabel];
        [self addSubview:_keyTextField];
        [self addSubview:_secretTextField];
        [self addSubview:_okButton];
        [self addSubview:_cancleButton];
        
        [_okButton addTarget:self action:@selector(okButtonClicked) forControlEvents:UIControlEventTouchDown];
        [_cancleButton addTarget:self action:@selector(cancleButtonClicked) forControlEvents:UIControlEventTouchDown];
        
        [self updateConstraints];
    }
    return self;
}

- (void)updateConstraints
{
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(_keyTextField.mas_top).with.offset(-CETitleFromBottomOffcet);
        make.centerX.equalTo(self);
    }];
    
    [_keyTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self).with.offset(-100);
        make.centerX.equalTo(self);
        make.width.equalTo(self).with.offset(-(CETextFieldHeight));
        make.height.equalTo(@(CETextFieldHeight));
    }];
    
    [_secretTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.top.equalTo(_keyTextField.mas_bottom).with.offset(CEElementsOffset);
        make.width.equalTo(_keyTextField);
        make.height.equalTo(_keyTextField);
    }];
    
    [_okButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_secretTextField.mas_left);
        make.top.equalTo(_secretTextField.mas_bottom).with.offset(CEElementsOffset);
    }];
    
    [_cancleButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_secretTextField.mas_right);
        make.top.equalTo(_secretTextField.mas_bottom).with.offset(CEElementsOffset);
        make.width.equalTo(_okButton);
        make.left.equalTo(_okButton.mas_right).with.offset(1);
    }];
    
    [_backgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_titleLabel.mas_top).with.offset(-CEElementsOffset/2);
        make.centerX.equalTo(self);
        make.width.equalTo(self).with.offset(-CEElementsOffset);
        make.bottom.equalTo(_okButton).with.offset(CEElementsOffset/2);
    }];
    [super updateConstraints];
}


#pragma mark - IBActions

- (void)cancleButtonClicked
{
    UIVisualEffectView *blurView;
    NSArray *arrOfSubviews = self.superview.subviews;
    for (id subview in arrOfSubviews)
    {
        if ([subview isKindOfClass:[UIVisualEffectView class]])
        {
            blurView = subview;
            break;
        }
    }
    [UIView animateWithDuration:CERemovingViewAnimationTime animations:^{
        [self mas_updateConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.superview.mas_centerY).with.offset(-500);
        }];
        blurView.alpha = 0;
        [self.superview layoutIfNeeded];
    } completion:^(BOOL finished) {
        [blurView removeFromSuperview];
        self.hidden = YES;
    }];
}

- (void)okButtonClicked
{
    NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
    NSString *keyString = self.keyTextField.text;
    NSString *secret = self.secretTextField.text;
    
    if (keyString.length != CELengthOfKeyAndSecret)
    {
        [self wrongInputAnimation];
        return;
    }
    if (secret.length != CELengthOfKeyAndSecret)
    {
        [self wrongInputAnimation];
        return;
    }
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
    [userDef setObject:keyString forKey:@"api_key"];
    [userDef setObject:secret forKey:@"api_secret"];
    [self cancleButtonClicked];
    [self.delegate inputsDataDidSave];
}


#pragma mark - Private

- (void)setupFontsForButtons
{
    
    UIFont *fontForButtons = [UIFont systemFontOfSize:20 weight:UIFontWeightThin];
    NSDictionary *attrsDictionary = @{
                                      NSForegroundColorAttributeName : [UIColor whiteColor],
                                      NSFontAttributeName : fontForButtons
                                      };
    
    NSAttributedString *okTitle = [[NSAttributedString alloc] initWithString:@"OK" attributes:attrsDictionary];
    NSAttributedString *cancleTitle = [[NSAttributedString alloc] initWithString:@"Cancle" attributes:attrsDictionary];
    
    [_okButton setAttributedTitle:okTitle forState:UIControlStateNormal];
    [_cancleButton setAttributedTitle:cancleTitle forState:UIControlStateNormal];
}

- (void)wrongInputAnimation
{
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position"];
    [animation setDuration:0.05];
    [animation setRepeatCount:4];
    [animation setAutoreverses:YES];
    [animation setFromValue:[NSValue valueWithCGPoint:
                             CGPointMake(self.center.x - CEShakingDistance, self.center.y)]];
    [animation setToValue:[NSValue valueWithCGPoint:
                           CGPointMake(self.center.x + CEShakingDistance, self.center.y)]];
    [self.layer addAnimation:animation forKey:@"position"];
}

@end
