//
//  UIFont+CEFont.m
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 21.12.2017.
//  Copyright © 2017 Artem Zabludovsky. All rights reserved.
//

#import "UIFont+CEFont.h"

@implementation UIFont(CEFont)

+ (UIFont *)ceFontForMarketAndWalletTitle
{
    return [UIFont fontWithName:@"PingFangHK-Semibold" size:60];
}

+ (UIFont *)ceFontForGlobalCapitalizationAndTotal
{
    return [UIFont systemFontOfSize:23 weight:UIFontWeightRegular];
}

+ (UIFont *)ceFontForChooseButtonInOperationsVC
{
    return [UIFont systemFontOfSize:31 weight:UIFontWeightLight];
}

+ (UIFont *)ceFontForTabsButtonInOperationsVC
{
    return [UIFont systemFontOfSize:15 weight:UIFontWeightLight];
}

+ (UIFont *)ceFontForGraphPeriodButtonsInTradeView
{
    return [UIFont systemFontOfSize:15 weight:UIFontWeightLight];
}

+ (UIFont *)ceFontForCECurrencyViewControllerLabels
{
    return [UIFont systemFontOfSize:16 weight:UIFontWeightThin];
}
@end
