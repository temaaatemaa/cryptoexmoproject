//
//  CEDevice.h
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 19.02.2018.
//  Copyright © 2018 Artem Zabludovsky. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sys/utsname.h>

@interface CEDevice : NSObject

+ (NSString *)deviceName;
+ (BOOL)isIphoneWithSmallScreen;
@end
