//
//  CEPrepareDataForCoreData.m
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 24.12.2017.
//  Copyright © 2017 Artem Zabludovsky. All rights reserved.
//


#import "CEPrepareDataForCoreData.h"
#import "AppDelegate.h"


static NSString *const CEImagesUrlString = @"https://www.cryptocompare.com";


@interface CEPrepareDataForCoreData()


@property (nonatomic, strong) NSPersistentContainer *container;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultController;

@end


@implementation CEPrepareDataForCoreData


#pragma mark - Lifecycle

- (instancetype)init
{
    self = [super init];
    if (self)
    {
		UIApplication *app = [UIApplication sharedApplication];
		_container = ((AppDelegate *)app.delegate).persistentContainer;
    }
    return self;
}


#pragma mark - Custom Accessors

- (NSFetchedResultsController *)fetchedResultController
{
	if (!_fetchedResultController) {
		NSFetchRequest *request = [Currencies fetchRequest];
		request.sortDescriptors = @[
									[[NSSortDescriptor alloc]initWithKey:@"volume24ch" ascending:NO]
									];
		_fetchedResultController = [[NSFetchedResultsController alloc]initWithFetchRequest:request
														managedObjectContext:self.container.viewContext
														  sectionNameKeyPath:nil
																   cacheName:nil];
		_fetchedResultController.delegate = self.delegate;
	}
	return _fetchedResultController;
}


#pragma mark - Public

- (void)saveInCoreDataWith:(NSDictionary * _Nonnull)tickerDictionary withCurrenciesNameData:(NSData *)currenciesNameData
{
    NSError *error;
    NSDictionary *dictionaryOfCurrencies = [NSJSONSerialization JSONObjectWithData:currenciesNameData options:0 error:&error];
    if (error)
    {
        NSLog(@"%@",error);
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        for (NSString *pairOfCurrencies in [tickerDictionary allKeys])
        {
			NSRange range = [pairOfCurrencies rangeOfString:@"_"];
			NSString *firstCurrencyShortName = [pairOfCurrencies substringToIndex:range.location];
			NSString *secondCurrencyShortName = [pairOfCurrencies substringFromIndex:range.location+1];
			NSArray *matches = [self findCurrencyInCoreDatWithFirstShortName:firstCurrencyShortName secondShortName:secondCurrencyShortName];

			Currencies *currency;
			if (!matches || ([matches count] > 1))
			{
				NSLog(@"matches %lu",(unsigned long)[matches count]);
			}
			else if ([matches count])
			{
				currency = [matches firstObject];
			}
			else
			{
				currency = [self createNewCurrenciesInCoreDataForFirstCurrencyShortName:firstCurrencyShortName
																secondCurrencyShortName:secondCurrencyShortName
															   withCurrenciesDictionary:dictionaryOfCurrencies];
			}
			NSDictionary *tickerForCurrency = tickerDictionary[pairOfCurrencies];
			[self valuesForDeterminingIncreaseOrDecreaseForCurrency:currency withTickerForCurrency:tickerForCurrency];
			[self valuesForMarketForCurrency:currency withTickerForCurrency:tickerForCurrency];
        }
        NSError *err;
        [self.container.viewContext save:&err];
        if (err)
        {
            NSLog(@"%@ %@",err,[err localizedDescription]);
            abort();
        }
    });
}


#pragma mark - Private

- (void)valuesForMarketForCurrency:(Currencies *)currency withTickerForCurrency:(NSDictionary *)tickerForCurrency
{
    NSString *volume24chString = tickerForCurrency[@"vol_curr"];
    currency.buyPrice = tickerForCurrency[@"buy_price"];
    currency.sellPrice = tickerForCurrency[@"sell_price"];
    currency.volume24ch = volume24chString.intValue;
    currency.valueInDollar = tickerForCurrency[@"last_trade"];
    currency.highPrice = tickerForCurrency[@"high"];
    currency.lowPrice = tickerForCurrency[@"low"];
    currency.avgPrice = tickerForCurrency[@"avg"];
    
    CGFloat valueFloat = currency.valueInDollar.floatValue;
    CGFloat avgFloat = [tickerForCurrency[@"avg"] floatValue];
    currency.persentage = (valueFloat/avgFloat-1)*100;
}

- (void)valuesForDeterminingIncreaseOrDecreaseForCurrency:(Currencies *)currency withTickerForCurrency:(NSDictionary *)tickerForCurrency
{
    if (currency.buyPrice.floatValue < [tickerForCurrency[@"buy_price"] floatValue])
    {
        currency.isPriceIncrease = YES;
        currency.isPriceDecrease = NO;
    }
    else if (currency.buyPrice.floatValue > [tickerForCurrency[@"buy_price"] floatValue])
    {
        currency.isPriceIncrease = NO;
        currency.isPriceDecrease = YES;
    }
    else
    {
        currency.isPriceIncrease = NO;
        currency.isPriceDecrease = NO;
    }
}

- (Currencies *)createNewCurrenciesInCoreDataForFirstCurrencyShortName:(NSString *)firstCurrencyShortName
											   secondCurrencyShortName:(NSString *)secondCurrencyShortName
                                         withCurrenciesDictionary:(NSDictionary *)dictionaryOfCurrencies
{
    Currencies *currency;
    currency = [NSEntityDescription insertNewObjectForEntityForName:@"Currencies" inManagedObjectContext:self.container.viewContext];
    currency.firstCurrencyShortName = firstCurrencyShortName;
	currency.secondCurrencyShortName = secondCurrencyShortName;
    currency.firstCurrencyName = dictionaryOfCurrencies[@"Data"][currency.firstCurrencyShortName][@"FullName"];
	currency.secondCurrencyName = dictionaryOfCurrencies[@"Data"][currency.secondCurrencyShortName][@"FullName"];
	if ([currency.firstCurrencyShortName isEqualToString:@"USD"])
	{
		if ([currency.secondCurrencyShortName isEqualToString:@"RUB"])
		{
			currency.firstCurrencyName = @"Dollar (USD)";
			currency.secondCurrencyName = @"Ruble (RUB)";
		}
	}
	if (!currency.secondCurrencyName)
	{
		if ([currency.secondCurrencyShortName isEqualToString:@"USD"])
		{
			currency.secondCurrencyName = @"Dollar (USD)";
		}
		if ([currency.secondCurrencyShortName isEqualToString:@"RUB"])
		{
			currency.secondCurrencyName = @"Ruble (RUB)";
		}
		if ([currency.secondCurrencyShortName isEqualToString:@"EUR"])
		{
			currency.secondCurrencyName = @"Euro (EUR)";
		}
		if (!currency.secondCurrencyName)
		{
			NSLog(@"No secondCurrencyName For %@", currency.secondCurrencyShortName);
			currency.secondCurrencyName = currency.secondCurrencyShortName;
		}
	}
    //ошибка апи - лишние буквы в BCH
    if ([currency.firstCurrencyShortName isEqualToString:@"BCH"])
    {
        currency.firstCurrencyName = [currency.firstCurrencyName stringByReplacingOccurrencesOfString:@"/ BCC " withString:@""];
    }
	NSString *imageUrlOnSite = dictionaryOfCurrencies[@"Data"][currency.firstCurrencyShortName][@"ImageUrl"];
	if (imageUrlOnSite)
	{
		NSString *urlString = [CEImagesUrlString stringByAppendingString:imageUrlOnSite];
		currency.imageURL = [NSURL URLWithString:urlString];
	}
    return currency;
}

- (NSArray *)findCurrencyInCoreDatWithFirstShortName:(NSString *)firstShortName secondShortName:(NSString *)secondShortName
{
    self.fetchedResultController.fetchRequest.predicate =
        [NSPredicate predicateWithFormat:@"firstCurrencyShortName = %@ AND secondCurrencyShortName = %@", firstShortName,secondShortName];

    NSError *err;
    if (![self.fetchedResultController performFetch:&err])
    {
        NSLog(@"Unresolved error %@, %@", err, [err userInfo]);
        abort();
    }
    return self.fetchedResultController.fetchedObjects;
}

@end
