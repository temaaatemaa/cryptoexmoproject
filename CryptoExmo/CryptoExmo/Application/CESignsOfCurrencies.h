//
//  CESignsOfCurrencies.h
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 01.02.2018.
//  Copyright © 2018 Artem Zabludovsky. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CESignsOfCurrencies : NSObject

+ (NSString *)changeShortNameForSign:(NSString *)name;

@end
