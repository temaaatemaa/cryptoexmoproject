//
//  CEMarketAndWalletTableViewCell.m
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 12.12.2017.
//  Copyright © 2017 Artem Zabludovsky. All rights reserved.
//

#import "CEMarketAndWalletTableViewCell.h"
#import <Masonry.h>
#import "UIColor+CEColor.h"
#import "CEDevice.h"


static const CGFloat CEElementsOffset = 7.f;
static const CGFloat CEPersentageLableWidth = 75.f;;
static const CGFloat CEVolumeLableHeight = 25.f;;

@implementation CEMarketAndWalletTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        self.backgroundColor = [UIColor ceBackgroundColor];
		UIView *selectedView = [[UIView alloc]init];
		selectedView.backgroundColor = [UIColor ceColorForTabBarItems];
		self.selectedBackgroundView =  selectedView;

        _nameLabel = [UILabel new];
        _nameLabel.font = [UIFont systemFontOfSize:20 weight:UIFontWeightRegular];
        _nameLabel.textColor = [UIColor ceFontColorForMarketAndWalletCell];
        [self.contentView addSubview:_nameLabel];
        
        _priceLabel = [UILabel new];
        _priceLabel.textAlignment = NSTextAlignmentRight;
        _priceLabel.textColor = [UIColor ceFontColorForMarketAndWalletCell];
        [self.contentView addSubview:_priceLabel];
        
        _volumeLabel = [UILabel new];
        _volumeLabel.textColor = [UIColor ceFontColorForMarketAndWalletCell];
        [self.contentView addSubview:_volumeLabel];
        
        _percetageLabel = [UILabel new];
        _percetageLabel.textColor = [UIColor ceFontColorForMarketAndWalletCell];
        _percetageLabel.textAlignment = NSTextAlignmentCenter;
        _percetageLabel.layer.cornerRadius = 6;
        _percetageLabel.clipsToBounds = YES;
        [self.contentView addSubview:_percetageLabel];
		
		self.imageView.layer.cornerRadius = (70-2*CEElementsOffset)/2;
		self.imageView.clipsToBounds = YES;
        
        _volumeLabel.font = [UIFont systemFontOfSize:17 weight:UIFontWeightThin];
        _priceLabel.font = [UIFont systemFontOfSize:17 weight:UIFontWeightRegular];
        _percetageLabel.font = [UIFont systemFontOfSize:17 weight:UIFontWeightThin];
        _nameLabel.font = [UIFont systemFontOfSize:20 weight:UIFontWeightRegular];
		

		if ([CEDevice isIphoneWithSmallScreen])
		{
			_volumeLabel.font = [UIFont systemFontOfSize:13 weight:UIFontWeightThin];
			_priceLabel.font = [UIFont systemFontOfSize:13 weight:UIFontWeightRegular];
			_percetageLabel.font = [UIFont systemFontOfSize:13 weight:UIFontWeightThin];
			_nameLabel.font = [UIFont systemFontOfSize:15 weight:UIFontWeightRegular];
		}

		[self makeConstraints];
    }
    return self;
}

- (void)makeConstraints
{
	[_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
		make.left.equalTo(self.imageView.mas_right).with.offset(CEElementsOffset);
		make.top.equalTo(self.contentView.mas_top).with.offset(CEElementsOffset);
		make.height.equalTo(self.priceLabel.mas_height);
	}];
	
	[_volumeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
		make.left.equalTo(self.imageView.mas_right).with.offset(CEElementsOffset);
		make.top.equalTo(_nameLabel.mas_bottom).with.offset(CEElementsOffset);
		make.bottom.equalTo(self.contentView.mas_bottom).with.offset(-CEElementsOffset);
		make.height.equalTo(@(CEVolumeLableHeight));
	}];
	
	[_priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
		make.left.equalTo(_nameLabel.mas_right).with.offset(CEElementsOffset);
		make.right.equalTo(self.contentView.mas_right).with.offset(-CEElementsOffset);
		make.top.equalTo(self.contentView.mas_top).with.offset(CEElementsOffset);
	}];
	
	[_percetageLabel mas_makeConstraints:^(MASConstraintMaker *make) {
		make.width.equalTo(@(CEPersentageLableWidth));
		make.bottom.equalTo(self.contentView.mas_bottom).with.offset(-CEElementsOffset);
		make.right.equalTo(self.contentView.mas_right).with.offset(-CEElementsOffset);
		make.height.equalTo(_volumeLabel.mas_height);
	}];
}

@end
