//
//  CECurrencyViewModel.h
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 02.01.2018.
//  Copyright © 2018 Artem Zabludovsky. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "CECurrencyViewModelProtocol.h"
#import "Currencies+CoreDataClass.h"


@interface CECurrencyViewModel : NSObject

/** Массив валют - пара с которой в данный момент работаем
 @abstract Используется для уведомлений новых данных */
@property (nonatomic, copy) NSArray *pair;

/** Делегат CECurrencyViewModelDelegate
 @abstract Используется для уведомлений о новых данных */
@property (nonatomic, weak) id<CECurrencyViewModelDelegate> delegate;

/** Загрузка данных для графика с минутной дельтой
 Метод Делегата:
 - (void)currencyHistoryDidUpdate:(CECurrencyViewModel *)currencyVM withHistory:(NSDictionary *)historyDictionary;*/
- (void)loadMinuteHistoryForGraph;

/** Загрузка данных для графика с часовой дельтой
 Метод Делегата:
 - (void)currencyHistoryDidUpdate:(CECurrencyViewModel *)currencyVM withHistory:(NSDictionary *)historyDictionary;*/
- (void)loadHourHistoryForGraph;

/** Загрузка данных для графика с дневной дельтой
 Метод Делегата:
 - (void)currencyHistoryDidUpdate:(CECurrencyViewModel *)currencyVM withHistory:(NSDictionary *)historyDictionary;*/
- (void)loadDayHistoryForGraph;

/** Получение Currensies данной валюты из CoreData
 @param pair Требуемая валюта
 @return Currencies*/
- (Currencies *)fetchRequestForCurrencyWithPair:(NSArray *)pair;

/** Подготовка к dealloc*/
- (void)prepareToDealloc;

@end
