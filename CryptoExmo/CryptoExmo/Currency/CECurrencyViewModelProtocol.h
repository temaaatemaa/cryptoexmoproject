//
//  CECurrencyViewModelProtocol.h
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 02.01.2018.
//  Copyright © 2018 Artem Zabludovsky. All rights reserved.
//

#ifndef CECurrencyViewModelProtocol_h
#define CECurrencyViewModelProtocol_h

@class CECurrencyViewModel;

@protocol CECurrencyViewModelDelegate

@required

/** Уведомление о загрузке данных для нового графика
 @param currencyVM CECurrencyViewModel
 @param historyDictionary Данные для нового графика
 
 вызывается при
 - (void)loadMinuteHistoryForGraph;
 - (void)loadHourHistoryForGraph;
 - (void)loadDayHistoryForGraph;*/
- (void)currencyHistoryDidUpdate:(CECurrencyViewModel *)currencyVM withHistory:(NSDictionary *)historyDictionary;

/** Уведомление об обновлении данных рынка криптовалют.
 @param currencyVM CECurrencyViewModel
 
 вызывается каждые CETimerInterfalForUpdatingData секунд */
- (void)didUpdateCurrencyData:(CECurrencyViewModel *)currencyVM;

@end

#endif /* CECurrencyViewModelProtocol_h */
