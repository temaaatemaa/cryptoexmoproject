//
//  CECurrencyViewController.m
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 02.01.2018.
//  Copyright © 2018 Artem Zabludovsky. All rights reserved.
//


#import "CECurrencyViewController.h"
#import "CECurrencyViewModel.h"
#import "UIColor+CEColor.h"
#import "UIFont+CEFont.h"

#import "CEGraph.h"
#import "CEExmoGraph.h"

#import "Currencies+CoreDataClass.h"
#import <Masonry.h>
#import "CEButtonDownLineView.h"
#import <AudioToolbox/AudioToolbox.h>

#import "CECryptoOperationsViewController.h"

#import "CESignsOfCurrencies.h"


static CGFloat const CEElementsOffset = 1.f;
static CGFloat const CEGraphButtonsHeight = 25.f;
static CGFloat const CELineButtonsHeight = 2.f;
static NSString *const CENameOfMinuteGraphPeriodButton = @"Day";
static NSString *const CENameOfHourGraphPeriodButton = @"Week";
static NSString *const CENameOfDayGraphPeriodButton = @"Month";


@interface CECurrencyViewController ()<CECurrencyViewModelDelegate>

@property (nonatomic, strong) UIView *graph;
@property (nonatomic, assign,getter=isExmoGraph) BOOL exmoGraph;

@property (nonatomic, strong) UIButton *graphMinuteButton;
@property (nonatomic, strong) UIButton *graphHourButton;
@property (nonatomic, strong) UIButton *graphDayButton;
@property (nonatomic, strong) CEButtonDownLineView *buttonLineView;

@property (nonatomic, strong) CECurrencyViewModel *currencyViewModel;

//эти лэйблы названия - слева
@property (nonatomic, strong) UILabel *buyTitleLabel;
@property (nonatomic, strong) UILabel *sellTitleLabel;
@property (nonatomic, strong) UILabel *highTitleLabel;
@property (nonatomic, strong) UILabel *lowTitleLabel;
@property (nonatomic, strong) UILabel *avgTitleLabel;
@property (nonatomic, strong) UILabel *volumeTitleLabel;

//эти лэйблы с численным значением - справа
@property (nonatomic, strong) UILabel *buyLabel;
@property (nonatomic, strong) UILabel *sellLabel;
@property (nonatomic, strong) UILabel *highLabel;
@property (nonatomic, strong) UILabel *lowLabel;
@property (nonatomic, strong) UILabel *avgLabel;
@property (nonatomic, strong) UILabel *volumeLabel;

@end


@implementation CECurrencyViewController


#pragma mark - Lifecycle

- (void)dealloc
{
    [self.currencyViewModel prepareToDealloc];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
	
	[self chooseGraph];
	
    [self setupNavigationBar];
    
    [self setupTabBarBackground];
    
    [self setupGraph];
    
    [self setupViewModel];
    
    [self setupLabels];
	
	[self setupChangeGraphButton];
	
    [self updateViewConstraints];
    [self.view setNeedsLayout];
    [self.view setNeedsUpdateConstraints];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)updateViewConstraints
{
    CGRect statusBarFrame = [UIApplication sharedApplication].statusBarFrame;
    [_graph mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_top).with.offset(CGRectGetHeight(self.navigationController.navigationBar.frame)
                                                        +CGRectGetHeight(statusBarFrame));//статус бар
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
		
		
		if (self.isExmoGraph)
		{
			//iph plus
			CGFloat height = 310;
			if (self.view.frame.size.width == 320)
			{//iphone5 5c ce
				height = 180;
			}
			if (self.view.frame.size.width == 375)
			{//iphone6 7 8
				height = 210;
				NSLog(@"%f",self.view.frame.size.height);
				if (self.view.frame.size.height == 812)
				{//iphone x
					height = 310;
				}
			}
			make.height.equalTo(@(height));
		}
		else
		{
			make.bottom.equalTo(self.view.mas_centerY);
		}
    }];
    
    [_graphMinuteButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_graph.mas_bottom);
        make.left.equalTo(self.view);
        make.height.equalTo(@(CEGraphButtonsHeight));
    }];
    [_graphHourButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_graph.mas_bottom);
        make.left.equalTo(_graphMinuteButton.mas_right).with.offset(1);
        make.width.equalTo(_graphMinuteButton);
        make.height.equalTo(_graphMinuteButton);
    }];
    [_graphDayButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_graph.mas_bottom);
        make.left.equalTo(_graphHourButton.mas_right).with.offset(1);
        make.right.equalTo(self.view);
        make.width.equalTo(_graphMinuteButton);
        make.height.equalTo(_graphMinuteButton);
    }];
    
    [_buyTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_graphDayButton.mas_bottom).with.offset(CEElementsOffset);
        make.left.equalTo(self.view);
    }];
    [_sellTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_buyTitleLabel.mas_bottom).with.offset(1);
        make.left.equalTo(_buyTitleLabel);
        make.height.equalTo(_buyTitleLabel);
    }];
    [_highTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_sellTitleLabel.mas_bottom).with.offset(1);
        make.left.equalTo(_buyTitleLabel);
        make.height.equalTo(_buyTitleLabel);
    }];
    [_lowTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_highTitleLabel.mas_bottom).with.offset(1);
        make.left.equalTo(_buyTitleLabel);
        make.height.equalTo(_buyTitleLabel);
    }];
    [_avgTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_lowTitleLabel.mas_bottom).with.offset(1);
        make.left.equalTo(_buyTitleLabel);
        make.height.equalTo(_buyTitleLabel);
    }];
    [_volumeTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_avgTitleLabel.mas_bottom).with.offset(1);
        make.left.equalTo(_buyTitleLabel);
        make.height.equalTo(_buyTitleLabel);
        make.bottom.equalTo(self.view).with.offset(-CGRectGetHeight(self.tabBarController.tabBar.frame)-CEElementsOffset);//tabbar
    }];
    
    [_buyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(_buyTitleLabel);
        make.left.equalTo(_buyTitleLabel.mas_right);
        make.right.equalTo(self.view);
        make.height.equalTo(_buyTitleLabel);
        make.width.equalTo(_buyTitleLabel);
    }];
    [_sellLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(_sellTitleLabel);
        make.left.equalTo(_sellTitleLabel.mas_right);
        make.right.equalTo(self.view);
        make.height.equalTo(_buyTitleLabel);
        make.width.equalTo(_buyTitleLabel);
    }];
    [_highLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(_highTitleLabel);
        make.left.equalTo(_highTitleLabel.mas_right);
        make.right.equalTo(self.view);
        make.height.equalTo(_buyTitleLabel);
        make.width.equalTo(_buyTitleLabel);
    }];
    [_lowLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(_lowTitleLabel);
        make.left.equalTo(_lowTitleLabel.mas_right);
        make.right.equalTo(self.view);
        make.height.equalTo(_buyTitleLabel);
        make.width.equalTo(_buyTitleLabel);
    }];
    [_avgLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(_avgTitleLabel);
        make.left.equalTo(_avgTitleLabel.mas_right);
        make.right.equalTo(self.view);
        make.height.equalTo(_buyTitleLabel);
        make.width.equalTo(_buyTitleLabel);
    }];
    [_volumeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(_volumeTitleLabel);
        make.left.equalTo(_volumeTitleLabel.mas_right);
        make.right.equalTo(self.view);
        make.height.equalTo(_buyTitleLabel);
        make.width.equalTo(_buyTitleLabel);
    }];
    [super updateViewConstraints];
}


#pragma mark - Custom Accessors

- (void)setPair:(NSArray *)pair
{
    _pair = pair;
}


#pragma mark - IBActions

- (void)setMinuteGraph
{
	if (self.isExmoGraph)
	{
		[(CEExmoGraph *)_graph loadTimeInterval:@"day" forFirstCurrency:self.pair[0] secondCurrency:self.pair[1]];
	}
	else
	{
		[self.currencyViewModel loadMinuteHistoryForGraph];
	}
	
	
    [self.buttonLineView removeFromSuperview];
    self.buttonLineView = [[CEButtonDownLineView alloc]initWithColor:[UIColor ceAdditionalColor]
                                                         andHeight:@(CELineButtonsHeight)];
    [self.graphMinuteButton addSubview:self.buttonLineView];
	
	[self updateViewConstraints];
	[self.view setNeedsLayout];
	
    AudioServicesPlaySystemSound(1520);
}

- (void)setHourGraph
{
  	if (self.isExmoGraph)
	{
		[(CEExmoGraph *)_graph loadTimeInterval:@"week" forFirstCurrency:self.pair[0] secondCurrency:self.pair[1]];
	}
	else
	{
		[self.currencyViewModel loadHourHistoryForGraph];
	}
	
    [self.buttonLineView removeFromSuperview];
    self.buttonLineView = [[CEButtonDownLineView alloc]initWithColor:[UIColor ceAdditionalColor]
                                                         andHeight:@(CELineButtonsHeight)];
    [self.graphHourButton addSubview:self.buttonLineView];
	
	[self updateViewConstraints];
	[self.view setNeedsLayout];
	
    AudioServicesPlaySystemSound(1520);
}

- (void)setDayGraph
{
	if (self.isExmoGraph)
	{
		[(CEExmoGraph *)_graph loadTimeInterval:@"month" forFirstCurrency:self.pair[0] secondCurrency:self.pair[1]];
	}
	else
	{
		[self.currencyViewModel loadDayHistoryForGraph];
	}
	
    [self.buttonLineView removeFromSuperview];
    self.buttonLineView = [[CEButtonDownLineView alloc]initWithColor:[UIColor ceAdditionalColor]
                                                         andHeight:@(CELineButtonsHeight)];
    [self.graphDayButton addSubview:self.buttonLineView];
	
	[self updateViewConstraints];
	[self.view setNeedsLayout];
	
    AudioServicesPlaySystemSound(1520);
}


#pragma mark - Private

- (void)chooseGraph
{
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	
	if ([[userDefaults objectForKey:@"graph"]isEqualToString:@"exmo"])
	{
		self.exmoGraph = YES;
	}
	else
	{
		self.exmoGraph = NO;
	}
}

- (UIImage *)imageWithImage:(UIImage *)image forSize:(CGSize)size
{
	UIGraphicsBeginImageContextWithOptions(size, NO, 0.0);
	[image drawInRect:CGRectMake(0, 0, size.width, size.height)];
	UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	return newImage;
}

- (void)setupChangeGraphButton
{
	UIImage *image = [UIImage imageNamed:@"changegraph"];
	image = [self imageWithImage:image forSize:CGSizeMake(35, 35)];
	[self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStylePlain target:self action:@selector(changeGraph)]];
}

- (void)changeGraph
{
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	if ([[userDefaults objectForKey:@"graph"]isEqualToString:@"exmo"]) {
		[userDefaults setObject:@"my" forKey:@"graph"];
	}
	else
	{
		[userDefaults setObject:@"exmo" forKey:@"graph"];//EEEEEXXXXMMMMMOOOOEEEXXXMMMMOOOO
	}
	[self.graph removeFromSuperview];
	[self.graphDayButton removeFromSuperview];
	[self.graphHourButton removeFromSuperview];
	[self.graphMinuteButton removeFromSuperview];
	
	[self chooseGraph];
	[self setupGraph];
	
	[self updateViewConstraints];
	[self.view layoutIfNeeded];
	
	NSArray *arrOfVC = self.tabBarController.viewControllers;
	NSMutableArray *arr = [NSMutableArray arrayWithArray:arrOfVC];
	UIViewController *vc = arr[2];
	UIViewController *newvc = [[CECryptoOperationsViewController alloc]init];
	newvc.tabBarItem = vc.tabBarItem;
	[arr replaceObjectAtIndex:2 withObject:newvc];
	self.tabBarController.viewControllers = arr.copy;
}

- (void)setupViewModel
{
    _currencyViewModel = [CECurrencyViewModel new];
    _currencyViewModel.delegate = self;
    
    self.currencyViewModel.pair = _pair;
    [self.currencyViewModel loadHourHistoryForGraph];
}

- (void)setupTabBarBackground
{
    CGRect rectForTabBar = self.tabBarController.tabBar.frame;
    UIView *backgroundForTabBarView = [[UIView alloc] initWithFrame:rectForTabBar];
    backgroundForTabBarView.backgroundColor = [UIColor ceBackgroundColor];
    [self.view addSubview:backgroundForTabBarView];
}

- (void)setupGraph
{
	if (self.isExmoGraph)
	{
		_graph = [[CEExmoGraph alloc]initWithFrame:CGRectNull];
	}
	else
	{
		_graph = [[CEGraph alloc]initWithFrame:CGRectNull];
	}
	
    [self.view addSubview:_graph];
    
    [self setupGraphButtons];
	
	if (self.isExmoGraph)
	{
		[self setHourGraph];
	}
	else
	{
		[self setHourGraph];
	}
}

- (void)setupNavigationBar
{
    self.title = _pair[0];
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.backgroundColor = [UIColor ceMainColor];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    self.navigationController.navigationBar.alpha = 1;
    
    CGRect rectForStatusBar = [UIApplication sharedApplication].statusBarFrame;
    CGRect rectForNavigationBar = self.navigationController.navigationBar.frame;
    
    CGRect rectForBackgroundViewForNavControllerAndStatusBar = CGRectMake(0, 0, CGRectGetWidth(rectForStatusBar) + CGRectGetWidth(rectForNavigationBar), CGRectGetHeight(rectForStatusBar) + CGRectGetHeight(rectForNavigationBar));
    
    UIView *backgroundViewForNavController = [[UIView alloc] initWithFrame:rectForBackgroundViewForNavControllerAndStatusBar];
    backgroundViewForNavController.backgroundColor = [UIColor ceBackgroundColor];
    [self.view addSubview:backgroundViewForNavController];
}

- (void)setupLabels
{
    _buyTitleLabel = [UILabel new];
    _sellTitleLabel = [UILabel new];
    _highTitleLabel = [UILabel new];
    _lowTitleLabel = [UILabel new];
    _avgTitleLabel = [UILabel new];
    _volumeTitleLabel = [UILabel new];
    
    _buyLabel = [UILabel new];
    _sellLabel = [UILabel new];
    _highLabel = [UILabel new];
    _lowLabel = [UILabel new];
    _avgLabel = [UILabel new];
    _volumeLabel = [UILabel new];
    
    [self.view addSubview:_buyTitleLabel];
    [self.view addSubview:_sellTitleLabel];
    [self.view addSubview:_highTitleLabel];
    [self.view addSubview:_lowTitleLabel];
    [self.view addSubview:_avgTitleLabel];
    [self.view addSubview:_volumeTitleLabel];
    
    [self.view addSubview:_buyLabel];
    [self.view addSubview:_sellLabel];
    [self.view addSubview:_highLabel];
    [self.view addSubview:_lowLabel];
    [self.view addSubview:_avgLabel];
    [self.view addSubview:_volumeLabel];
    
    _buyTitleLabel.backgroundColor = [UIColor ceMainColor];
    _sellTitleLabel.backgroundColor = [UIColor ceMainColor];
    _highTitleLabel.backgroundColor = [UIColor ceMainColor];
    _lowTitleLabel.backgroundColor = [UIColor ceMainColor];
    _avgTitleLabel.backgroundColor = [UIColor ceMainColor];
    _volumeTitleLabel.backgroundColor = [UIColor ceMainColor];
    
    _buyLabel.backgroundColor = [UIColor ceMainColor];
    _sellLabel.backgroundColor = [UIColor ceMainColor];
    _highLabel.backgroundColor = [UIColor ceMainColor];
    _lowLabel.backgroundColor = [UIColor ceMainColor];
    _avgLabel.backgroundColor = [UIColor ceMainColor];
    _volumeLabel.backgroundColor = [UIColor ceMainColor];
    
    _buyTitleLabel.text = @" Buy:";
    _sellTitleLabel.text = @" Sell:";
    _highTitleLabel.text = @" High:";
    _lowTitleLabel.text = @" Low:";
    _avgTitleLabel.text = @" Average:";
    _volumeTitleLabel.text = @" Volume:";

    _buyLabel.textColor = [UIColor ceFontColor];
    _sellLabel.textColor = [UIColor ceFontColor];
    _highLabel.textColor = [UIColor ceFontColor];
    _lowLabel.textColor = [UIColor ceFontColor];
    _avgLabel.textColor = [UIColor ceFontColor];
    _volumeLabel.textColor = [UIColor ceFontColor];
    
    _buyTitleLabel.textColor = [UIColor ceFontColor];
    _sellTitleLabel.textColor = [UIColor ceFontColor];
    _highTitleLabel.textColor = [UIColor ceFontColor];
    _lowTitleLabel.textColor = [UIColor ceFontColor];
    _avgTitleLabel.textColor = [UIColor ceFontColor];
    _volumeTitleLabel.textColor = [UIColor ceFontColor];
    
    [self setLabelsValue];
    
    UIFont *fontForLabels = [UIFont ceFontForCECurrencyViewControllerLabels];
    _buyTitleLabel.font = fontForLabels;
    _sellTitleLabel.font = fontForLabels;
    _highTitleLabel.font = fontForLabels;
    _lowTitleLabel.font = fontForLabels;
    _avgTitleLabel.font = fontForLabels;
    _volumeTitleLabel.font = fontForLabels;
    
    _buyLabel.font = fontForLabels;
    _sellLabel.font = fontForLabels;
    _highLabel.font = fontForLabels;
    _lowLabel.font = fontForLabels;
    _avgLabel.font = fontForLabels;
    _volumeLabel.font = fontForLabels;
}

- (void)setLabelsValue
{
    Currencies *currency = [self.currencyViewModel fetchRequestForCurrencyWithPair:self.pair];
	NSString *changedName = [currency.firstCurrencyName stringByReplacingOccurrencesOfString:@"(" withString:@""];
	NSString *changedNameWithoutLastCharacter = [changedName stringByReplacingOccurrencesOfString:@")" withString:@""];
	NSString *changedNameWithSlash = [changedNameWithoutLastCharacter stringByAppendingString:@"/"];
	NSString *nameLabel = [changedNameWithSlash stringByAppendingString:currency.secondCurrencyShortName];
	self.title = nameLabel;
	
    NSDecimalNumber *buyNumber = [NSDecimalNumber decimalNumberWithString:currency.buyPrice];
    NSDecimalNumber *sellNumber = [NSDecimalNumber decimalNumberWithString:currency.sellPrice];
    NSDecimalNumber *highNumber = [NSDecimalNumber decimalNumberWithString:currency.highPrice];
    NSDecimalNumber *lowNumber = [NSDecimalNumber decimalNumberWithString:currency.lowPrice];
    NSDecimalNumber *avgNumber = [NSDecimalNumber decimalNumberWithString:currency.avgPrice];
    NSDecimalNumber *volumeNumber = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%d",currency.volume24ch]];
    
    NSDecimalNumberHandler *behavior = [NSDecimalNumberHandler decimalNumberHandlerWithRoundingMode:NSRoundDown
                                                                                              scale:4
                                                                                   raiseOnExactness:NO
                                                                                    raiseOnOverflow:NO
                                                                                   raiseOnUnderflow:NO
                                                                                raiseOnDivideByZero:NO];
    
    buyNumber = [buyNumber decimalNumberByRoundingAccordingToBehavior:behavior];
    sellNumber = [sellNumber decimalNumberByRoundingAccordingToBehavior:behavior];
    highNumber = [highNumber decimalNumberByRoundingAccordingToBehavior:behavior];
    lowNumber = [lowNumber decimalNumberByRoundingAccordingToBehavior:behavior];
    avgNumber = [avgNumber decimalNumberByRoundingAccordingToBehavior:behavior];
    volumeNumber = [volumeNumber decimalNumberByRoundingAccordingToBehavior:behavior];
	
    _buyLabel.text = [NSString stringWithFormat:@"%@ %@",[CESignsOfCurrencies changeShortNameForSign:self.pair[1]], buyNumber];
    _sellLabel.text = [NSString stringWithFormat:@"%@ %@",[CESignsOfCurrencies changeShortNameForSign:self.pair[1]],sellNumber];
    _highLabel.text = [NSString stringWithFormat:@"%@ %@",[CESignsOfCurrencies changeShortNameForSign:self.pair[1]],highNumber];
    _lowLabel.text = [NSString stringWithFormat:@"%@ %@",[CESignsOfCurrencies changeShortNameForSign:self.pair[1]],lowNumber];
    _avgLabel.text = [NSString stringWithFormat:@"%@ %@",[CESignsOfCurrencies changeShortNameForSign:self.pair[1]],avgNumber];
    _volumeLabel.text = [NSString stringWithFormat:@"%@ %@",[CESignsOfCurrencies changeShortNameForSign:self.pair[1]],volumeNumber];
}

- (void)setupGraphButtons
{
    _graphMinuteButton = [UIButton new];
    _graphHourButton = [UIButton new];
    _graphDayButton = [UIButton new];
    
    [self.view addSubview:_graphMinuteButton];
    [self.view addSubview:_graphHourButton];
    [self.view addSubview:_graphDayButton];
    
    _graphMinuteButton.backgroundColor = [UIColor ceMainColor];
    _graphHourButton.backgroundColor = [UIColor ceMainColor];
    _graphDayButton.backgroundColor = [UIColor ceMainColor];
    
    _buttonLineView = [[CEButtonDownLineView alloc]initWithColor:[UIColor ceAdditionalColor]
                                                     andHeight:@(CELineButtonsHeight)];
    [_graphHourButton addSubview:_buttonLineView];
    
    NSDictionary *attrsDictionary = @{
                                      NSForegroundColorAttributeName : [UIColor whiteColor],
                                      NSFontAttributeName : [UIFont ceFontForGraphPeriodButtonsInTradeView]
                                      };
	NSAttributedString *minTitle;
	NSAttributedString *hourTitle;
	NSAttributedString *dayTitle;
	if (self.exmoGraph)
	{
		minTitle = [[NSAttributedString alloc] initWithString:CENameOfMinuteGraphPeriodButton
																	   attributes:attrsDictionary];
		hourTitle = [[NSAttributedString alloc] initWithString:CENameOfHourGraphPeriodButton
																		attributes:attrsDictionary];
		dayTitle = [[NSAttributedString alloc] initWithString:CENameOfDayGraphPeriodButton
																	   attributes:attrsDictionary];
	}
	else
	{
		minTitle = [[NSAttributedString alloc] initWithString:@"Min"
												   attributes:attrsDictionary];
		hourTitle = [[NSAttributedString alloc] initWithString:@"Hour"
													attributes:attrsDictionary];
		dayTitle = [[NSAttributedString alloc] initWithString:@"Day"
												   attributes:attrsDictionary];
	}
    
    [_graphMinuteButton setAttributedTitle:minTitle forState:UIControlStateNormal];
    [_graphHourButton setAttributedTitle:hourTitle forState:UIControlStateNormal];
    [_graphDayButton setAttributedTitle:dayTitle forState:UIControlStateNormal];
    
    [_graphMinuteButton addTarget:self action:@selector(setMinuteGraph) forControlEvents:UIControlEventTouchDown];
    [_graphHourButton addTarget:self action:@selector(setHourGraph) forControlEvents:UIControlEventTouchDown];
    [_graphDayButton addTarget:self action:@selector(setDayGraph) forControlEvents:UIControlEventTouchDown];
}


#pragma mark - CECurrencyViewModelDelegate

- (void)currencyHistoryDidUpdate:(CECurrencyViewModel *)currencyVM withHistory:(NSDictionary *)historyDictionary
{
	if (!self.isExmoGraph)
	{
		((CEGraph *)self.graph).historyData = historyDictionary;
	}
}

- (void)didUpdateCurrencyData:(CECurrencyViewModel *)currencyVM
{
    [self setLabelsValue];
}


@end
