//
//  CECurrencyViewModel.m
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 02.01.2018.
//  Copyright © 2018 Artem Zabludovsky. All rights reserved.
//


#import "CECurrencyViewModel.h"
#import "CEExmoNetwork.h"
#import "CEExmoNetworkProtocol.h"
#import "AppDelegate.h"


static CGFloat const CETimerInterfalForUpdatingData = 5.f;


@interface CECurrencyViewModel()<CEExmoNetworkDelegate, NSFetchedResultsControllerDelegate>

@property (nonatomic, strong) CEExmoNetwork *exmo;
@property (nonatomic, strong) NSPersistentContainer *container;
@property (nonatomic, strong) NSTimer *updateDataTimer;

@end


@implementation CECurrencyViewModel


#pragma mark - Lifecycle

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        _exmo = [[CEExmoNetwork alloc]init];
        _exmo.delegate = self;
        
        _updateDataTimer = [NSTimer scheduledTimerWithTimeInterval:CETimerInterfalForUpdatingData
                                                           repeats:YES
                                                             block:^(NSTimer * _Nonnull timer) {
            [self.delegate didUpdateCurrencyData:self];
        }];
    }
    return self;
}


#pragma mark - Custom Accessors

- (NSPersistentContainer *)container
{
    UIApplication *app = [UIApplication sharedApplication];
    NSPersistentContainer *container = ((AppDelegate *)app.delegate).persistentContainer;
    return container;
}


#pragma mark - Public

- (Currencies *)fetchRequestForCurrencyWithPair:(NSArray *)pair
{
    NSFetchRequest *req = [Currencies fetchRequest];
    req.predicate = [NSPredicate predicateWithFormat:@"firstCurrencyShortName == %@ AND secondCurrencyShortName == %@",pair[0],pair[1]];
    NSArray *matches = [self.container.viewContext executeFetchRequest:req error:nil];
    return [matches firstObject];
}

- (void)prepareToDealloc
{
    [self.updateDataTimer invalidate];
    self.updateDataTimer = nil;
}


#pragma mark - Private

- (void)loadMinuteHistoryForGraph
{
    [self.exmo getCurrencyHistoryForPair:self.pair withParametr:@"minute" withLimit:@60];
}

- (void)loadHourHistoryForGraph
{
    [self.exmo getCurrencyHistoryForPair:self.pair withParametr:@"hour" withLimit:@48];
}

- (void)loadDayHistoryForGraph
{
    [self.exmo getCurrencyHistoryForPair:self.pair withParametr:@"day" withLimit:@30];
}

- (void)currencyHistoryDidLoad:(CEExmoNetwork *)exmo withHistory:(NSDictionary *)historyDictionary forPair:(NSArray *)pair
{
    [self.delegate currencyHistoryDidUpdate:self withHistory:historyDictionary];
}

@end
