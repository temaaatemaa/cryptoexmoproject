//
//  CECurrencyViewController.h
//  CryptoExmo
//
//  Created by Artem Zabludovsky on 02.01.2018.
//  Copyright © 2018 Artem Zabludovsky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CECurrencyViewController : UIViewController

@property (nonatomic, copy) NSArray *pair;

@end
