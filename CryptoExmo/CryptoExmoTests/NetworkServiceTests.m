//
//  NetworkServiceTests.m
//  CryptoExmoTests
//
//  Created by Artem Zabludovsky on 12.01.2018.
//  Copyright © 2018 Artem Zabludovsky. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import <Expecta/Expecta.h>
#import "CENetworkService.h"
#import "CEMarketViewModel.h"

@interface CENetworkService()
@property (nonatomic, strong) NSPersistentContainer *container;
@property (nonatomic, strong) NSURLSession *session;
- (void)downloadIconForCurrency:(Currencies *)currency inContext:(NSManagedObjectContext *)context;

@end

@interface NetworkServiceTests : XCTestCase

@property (nonatomic, strong) CENetworkService *networkService;

@end

@implementation NetworkServiceTests

- (void)setUp
{
    [super setUp];
    _networkService = OCMPartialMock([CENetworkService new]);
}

- (void)tearDown
{

    [super tearDown];
}

- (void)testDownloadIconsForCurrencies
{
    id persistentContinerMock = OCMClassMock([NSPersistentContainer class]);
    OCMStub(self.networkService.container).andReturn(persistentContinerMock);
    id mocMock = OCMClassMock([NSManagedObjectContext class]);
    OCMStub([persistentContinerMock viewContext]).andReturn(mocMock);
    
    OCMStub([self.networkService downloadIconForCurrency:OCMOCK_ANY inContext:OCMOCK_ANY]);
    
    id currencyMock = OCMClassMock([Currencies class]);
    OCMExpect([currencyMock imageURL]).andReturn(@"asd");
    
    OCMStub([mocMock executeFetchRequest:OCMOCK_ANY error:[OCMArg anyObjectRef]]).andReturn(@[currencyMock]);
    
    [self.networkService downloadIconsForCurrencies];
    
    [[NSRunLoop mainRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:1]];
    OCMVerify([self.networkService downloadIconForCurrency:OCMOCK_ANY inContext:OCMOCK_ANY]);
}

- (void)testDownloadIconForCurrency
{
    id delegate = OCMClassMock([CEMarketViewModel class]);
    OCMStub(self.networkService.delegate).andReturn(delegate);
    OCMStub([delegate respondsToSelector:@selector(iconDidDownload:forCurrency:)]).andReturn(YES);
    
    OCMExpect([delegate iconDidDownload:OCMOCK_ANY forCurrency:OCMOCK_ANY]);
    
    id mock = OCMClassMock([NSURLSession class]);
    OCMStub(self.networkService.session).andReturn(mock);
    
    id currencyMock = OCMClassMock([Currencies class]);
    OCMExpect([currencyMock setImageData:OCMOCK_ANY]);
    
    id mocMock = OCMClassMock([NSManagedObjectContext class]);
    OCMStub([mocMock save:[OCMArg anyObjectRef]]);
    
    NSData *data = [OCMArg any];
    NSURLResponse *response = [OCMArg any];
    NSError *error = [OCMArg isNil];
    
    OCMStub([mock dataTaskWithURL:OCMOCK_ANY completionHandler:([OCMArg invokeBlockWithArgs: data,response,error, nil])]);
    
    [self.networkService downloadIconForCurrency:currencyMock inContext:mocMock];
    
    [[NSRunLoop mainRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:1]];
    OCMVerifyAll(delegate);
}


@end
