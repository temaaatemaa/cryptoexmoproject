//
//  CEMarketViewModelTests.m
//  CryptoExmoTests
//
//  Created by Artem Zabludovsky on 08.01.2018.
//  Copyright © 2018 Artem Zabludovsky. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import <Expecta/Expecta.h>
#import "CEMarketViewModel.h"
#import "CEExmoNetwork.h"
#import "CENetworkService.h"
#import "UIColor+CEColor.h"
#import "CEMarketViewController.h"

static NSString *const CECellReuseID = @"CECELLReuseID";

@interface CEMarketViewModel()
- (void)globalCapitalizationDidLoad:(CEExmoNetwork *)exmo withData:(NSData *)data;
- (void)iconDidDownload:(CENetworkService *)CENetworkService forCurrency:(Currencies *)currency;
- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller;
- (void)fetchCurrencyArrayFromCoreData;
- (void)downloadIcons;
- (UIImage *)imageWithData:(NSData *)data forSize:(CGSize)size;
- (CEMarketAndWalletTableViewCell *)setupColorForCurrency:(Currencies *)curr inCell:(CEMarketAndWalletTableViewCell *)cell;
- (CEMarketAndWalletTableViewCell *)prepareCellForIndexPath:(NSIndexPath *)indexPath forTableView:(UITableView *)tableView;
@property (nonatomic, strong) NSPersistentContainer *container;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultController;
@property (nonatomic, copy) NSArray *currenciesArray;

@end
@interface CEMarketViewModelTests : XCTestCase
@property (nonatomic, strong) CEMarketViewModel *marketVM;
@end

@implementation CEMarketViewModelTests

- (void)setUp {
    [super setUp];

    self.marketVM = OCMPartialMock([CEMarketViewModel new]);
}

- (void)tearDown {
    [super tearDown];
}

- (void)testGlobalCapitalizationDidLoadDataIsNil
{
    id delegateMock = OCMClassMock([CEMarketViewController class]);
    OCMStub([self.marketVM delegate]).andReturn(delegateMock);
    
    OCMReject([delegateMock globalCapitalizationDidReceived:self.marketVM withCapitalization:OCMOCK_ANY]);
    
    [self.marketVM globalCapitalizationDidLoad:[OCMArg isNil] withData:nil];
    
    OCMVerifyAll(delegateMock);
}

- (void)testGlobalCapitalizationDidLoadWithNormData
{
    id delegateMock = OCMClassMock([CEMarketViewController class]);
    OCMStub([self.marketVM delegate]).andReturn(delegateMock);
    
    NSDictionary *dict = @{@"total_market_cap_usd" : @123123};
    id jsonser = OCMClassMock([NSJSONSerialization class]);
    OCMStub([jsonser JSONObjectWithData:OCMOCK_ANY options:0 error:nil]).andReturn(dict);
    
    OCMExpect([delegateMock globalCapitalizationDidReceived:OCMOCK_ANY withCapitalization:@123123]);

    [self.marketVM globalCapitalizationDidLoad:nil withData:[OCMArg isNotNil]];
    
    OCMVerifyAll(delegateMock);
}

- (void)testIconDidDownload
{
    [self.marketVM iconDidDownload:nil forCurrency:nil];
    OCMVerify([self.marketVM.delegate marketDataDidReceived:OCMOCK_ANY withCurrencies:OCMOCK_ANY]);
}

- (void)testControllerDidChangeContent
{
    [self.marketVM controllerDidChangeContent:nil];
    OCMStub([self.marketVM fetchCurrencyArrayFromCoreData]);
    
    [[NSRunLoop mainRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.01]];
    
    OCMVerify([self.marketVM fetchCurrencyArrayFromCoreData]);
}

- (void)testFetchCurrencyArrayFromCoreDataCurrImageDataIsNill
{
    OCMStub([self.marketVM globalCapitalizationDidLoad:OCMOCK_ANY withData:OCMOCK_ANY]);
    
    id delegateMock = OCMClassMock([CEMarketViewController class]);
    OCMStub([self.marketVM delegate]).andReturn(delegateMock);
    
    id fetchRCMock = OCMClassMock([NSFetchedResultsController class]);
    OCMStub([self.marketVM fetchedResultController]).andReturn(fetchRCMock);
    
    OCMStub([fetchRCMock performFetch:nil]);
    
    id currencyMock = OCMPartialMock([Currencies new]);
    OCMStub([currencyMock imageData]).andReturn(nil);
    
    OCMStub([fetchRCMock fetchedObjects]).andReturn(@[currencyMock]);
    
    [self.marketVM fetchCurrencyArrayFromCoreData];
    
    OCMVerify([self.marketVM.delegate marketDataDidReceived:OCMOCK_ANY withCurrencies:OCMOCK_ANY]);
    OCMVerify([self.marketVM downloadIcons]);
    
    [delegateMock stopMocking];
}

- (void)testFetchCurrencyArrayFromCoreDataCurrImageDataIsNonNill
{
    OCMStub([self.marketVM globalCapitalizationDidLoad:OCMOCK_ANY withData:OCMOCK_ANY]);
    
    id delegateMock = OCMClassMock([CEMarketViewController class]);
    OCMStub([self.marketVM delegate]).andReturn(delegateMock);
    
    id fetchRCMock = OCMClassMock([NSFetchedResultsController class]);
    OCMStub([self.marketVM fetchedResultController]).andReturn(fetchRCMock);
    
    OCMStub([fetchRCMock performFetch:nil]);

    id currencyMock = OCMPartialMock([Currencies new]);
    OCMStub([currencyMock imageData]).andReturn([OCMArg isNotNil]);

    OCMStub([fetchRCMock fetchedObjects]).andReturn(@[currencyMock]);
    
    [self.marketVM fetchCurrencyArrayFromCoreData];

    OCMVerify([self.marketVM.delegate marketDataDidReceived:OCMOCK_ANY withCurrencies:OCMOCK_ANY]);
    OCMReject([self.marketVM downloadIcons]);
    
    [delegateMock stopMocking];
    
}

- (void)testImageWithNilData
{
    CGSize size = {50,50};
    UIImage *image = [self.marketVM imageWithData:nil forSize:size];
    expect(image).toNot.beNil();
}

- (void)testImageWithData
{
    CGSize size = {50,50};
    UIImage *img = [UIImage imageNamed:@"ruble"];
    CGDataProviderRef provider = CGImageGetDataProvider(img.CGImage);
    NSData *data = (id)CFBridgingRelease(CGDataProviderCopyData(provider));
    
    UIImage *image = [self.marketVM imageWithData:data forSize:size];
    expect(image).toNot.beNil();
}

- (void)testSetupColorCurrencyInCellWhereCurrenciesPersantageGreater0AndPriceIsIncrese
{
    id currencyMock = OCMPartialMock([Currencies new]);
    OCMStub([currencyMock persentage]).andReturn(1);
    OCMStub([currencyMock isPriceIncrease]).andReturn(YES);
    OCMStub([currencyMock isPriceDecrease]).andReturn(NO);

    
    CEMarketAndWalletTableViewCell *cell = [self.marketVM setupColorForCurrency:currencyMock inCell:[CEMarketAndWalletTableViewCell new]];
    expect(cell.percetageLabel.backgroundColor).to.equal([UIColor ceGreenColorForPersentageInMarketAndWalletCell]);
    expect(cell.priceLabel.textColor).to.equal([UIColor ceGreenColorForPriceInMarketAndWalletCell]);
}

- (void)testSetupColorCurrencyInCellWhereCurrenciesPersantageNotGreater0AndPriceIsDecrese
{
    id currencyMock = OCMPartialMock([Currencies new]);
    OCMStub([currencyMock persentage]).andReturn(-1);
    OCMStub([currencyMock isPriceIncrease]).andReturn(NO);
    OCMStub([currencyMock isPriceDecrease]).andReturn(YES);
    
    CEMarketAndWalletTableViewCell *cell = [self.marketVM setupColorForCurrency:currencyMock inCell:[CEMarketAndWalletTableViewCell new]];
    expect(cell.percetageLabel.backgroundColor).to.equal([UIColor ceRedColorInMarketAndWalletCell]);
    expect(cell.priceLabel.textColor).to.equal([UIColor ceRedColorInMarketAndWalletCell]);
}

- (void)testPrepareCell
{
    id currencyMock = OCMPartialMock([Currencies new]);
    OCMStub([currencyMock firstCurrencyName]).andReturn(@"BTC");
    OCMStub([currencyMock firstCurrencyShortName]).andReturn(@"BTC");
    OCMStub([currencyMock secondCurrencyShortName]).andReturn(@"USD");
    OCMStub([currencyMock buyPrice]).andReturn(@"123");
    OCMStub([currencyMock volume24ch]).andReturn((@123).intValue);
    OCMStub([currencyMock persentage]).andReturn(-1.22f);
    OCMStub([currencyMock imageData]).andReturn(nil);
    OCMStub([currencyMock isPriceIncrease]).andReturn(NO);
    OCMStub([currencyMock isPriceDecrease]).andReturn(YES);
    
    id tableMock = OCMClassMock([UITableView class]);
    OCMStub([tableMock dequeueReusableCellWithIdentifier:OCMOCK_ANY]).andReturn([[CEMarketAndWalletTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"123"]);
    
    OCMStub(self.marketVM.currenciesArray).andReturn(@[currencyMock]);
    CGSize size = {20,20};
    OCMStub([self.marketVM imageWithData:OCMOCK_ANY forSize:size]).andReturn([UIImage imageNamed:@"ruble"]);
    
    CEMarketAndWalletTableViewCell *cell = [self.marketVM prepareCellForIndexPath:[NSIndexPath indexPathForRow:1 inSection:0] forTableView:tableMock];
    
    expect(cell.nameLabel.text).to.equal(@"BTCUSD");
    expect(cell.percetageLabel.backgroundColor).to.equal([UIColor ceRedColorInMarketAndWalletCell]);
    expect(cell.priceLabel.textColor).to.equal([UIColor ceRedColorInMarketAndWalletCell]);
}

- (void)testGetFetchedRC
{
    NSFetchedResultsController *fetchRC = [self.marketVM fetchedResultController];
    
    expect(fetchRC).toNot.beNil();
    expect(fetchRC.fetchRequest.sortDescriptors).toNot.beNil();
}

@end
