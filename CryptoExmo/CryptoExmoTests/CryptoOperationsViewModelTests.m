//
//  CryptoOperationsViewModelTests.m
//  CryptoExmoTests
//
//  Created by Artem Zabludovsky on 10.01.2018.
//  Copyright © 2018 Artem Zabludovsky. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import <Expecta.h>
#import "CECryptoOperationsViewModel.h"
#import "CEExmoNetwork.h"
#import "CECryptoOperationsViewController.h"

@interface CECryptoOperationsViewModel()
- (void)setDelegate:(id<CECryptoOperationsViewModelDelegate>)delegate;
- (void)getInfoForMarket;
- (void)setup;
- (void)showAuthorisationController;
- (void)getArrayOfCurrenciesAmount;
- (void)downloadArrayOfUserOpenOrders;
- (void)downloadTradesHistory;
- (void)deleteUserOrder:(NSString *)orderID;
- (void)pairDidChange:(NSArray *)pair;
- (void)createOrderForType:(NSString *)type withQuantity:(NSDecimalNumber *)quantity withPrice:(NSDecimalNumber *)price;
- (void)loadMinuteHistoryForGraph;
- (void)loadHourHistoryForGraph;
- (void)loadDayHistoryForGraph;
- (void)findCurrenciesWhichNonNilInUserBalance:(NSDictionary *)balance;
- (void)findPairAmountInBalanceInUserBalance:(NSDictionary *)balance;
- (NSArray *)sortHistoryOrdersArray:(NSArray *)historyOrdersArray;
- (NSMutableArray *)getOpenOrdersMutableArrayFromOrdersDictionary:(NSDictionary *)openOrdersDictionary;
- (NSArray *)sortOpenOrdersArray:(NSMutableArray *)openOrdersArray;
- (NSArray *)fetchArrayOfCurrenciesWithController:(NSFetchedResultsController *)controller;
- (Currencies *)fetchCurrencyWithController:(NSFetchedResultsController *)controller;
- (void)authControllerDidClose;
- (void)userInfoDidRecived:(CEExmoNetwork *)exmo withData:(NSData *)data;
- (void)openOrdersDidLoad:(CEExmoNetwork *)exmo withOpenOrders:(NSDictionary *)openOrdersDictionary;
- (void)orderDidCreated:(CEExmoNetwork *)exmo withOrderID:(NSNumber *)orderId withResult:(NSString *)result withError:(NSString *)error;
- (void)orderDidDelete:(CEExmoNetwork *)exmo WithResult:(NSString *)result withError:(NSString *)error;
- (void)historyDidLoad:(CEExmoNetwork *)exmo withHistoryArray:(NSArray *)historyArray error:(NSString *)error;
- (void)currencyHistoryDidLoad:(CEExmoNetwork *)exmo withHistory:(NSDictionary *)historyDictionary forPair:(NSArray *)pair;
- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller;
@property (nonatomic, strong) CEExmoNetwork *exmo;
@property (nonatomic, copy) NSArray *ordersHistoryArray;
@property (nonatomic, strong) NSMutableDictionary *dictionaryOfBalance;
@property (nonatomic, copy) NSArray *arrayOfCurrenciesNameWhichBalanceIsNonNull;
@property (nonatomic, strong) NSDecimalNumber *amountOfCryptoCurrency;
@property (nonatomic, strong) NSDecimalNumber *amountOfSecondCurrency;

@end

@interface CryptoOperationsViewModelTests : XCTestCase
@property (nonatomic, strong) CECryptoOperationsViewModel *operationsVM;
@end

@implementation CryptoOperationsViewModelTests

- (void)setUp
{
    [super setUp];
    _operationsVM = OCMPartialMock([CECryptoOperationsViewModel new]);
}

- (void)tearDown
{
    
    [super tearDown];
}

- (void)testSetDelegateApiKeyNonNil
{
    id delegate = [CECryptoOperationsViewController new];
    OCMStub([self.operationsVM setup]);
    OCMStub([delegate currencyDataDidUpdate:OCMOCK_ANY with:OCMOCK_ANY]);
    
    [self.operationsVM setDelegate:delegate];
    
    expect(self.operationsVM.delegate).equal(delegate);
    OCMVerify([self.operationsVM.exmo getInfoForMarket]);
    OCMVerify([self.operationsVM setup]);
}

- (void)testSetupApiKeyNonNil
{
    id userDefaultsMock = OCMClassMock([NSUserDefaults class]);
    OCMStub([userDefaultsMock standardUserDefaults]).andReturn(userDefaultsMock);
    OCMStub([userDefaultsMock objectForKey:@"api_key"]).andReturn([OCMArg isNotNil]);
    OCMStub([self.operationsVM showAuthorisationController]);
    OCMStub([self.operationsVM getArrayOfCurrenciesAmount]);
    OCMStub([self.operationsVM pairDidChange:OCMOCK_ANY]);

    [self.operationsVM setup];

    OCMReject([self.operationsVM showAuthorisationController]);
    OCMVerify([self.operationsVM getArrayOfCurrenciesAmount]);

    [userDefaultsMock stopMocking];
}

- (void)testSetupApiKeyNil
{
    id userDefaultsMock = OCMClassMock([NSUserDefaults class]);
    OCMStub([userDefaultsMock standardUserDefaults]).andReturn(userDefaultsMock);
    OCMStub([userDefaultsMock objectForKey:@"api_key"]).andReturn(nil);
    OCMStub([self.operationsVM showAuthorisationController]);
    OCMStub([self.operationsVM getArrayOfCurrenciesAmount]);
    OCMStub([self.operationsVM pairDidChange:OCMOCK_ANY]);

    [self.operationsVM setup];

    OCMVerify([self.operationsVM showAuthorisationController]);
    OCMReject([self.operationsVM getArrayOfCurrenciesAmount]);
    
    [userDefaultsMock stopMocking];
}

- (void)testShowAuthorisationController
{
    [self.operationsVM showAuthorisationController];
    OCMVerify([self.operationsVM.delegate shouldPresent:OCMOCK_ANY QRCodeViewController:OCMOCK_ANY]);
}

- (void)testDownloadArrayOfUserOpenOrders
{
    [self.operationsVM downloadArrayOfUserOpenOrders];
    OCMVerify([self.operationsVM.exmo getArrayOfUserOpenOrders]);
}

- (void)testDownloadTradesHistory
{
    id exmo = OCMClassMock([CEExmoNetwork class]);
    self.operationsVM.exmo = exmo;
    NSArray *pair = @[
                      @"BTC",
                      @"USD"
                      ];
    OCMStub(self.operationsVM.ordersHistoryArray).andReturn(pair);//count = 2
    OCMStub(self.operationsVM.pair).andReturn(pair);
    
    [[exmo expect] getHistoryForPair:@"BTC_USD" amount:@25 offset:@2];
    
    [self.operationsVM downloadTradesHistory];
    
    [exmo verify];
}

- (void)testDeleteUserOrder
{
    id exmo = OCMClassMock([CEExmoNetwork class]);
    self.operationsVM.exmo = exmo;
    
    NSString *orderID = @"123";

    OCMExpect([exmo deleteUserOrder:orderID]);

    [self.operationsVM deleteUserOrder:orderID];
    
    OCMVerifyAll(exmo);
}

- (void)testPairDidChange
{
    id exmo = OCMClassMock([CEExmoNetwork class]);
    self.operationsVM.exmo = exmo;
    
    NSArray *pair = @[
                      @"BTC",
                      @"USD"
                      ];
    NSDictionary *dict = @{
                           @"USD" : @"123",
                           @"BTC" : @"0.0002"
                           };
    OCMStub(self.operationsVM.dictionaryOfBalance).andReturn(dict);
    OCMExpect([exmo getInfoForMarket]);
    
    [self.operationsVM pairDidChange:pair];
    
    expect(self.operationsVM.pair).equal(pair);
    expect(self.operationsVM.amountOfSecondCurrency).toNot.beNil();
    expect(self.operationsVM.amountOfCryptoCurrency).toNot.beNil();
    
    OCMVerifyAll(exmo);
}

- (void)testCreateOrderForType
{
    id exmo = OCMClassMock([CEExmoNetwork class]);
    self.operationsVM.exmo = exmo;
    
    NSString *type = @"type";
    NSDecimalNumber *quantity = [NSDecimalNumber decimalNumberWithString:@"1.234567"];
    NSDecimalNumber *price = [NSDecimalNumber decimalNumberWithString:@"100"];
    NSArray *pair = @[
                      @"BTC",
                      @"USD"
                      ];
    OCMStub(self.operationsVM.pair).andReturn(pair);
    OCMExpect([exmo createOrderWithPair:pair[0] and:pair[1] withType:type withQuantity:quantity withPrice:price]);
    
    [self.operationsVM createOrderForType:type withQuantity:quantity withPrice:price];
    
    OCMVerifyAll(exmo);
}

- (void)testLoadHistoryForGraph
{
    id exmo = OCMClassMock([CEExmoNetwork class]);
    self.operationsVM.exmo = exmo;
    
    NSArray *pair = @[
                      @"BTC",
                      @"USD"
                      ];
     OCMStub(self.operationsVM.pair).andReturn(pair);
    
    OCMExpect([exmo getCurrencyHistoryForPair:pair withParametr:@"minute" withLimit:@60]);
    OCMExpect([exmo getCurrencyHistoryForPair:pair withParametr:@"hour" withLimit:@48]);
    OCMExpect([exmo getCurrencyHistoryForPair:pair withParametr:@"day" withLimit:@30]);
    
    [self.operationsVM loadMinuteHistoryForGraph];
    [self.operationsVM loadDayHistoryForGraph];
    [self.operationsVM loadHourHistoryForGraph];
    
    OCMVerifyAll(exmo);
}

- (void)testGetArrayOfCurrenciesWhichNonNullApiKeyIsNill
{
    id userDefaultsMock = OCMClassMock([NSUserDefaults class]);
    OCMStub([userDefaultsMock standardUserDefaults]).andReturn(userDefaultsMock);
    OCMStub([userDefaultsMock objectForKey:@"api_key"]).andReturn(nil);
    OCMStub([self.operationsVM.exmo getUserInfo]);
    
    id exmoMock = OCMClassMock([CEExmoNetwork class]);
    OCMStub(self.operationsVM.exmo).andReturn(exmoMock);
    OCMReject([exmoMock getUserInfo]);
    
    [self.operationsVM getArrayOfCurrenciesAmount];
    OCMVerifyAll(exmoMock);
    
    [userDefaultsMock stopMocking];
}

- (void)testGetArrayOfCurrenciesWhichNonNullApiKeyIsNonNill
{
    id userDefaultsMock = OCMClassMock([NSUserDefaults class]);
    OCMStub([userDefaultsMock standardUserDefaults]).andReturn(userDefaultsMock);
    OCMStub([userDefaultsMock objectForKey:@"api_key"]).andReturn(@"123");
    
    id exmoMock = OCMClassMock([CEExmoNetwork class]);
    OCMStub(self.operationsVM.exmo).andReturn(exmoMock);
    OCMExpect([exmoMock getUserInfo]);
    
    [self.operationsVM getArrayOfCurrenciesAmount];
    OCMVerifyAll(exmoMock);
    
    [userDefaultsMock stopMocking];
}

- (void)testFindCurrenciesWhichNonNilInUserBalance
{
    NSDictionary *balance = @{
                              @"balances" : @{
                                      @"USD" : @"5",
                                      @"BTC" : @"10",
                                      @"XXX" : @"0"
                                      }
                              };
    [self.operationsVM findCurrenciesWhichNonNilInUserBalance:balance];
    
    expect(self.operationsVM.arrayOfCurrenciesNameWhichBalanceIsNonNull).equal(@[@"USD",@"BTC"]);
    expect(self.operationsVM.dictionaryOfBalance).equal(@{
                                                          @"USD" : @"5",
                                                          @"BTC" : @"10"
                                                          });
}

- (void)testFindPairAmountInBalance
{
    NSDictionary *balance = @{
                              @"balances" : @{
                                      @"USD" : @"5",
                                      @"BTC" : @"10",
                                      @"XXX" : @"0"
                                      }
                              };
    NSArray *pair = @[
                      @"BTC",
                      @"USD"
                      ];
    OCMStub(self.operationsVM.pair).andReturn(pair);
    self.operationsVM.dictionaryOfBalance = [NSMutableDictionary new];
    
    [self.operationsVM findPairAmountInBalanceInUserBalance:balance];
    
    expect(self.operationsVM.dictionaryOfBalance).equal(@{@"USD" : @"5",
                                                          @"BTC" : @"10",
                                                          @"XXX" : @"0"});
    expect(self.operationsVM.amountOfSecondCurrency).equal([NSDecimalNumber decimalNumberWithString:@"5"]);
    expect(self.operationsVM.amountOfCryptoCurrency).equal([NSDecimalNumber decimalNumberWithString:@"10"]);
}

- (void)testSortHistoryOrdersArray
{
    NSDictionary *firstOrder = @{
                                 @"date" : @1
                                 };
    NSDictionary *secondOrder = @{
                                 @"date" : @2
                                 };
    NSDictionary *thirdOrder = @{
                                  @"date" : @3
                                  };
    NSArray *historyOrdersArray = @[firstOrder,secondOrder,thirdOrder];
    
    NSArray *sortHistoryArray = [self.operationsVM sortHistoryOrdersArray:historyOrdersArray];
    
    expect(sortHistoryArray).equal(@[thirdOrder,secondOrder,firstOrder]);
}

- (void)testGetOpenMutableArrayFromOpenOrdersDictionary
{
    NSDictionary *openOrdersDictionary = @{
                                           @"BTC_USD" : @[@"BTCfirst",@"BTCsecond"],
                                           @"ETH_USD" : @[@"ETHfirst,ETHsecond"]
                                           };
    NSMutableArray *array = [self.operationsVM getOpenOrdersMutableArrayFromOrdersDictionary:openOrdersDictionary];
    
    expect(array).equal(@[@"BTCfirst",@"BTCsecond",@"ETHfirst,ETHsecond"]);
}

- (void)testSortOpenOrdersArray
{
    NSDictionary *firstOrder = @{
                                 @"created" : @1
                                 };
    NSDictionary *secondOrder = @{
                                  @"created" : @2
                                  };
    NSDictionary *thirdOrder = @{
                                 @"created" : @3
                                 };
    NSMutableArray *openOrdersArray = [NSMutableArray arrayWithArray:@[firstOrder,secondOrder,thirdOrder]];
    
    NSArray *sortOpenArray = [self.operationsVM sortOpenOrdersArray:openOrdersArray];
    
    expect(sortOpenArray).equal(@[thirdOrder,secondOrder,firstOrder]);
}

- (void)testFetchArrayOfCurrenciesWithController
{
    id fetchRCMock = OCMClassMock([NSFetchedResultsController class]);
    OCMStub([fetchRCMock fetchRequest]).andReturn(nil);
    OCMStub([fetchRCMock performFetch:[OCMArg anyObjectRef]]).andReturn(1);
    OCMStub([fetchRCMock fetchedObjects]).andReturn(@[@"object"]);
    
    NSArray *fetchedObjects = [self.operationsVM fetchArrayOfCurrenciesWithController:fetchRCMock];
    
    expect(fetchedObjects).equal(@[@"object"]);
}

- (void)testFetchArrayOfCurrenciesWithControllerError
{
    id fetchRCMock = OCMClassMock([NSFetchedResultsController class]);
    OCMStub([fetchRCMock fetchRequest]).andReturn(nil);
    OCMStub([fetchRCMock performFetch:[OCMArg anyObjectRef]]).andReturn(0);
    OCMStub([fetchRCMock fetchedObjects]).andReturn(@[@"object"]);
    
    NSArray *fetchedObjects = [self.operationsVM fetchArrayOfCurrenciesWithController:fetchRCMock];
    
    expect(fetchedObjects).equal(nil);
}

- (void)testFetchCurrencyWithController
{
    id fetchRCMock = OCMClassMock([NSFetchedResultsController class]);
    OCMStub([fetchRCMock fetchRequest]).andReturn(nil);
    OCMStub([fetchRCMock performFetch:[OCMArg anyObjectRef]]).andReturn(1);
    OCMStub([fetchRCMock fetchedObjects]).andReturn(@[[Currencies new]]);
    
    Currencies *currency = [self.operationsVM fetchCurrencyWithController:fetchRCMock];
    
    expect(currency).toNot.beNil();
}

- (void)testFetchCurrencyWithControllerError
{
    id fetchRCMock = OCMClassMock([NSFetchedResultsController class]);
    OCMStub([fetchRCMock fetchRequest]).andReturn(nil);
    OCMStub([fetchRCMock performFetch:[OCMArg anyObjectRef]]).andReturn(0);
    OCMStub([fetchRCMock fetchedObjects]).andReturn(@[[Currencies new]]);
    
    Currencies *currency = [self.operationsVM fetchCurrencyWithController:fetchRCMock];
    
    expect(currency).to.beNil();
}

- (void)testAuthControllerDidClose
{
    [self.operationsVM authControllerDidClose];
    OCMVerify([self.operationsVM getArrayOfCurrenciesAmount]);
}

- (void)testUserInfoDidRecived
{
    id delegate = OCMClassMock([CECryptoOperationsViewController class]);
    OCMStub(self.operationsVM.delegate).andReturn(delegate);
    NSArray *pair = @[
                      @"RUB",
                      @"USD"
                      ];
    OCMStub([self.operationsVM pair]).andReturn(pair);
    
    NSDictionary *dict = @{
                           @"error" : @"123123",
                           @"balances" : @{
                                   @"USD" : @"5",
                                   @"RUB" : @"0"
                                   }
                           };
    id jsonser = OCMClassMock([NSJSONSerialization class]);
    OCMStub([jsonser JSONObjectWithData:OCMOCK_ANY options:0 error:nil]).andReturn(dict);
    
    OCMExpect([delegate userDataDidUpdate:OCMOCK_ANY withBalance:OCMOCK_ANY andCurrencies:OCMOCK_ANY]);

    
    [self.operationsVM userInfoDidRecived:[OCMArg any] withData:[OCMArg isNotNil]];
    
    OCMVerify([self.operationsVM findCurrenciesWhichNonNilInUserBalance:dict]);
    OCMVerify([self.operationsVM findPairAmountInBalanceInUserBalance:dict]);
    expect(self.operationsVM.amountOfSecondCurrency).equal([NSDecimalNumber decimalNumberWithString:@"5"]);
    expect(self.operationsVM.amountOfCryptoCurrency).equal([NSDecimalNumber decimalNumberWithString:@"0"]);
    
    OCMVerifyAll(delegate);
}

- (void)testUserInfoDidRecivedWithNilData
{
    id delegate = OCMClassMock([CECryptoOperationsViewController class]);
    OCMStub(self.operationsVM.delegate).andReturn(delegate);
    
    
    OCMReject([delegate userDataDidUpdate:OCMOCK_ANY withBalance:OCMOCK_ANY andCurrencies:OCMOCK_ANY]);
    
    [self.operationsVM userInfoDidRecived:[OCMArg any] withData:nil];
    
    OCMVerifyAll(delegate);
}

- (void)testOpenOrdersDidLoad
{
    id delegate = OCMClassMock([CECryptoOperationsViewController class]);
    OCMStub(self.operationsVM.delegate).andReturn(delegate);
    
    NSDictionary *openOrdersDictionary = @{
                                           @"error" : @"error"
                                           };
    
    OCMExpect([delegate shouldShowAlert:[OCMArg any] withMessage:@"error"]);
    OCMExpect([delegate openOrdersDidLoad:[OCMArg any] withOpenOrders:OCMOCK_ANY]);
    
    [self.operationsVM openOrdersDidLoad:[OCMArg any] withOpenOrders:openOrdersDictionary];
    
    OCMVerifyAll(delegate);
}

- (void)testOrderDidCreatedWithError
{
    id delegate = OCMClassMock([CECryptoOperationsViewController class]);
    OCMStub(self.operationsVM.delegate).andReturn(delegate);
    
    OCMExpect([delegate notCreateOrder:OCMOCK_ANY withError:@"error"]);
    
    [self.operationsVM orderDidCreated:OCMOCK_ANY withOrderID:OCMOCK_ANY withResult:OCMOCK_ANY withError:@"error"];
    
    OCMVerifyAll(delegate);
}

- (void)testOrderDidCreatedWhitoutError
{
    id delegate = OCMClassMock([CECryptoOperationsViewController class]);
    OCMStub(self.operationsVM.delegate).andReturn(delegate);
    
    OCMStub([self.operationsVM downloadArrayOfUserOpenOrders]);
    
    OCMExpect([delegate orderDidCreated:OCMOCK_ANY withResult:YES]);
    
    [self.operationsVM orderDidCreated:OCMOCK_ANY withOrderID:OCMOCK_ANY withResult:OCMOCK_ANY withError:@""];
    OCMVerify([self.operationsVM downloadArrayOfUserOpenOrders]);
    
    OCMVerifyAll(delegate);
}

- (void)testOrderDidDelete
{
    id delegate = OCMClassMock([CECryptoOperationsViewController class]);
    OCMStub(self.operationsVM.delegate).andReturn(delegate);
    
    OCMExpect([delegate orderDidDelete:OCMOCK_ANY WithResult:@"result" withError:@"error"]);
    
    [self.operationsVM orderDidDelete:OCMOCK_ANY WithResult:@"result" withError:@"error"];
    
    OCMVerifyAll(delegate);
}

- (void)testHistoryDidDelete
{
    id delegate = OCMClassMock([CECryptoOperationsViewController class]);
    OCMStub(self.operationsVM.delegate).andReturn(delegate);
    
    NSArray *historyArr = @[@"historyArr"];
    OCMExpect([delegate historyDidLoad:OCMOCK_ANY withHistoryArray:historyArr error:@"error"]);
    OCMExpect([delegate shouldShowAlert:OCMOCK_ANY withMessage:@"error"]);
    
    [self.operationsVM historyDidLoad:OCMOCK_ANY withHistoryArray:historyArr error:@"error"];
    
    OCMVerifyAll(delegate);
}

- (void)testCurrencyHistoryDidLoad
{
    id delegate = OCMClassMock([CECryptoOperationsViewController class]);
    OCMStub(self.operationsVM.delegate).andReturn(delegate);
    
    OCMExpect([delegate currencyHistoryDidUpdate:OCMOCK_ANY withHistory:OCMOCK_ANY]);
    
    [self.operationsVM currencyHistoryDidLoad:OCMOCK_ANY withHistory:OCMOCK_ANY forPair:OCMOCK_ANY];
    
    OCMVerifyAll(delegate);
}

- (void)testCintrollerDidChancgeContent
{
    id delegate = OCMClassMock([CECryptoOperationsViewController class]);
    OCMStub(self.operationsVM.delegate).andReturn(delegate);
    
    OCMExpect([delegate currencyDataDidUpdate:OCMOCK_ANY with:OCMOCK_ANY]);
    
    id currMock = OCMClassMock([Currencies class]);
    OCMStub([currMock buyPrice]).andReturn(@"123");
    OCMStub([self.operationsVM fetchCurrencyWithController:OCMOCK_ANY]).andReturn(currMock);
    OCMStub([self.operationsVM fetchArrayOfCurrenciesWithController:OCMOCK_ANY]);
    
    [self.operationsVM controllerDidChangeContent:OCMOCK_ANY];
    
    [[NSRunLoop mainRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.01]];

    expect(self.operationsVM.changeCurrency).equal([NSDecimalNumber decimalNumberWithString:@"123"]);
    
    OCMVerifyAll(delegate);
}

- (void)testCustomAccessors
{
    NSDecimalNumber *amountUSD = [self.operationsVM amountOfSecondCurrency];
    NSDecimalNumber *amountCrypto = [self.operationsVM amountOfCryptoCurrency];
    NSDecimalNumber *change = [self.operationsVM changeCurrency];
    
    expect(amountUSD).equal([NSDecimalNumber decimalNumberWithString:@"0.0"]);
    expect(amountCrypto).equal([NSDecimalNumber decimalNumberWithString:@"0.0"]);
    expect(change).equal([NSDecimalNumber decimalNumberWithString:@"0.0"]);
}
@end
