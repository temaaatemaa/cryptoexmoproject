//
//  CurrencyCiewModelTests.m
//  CryptoExmoTests
//
//  Created by Artem Zabludovsky on 09.01.2018.
//  Copyright © 2018 Artem Zabludovsky. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import <Expecta.h>
#import "CECurrencyViewModel.h"
#import "CECurrencyViewController.h"
#import "CEExmoNetwork.h"

@interface CECurrencyViewModel()
@property (nonatomic, strong) CEExmoNetwork *exmo;
@property (nonatomic, strong) NSTimer *updateDataTimer;
@property (nonatomic, strong) NSPersistentContainer *container;

- (void)loadMinuteHistoryForGraph;
- (void)loadHourHistoryForGraph;
- (void)loadDayHistoryForGraph;
- (void)currencyHistoryDidLoad:(CEExmoNetwork *)exmo withHistory:(NSDictionary *)historyDictionary forPair:(NSArray *)pair;
- (Currencies *)fetchRequestForCurrencyWithPair:(NSArray *)pair;

@end

@interface CurrencyViewModelTests : XCTestCase
@property (nonatomic, strong) CECurrencyViewModel *currencyVM;
@end

@implementation CurrencyViewModelTests

- (void)setUp
{
    [super setUp];
    _currencyVM = OCMPartialMock([CECurrencyViewModel new]);
}

- (void)tearDown
{

    [super tearDown];
}

- (void)testInit
{
    id delegateMock = OCMClassMock([CECurrencyViewController class]);
    OCMStub([self.currencyVM delegate]).andReturn(delegateMock);
    OCMExpect([delegateMock didUpdateCurrencyData:OCMOCK_ANY]);
    
    [CECurrencyViewModel new];
    
    [[NSRunLoop mainRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:5.5]];
    OCMVerifyAll(delegateMock);
}

- (void)testLoadMinuteHistoryForGraph
{
    [self.currencyVM loadMinuteHistoryForGraph];
    OCMVerify([self.currencyVM.exmo getCurrencyHistoryForPair:OCMOCK_ANY withParametr:@"minute" withLimit:@60]);
}

- (void)testLoadHourHistoryForGraph
{
    [self.currencyVM loadHourHistoryForGraph];
    OCMVerify([self.currencyVM.exmo getCurrencyHistoryForPair:OCMOCK_ANY withParametr:@"hour" withLimit:@48]);
}

- (void)testLoadDayHistoryForGraph
{
    [self.currencyVM loadDayHistoryForGraph];
    OCMVerify([self.currencyVM.exmo getCurrencyHistoryForPair:OCMOCK_ANY withParametr:@"day" withLimit:@30]);
}

- (void)testCurrencyHistoryDidLoad
{
    NSDictionary *dictionary = [NSDictionary new];
    [self.currencyVM currencyHistoryDidLoad:[OCMArg any] withHistory:dictionary forPair:[NSArray new]];
    OCMVerify([self.currencyVM.delegate currencyHistoryDidUpdate:OCMOCK_ANY withHistory:dictionary]);
}

- (void)testDealloc
{
    [self.currencyVM prepareToDealloc];
    expect(self.currencyVM.updateDataTimer).to.beNil();
}

- (void)testFetchRequest
{
    id persistentContinerMock = OCMClassMock([NSPersistentContainer class]);
    OCMStub(self.currencyVM.container).andReturn(persistentContinerMock);
    id mocMock = OCMClassMock([NSManagedObjectContext class]);
    OCMStub([persistentContinerMock viewContext]).andReturn(mocMock);
    
    OCMStub([mocMock executeFetchRequest:OCMOCK_ANY error:nil]).andReturn(@[[Currencies new]]);
    
    Currencies *currency = [self.currencyVM fetchRequestForCurrencyWithPair:@[@"BTC",@"USD"]];
    
    expect(currency).toNot.beNil();
}

@end
