//
//  WalletViewModelTests.m
//  CryptoExmoTests
//
//  Created by Artem Zabludovsky on 09.01.2018.
//  Copyright © 2018 Artem Zabludovsky. All rights reserved.
//


#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import <Expecta.h>
#import "CEWalletViewModel.h"
#import "CEExmoNetwork.h"
#import "CEWalletTableHeaderViewCell.h"
#import "CEMarketAndWalletTableViewCell.h"
#import "CEWalletViewController.h"


@interface CEWalletViewModel()
- (void)setup;
- (void)showAuthController;
- (void)getArrayOfCorrenciesWhichNonNul;
- (void)getUserInfo;
- (UITableViewCell *)prepareHeaderCellFor:(UITableView *)tableView;
- (CEMarketAndWalletTableViewCell *)prepareRUBCell:(CEMarketAndWalletTableViewCell *)cell;
- (CEMarketAndWalletTableViewCell *)prepareUSDCell:(CEMarketAndWalletTableViewCell *)cell;
- (CEMarketAndWalletTableViewCell *)prepareCell:(CEMarketAndWalletTableViewCell *)cell ForCurrency:(NSString *)currencyName;
- (void)userInfoDidRecived:(CEExmoNetwork *)exmo withData:(NSData *)data;
- (void)authControllerDidClose;
- (CGFloat)getSumOfCurr;
- (UIImage *)imageWithData:(NSData *)data forSize:(CGSize)size;
@property (nonatomic, copy) NSArray *realBalanceArray;
@property (nonatomic, strong) CEExmoNetwork *exmo;
@property (nonatomic, strong) NSMutableDictionary *dictOfValueOfEachCurrencyInDollar;
@property (nonatomic, strong) NSPersistentContainer *container;
@property (nonatomic, strong) NSMutableDictionary *dictOfRealBalance;
@end


@interface WalletViewModelTests : XCTestCase

@property (nonatomic, strong) CEWalletViewModel *walletVM;

@end


@implementation WalletViewModelTests

- (void)setUp
{
    [super setUp];
    _walletVM = OCMPartialMock([CEWalletViewModel new]);
}

- (void)tearDown
{

    [super tearDown];
}


- (void)testSetDelegate//)))
{
    id delegate = [CEWalletViewModel new];
    
    OCMStub([self.walletVM setup]);
    [self.walletVM setDelegate:delegate];
    
    expect(self.walletVM.delegate).equal(delegate);
    OCMVerify([self.walletVM setup]);
}

- (void)testSetupApiKeyNonNil
{
    id userDefaultsMock = OCMClassMock([NSUserDefaults class]);
    OCMStub([userDefaultsMock standardUserDefaults]).andReturn(userDefaultsMock);
    OCMStub([userDefaultsMock objectForKey:@"api_key"]).andReturn(@[@13323]);
    OCMStub([self.walletVM getArrayOfCorrenciesWhichNonNul]);

    [self.walletVM setup];
    OCMVerify([self.walletVM getArrayOfCorrenciesWhichNonNul]);

    expect(self.walletVM.dictOfValueOfEachCurrencyInDollar).toNot.beNil();
    [userDefaultsMock stopMocking];
}

- (void)testSetupApiKeyIsNil
{
    id userDefaultsMock = OCMClassMock([NSUserDefaults class]);
    OCMStub([userDefaultsMock standardUserDefaults]).andReturn(userDefaultsMock);
    OCMStub([userDefaultsMock objectForKey:@"api_key"]).andReturn(nil);
    OCMStub([self.walletVM showAuthController]);

    [self.walletVM setup];
    OCMVerify([self.walletVM showAuthController]);
    expect(self.walletVM.dictOfValueOfEachCurrencyInDollar).toNot.beNil();
    
    [userDefaultsMock stopMocking];
}

- (void)testGetArrayOfCurrenciesWhichNonNullApiKeyIsNill
{
    id userDefaultsMock = OCMClassMock([NSUserDefaults class]);
    OCMStub([userDefaultsMock standardUserDefaults]).andReturn(userDefaultsMock);
    OCMStub([userDefaultsMock objectForKey:@"api_key"]).andReturn(nil);
    OCMStub([self.walletVM.exmo getUserInfo]);

    id exmoMock = OCMClassMock([CEExmoNetwork class]);
    OCMStub(self.walletVM.exmo).andReturn(exmoMock);
    OCMReject([exmoMock getUserInfo]);
    
    [self.walletVM getArrayOfCorrenciesWhichNonNul];
    OCMVerifyAll(exmoMock);
    
    [userDefaultsMock stopMocking];
}

- (void)testGetArrayOfCurrenciesWhichNonNullApiKeyIsNonNill
{
    //todo
    id userDefaultsMock = OCMClassMock([NSUserDefaults class]);
    OCMStub([userDefaultsMock standardUserDefaults]).andReturn(userDefaultsMock);
    OCMStub([userDefaultsMock objectForKey:@"api_key"]).andReturn(@"123");
    
    id exmoMock = OCMClassMock([CEExmoNetwork class]);
    OCMStub(self.walletVM.exmo).andReturn(exmoMock);
    OCMExpect([exmoMock getUserInfo]);

    [self.walletVM getArrayOfCorrenciesWhichNonNul];
    OCMVerifyAll(exmoMock);
    
    [userDefaultsMock stopMocking];
}

- (void)testGetCellForTableViewHeaderCell
{
    UITableView *tableView = OCMPartialMock([UITableView new]);
    OCMStub([tableView dequeueReusableCellWithIdentifier:OCMOCK_ANY]).andReturn([[CEWalletTableHeaderViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"any"]);
    
    CEWalletTableHeaderViewCell *cell = (CEWalletTableHeaderViewCell *)[self.walletVM getCellForTableView:tableView forIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    
    OCMVerify([self.walletVM prepareHeaderCellFor:tableView]);
    expect(cell).toNot.beNil();
    expect(cell.CETitleLable.text).equal(@"Wallet");
}

- (void)testGetCellForTableViewRubCell
{
    UITableView *tableView = OCMPartialMock([UITableView new]);
    OCMStub([tableView dequeueReusableCellWithIdentifier:OCMOCK_ANY]).andReturn([[CEMarketAndWalletTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"any"]);
    OCMStub(self.walletVM.realBalanceArray).andReturn(@[@"RUB"]);
    
    CEMarketAndWalletTableViewCell *cell = (CEMarketAndWalletTableViewCell *)[self.walletVM getCellForTableView:tableView forIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    
    OCMVerify([self.walletVM prepareRUBCell:OCMOCK_ANY]);
    expect(cell).toNot.beNil();
    expect(cell.nameLabel.text).equal(@"Ruble (RUB)");
}

- (void)testGetCellForTableViewUSDCell
{
    UITableView *tableView = OCMPartialMock([UITableView new]);
    OCMStub([tableView dequeueReusableCellWithIdentifier:OCMOCK_ANY]).andReturn([[CEMarketAndWalletTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"any"]);
    OCMStub(self.walletVM.realBalanceArray).andReturn(@[@"USD"]);
    
    CEMarketAndWalletTableViewCell *cell = (CEMarketAndWalletTableViewCell *)[self.walletVM getCellForTableView:tableView forIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    
    OCMVerify([self.walletVM prepareUSDCell:OCMOCK_ANY]);
    expect(cell).toNot.beNil();
    expect(cell.nameLabel.text).equal(@"Dollar (USD)");
}

- (void)testGetCellForTableViewRegularCell
{
    UITableView *tableView = OCMPartialMock([UITableView new]);
    OCMStub([tableView dequeueReusableCellWithIdentifier:OCMOCK_ANY]).andReturn([[CEMarketAndWalletTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"any"]);
    OCMStub(self.walletVM.realBalanceArray).andReturn(@[@"XXX"]);
    
    id containerMock = OCMClassMock([NSPersistentContainer class]);
    OCMStub(self.walletVM.container).andReturn(containerMock);
    id mocMock = OCMClassMock([NSManagedObjectContext class]);
    OCMStub([containerMock viewContext]).andReturn(mocMock);
    
    id currMock = OCMClassMock([Currencies class]);
    OCMStub([mocMock executeFetchRequest:OCMOCK_ANY error:nil]).andReturn(@[currMock]);
    OCMStub([currMock firstCurrencyName]).andReturn(@"Xxxxx (XXX)");
    OCMStub([self.walletVM imageWithData:OCMOCK_ANY forSize:CGSizeMake(50, 50)]);
    
    CEMarketAndWalletTableViewCell *cell = (CEMarketAndWalletTableViewCell *)[self.walletVM getCellForTableView:tableView forIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    
    OCMVerify([self.walletVM prepareCell:OCMOCK_ANY ForCurrency:@"XXX"]);
    expect(cell).toNot.beNil();
    expect(cell.nameLabel.text).equal(@"Xxxxx (XXX)");
}

- (void)testUserInfoDidReceivedWithNilData
{
    id delegateMock = OCMClassMock([CEWalletViewController class]);
    OCMStub([self.walletVM delegate]).andReturn(delegateMock);
    OCMReject([delegateMock dataDidReceived:OCMOCK_ANY withBalance:OCMOCK_ANY withArray:OCMOCK_ANY]);
    
    [self.walletVM userInfoDidRecived:nil withData:nil];
    
    OCMVerifyAll(delegateMock);
}

- (void)testUserInfoDidReceivedWithDataWithError
{
    NSDictionary *dict = @{
                           @"error" : @"123123",
                           @"balances" : @{
                                   @"USD" : @"5",
                                   @"RUB" : @"0"
                                            }
                           };
    id jsonser = OCMClassMock([NSJSONSerialization class]);
    OCMStub([jsonser JSONObjectWithData:OCMOCK_ANY options:0 error:nil]).andReturn(dict);
    
    id delegateMock = OCMClassMock([CEWalletViewController class]);
    OCMStub([self.walletVM delegate]).andReturn(delegateMock);
    OCMExpect([delegateMock dataDidReceived:OCMOCK_ANY withBalance:OCMOCK_ANY withArray:OCMOCK_ANY]);
    OCMExpect([delegateMock shouldPresent:OCMOCK_ANY AlertWithMessage:OCMOCK_ANY]);
    
    [self.walletVM userInfoDidRecived:nil withData:[OCMArg isNotNil]];
    
    OCMVerifyAll(delegateMock);
}


- (void)testShowAuthController
{
    [self.walletVM showAuthController];
    OCMVerify([self.walletVM.delegate shouldPresent:OCMOCK_ANY QRCodeViewController:OCMOCK_ANY]);
}

- (void)testGetSumOfCurr
{
    self.walletVM.dictOfValueOfEachCurrencyInDollar = [[NSMutableDictionary alloc]init];
    NSArray *arrayOfBalance = @[@"RUB",@"USD",@"XXX"];
    NSDictionary *dictOfBalance = @{
                                    @"RUB" : @"3.22",
                                    @"USD" : @"10",
                                    @"XXX" : @"12"
                                    };
    OCMStub(self.walletVM.realBalanceArray).andReturn(arrayOfBalance);
    OCMStub(self.walletVM.dictOfRealBalance).andReturn(dictOfBalance);
    
    CGFloat sum = [self.walletVM getSumOfCurr];
    expect(sum).toNot.beNil();
}

- (void)testImageWithData
{
    CGSize size = {50,50};
    UIImage *img = [UIImage imageNamed:@"ruble"];
    CGDataProviderRef provider = CGImageGetDataProvider(img.CGImage);
    NSData *data = (id)CFBridgingRelease(CGDataProviderCopyData(provider));
    
    UIImage *image = [self.walletVM imageWithData:data forSize:size];
    expect(image).toNot.beNil();
}

- (void)testImageWithNilData
{
    CGSize size = {50,50};
    UIImage *image = [self.walletVM imageWithData:nil forSize:size];
    expect(image).toNot.beNil();
}

- (void)testAuthControllerDidClose
{
    [self.walletVM authControllerDidClose];
    OCMVerify([self.walletVM getArrayOfCorrenciesWhichNonNul]);
}

- (void)testGetNumberRowInTableView
{
    NSArray *arrWithCount2 = @[
                               @1,
                               @2
                               ];
    OCMStub(self.walletVM.realBalanceArray).andReturn(arrWithCount2);
    NSUInteger numberOfRows = [self.walletVM getNumberRowsInTableView:[OCMArg any]];
    
    expect(numberOfRows).to.equal([arrWithCount2 count] + 1);
}

- (void)testNumberOfSectionOInTableViewWithNotEmptyArrayOfRealBalance
{
    id tableMock = OCMClassMock([UITableView class]);
    
    NSArray *nonNillArray = @[
                               @1,
                               @2
                               ];
    OCMStub(self.walletVM.realBalanceArray).andReturn(nonNillArray);
    
    OCMStub([tableMock setBackgroundView:OCMOCK_ANY]);
    OCMStub([tableMock setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine]);
    
    NSUInteger numberOfSections = [self.walletVM numberOfSectionInTableView:tableMock];
    
    expect(numberOfSections).to.equal(1);
}

- (void)testNumberOfSectionOInTableViewWithEmptyArrayOfRealBalance
{
    id tableMock = OCMClassMock([UITableView class]);
    
    NSArray *emptyArray = @[];
    OCMStub(self.walletVM.realBalanceArray).andReturn(emptyArray);
    
    OCMStub([tableMock setBackgroundView:OCMOCK_ANY]);
    OCMStub([tableMock setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine]);
    
    NSUInteger numberOfSections = [self.walletVM numberOfSectionInTableView:tableMock];
    
    expect(numberOfSections).to.equal(1);
}

@end
