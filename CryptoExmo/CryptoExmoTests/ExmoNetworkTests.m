//
//  CEExmoNetworkTests.m
//  CryptoExmoTests
//
//  Created by Artem Zabludovsky on 08.01.2018.
//  Copyright © 2018 Artem Zabludovsky. All rights reserved.
//


#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import <Expecta/Expecta.h>
#import "CEExmoNetwork.h"
#import "CEWalletViewModel.h"
#import "CECryptoOperationsViewModel.h"
#import "CEPrepareDataForCoreData.h"


@interface CEExmoNetwork()


@property NSURLSession *session;
- (int)handleErrorFromDataTask:(NSError *)error withData:(NSData *)data;
- (NSURLRequest *)createPostRequestFor:(NSDictionary *)postDictionary method:(NSString *)methodName;
- (NSString *)getNonce;
- (NSString *)hmacForKey:(NSString *)key andData:(NSString *)data;
- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveData:(NSData *)data;
@end


@interface CEExmoNetworkTests : XCTestCase

@property (nonatomic, strong) CEExmoNetwork *exmo;

@end



@implementation CEExmoNetworkTests

- (void)setUp
{
    [super setUp];    
    self.exmo = OCMPartialMock([CEExmoNetwork new]);
}

- (void)tearDown
{
    [super tearDown];
}


- (void)testGetUserInfoDownloadNullData
{
    id delegate = OCMClassMock([CEWalletViewModel class]);
    OCMStub(self.exmo.delegate).andReturn(delegate);
    OCMStub([self.exmo createPostRequestFor:OCMOCK_ANY method:OCMOCK_ANY]).andReturn(nil);
    OCMReject([delegate userInfoDidRecived:OCMOCK_ANY withData:OCMOCK_ANY]);
    OCMStub([self.exmo handleErrorFromDataTask:OCMOCK_ANY withData:OCMOCK_ANY]).andReturn(0);
    
    id mock = OCMClassMock([NSURLSession class]);
    OCMStub(self.exmo.session).andReturn(mock);
    
    NSData *data = [OCMArg any];
    NSURLResponse *response = [OCMArg any];
    NSError *error = [OCMArg any];
    
    OCMStub([mock dataTaskWithRequest:OCMOCK_ANY completionHandler:([OCMArg invokeBlockWithArgs: data,response,error, nil])]);
    
    [self.exmo getUserInfo];
    
    OCMVerifyAll(delegate);
}

- (void)testGetUserInfoDownloadNonNullData
{
    id delegate = OCMClassMock([CEWalletViewModel class]);
    OCMStub(self.exmo.delegate).andReturn(delegate);
    OCMStub([self.exmo createPostRequestFor:OCMOCK_ANY method:OCMOCK_ANY]).andReturn(nil);
    OCMExpect([delegate userInfoDidRecived:OCMOCK_ANY withData:OCMOCK_ANY]);
    OCMStub([self.exmo handleErrorFromDataTask:OCMOCK_ANY withData:OCMOCK_ANY]).andReturn(1);
    
    id mock = OCMClassMock([NSURLSession class]);
    OCMStub(self.exmo.session).andReturn(mock);
    
    NSData *data = [OCMArg any];
    NSURLResponse *response = [OCMArg any];
    NSError *error = [OCMArg any];
    
    OCMStub([mock dataTaskWithRequest:OCMOCK_ANY completionHandler:([OCMArg invokeBlockWithArgs: data,response,error, nil])]);
    
    [self.exmo getUserInfo];
    
    OCMVerifyAll(delegate);
}

- (void)testGetGlobalCapitalizationNilData
{
    id delegate = OCMClassMock([CEWalletViewModel class]);
    OCMStub(self.exmo.delegate).andReturn(delegate);
    
    OCMStub([self.exmo handleErrorFromDataTask:OCMOCK_ANY withData:OCMOCK_ANY]).andReturn(0);
    
    id mock = OCMClassMock([NSURLSession class]);
    OCMStub(self.exmo.session).andReturn(mock);
    
    NSData *data = [OCMArg any];
    NSURLResponse *response = [OCMArg any];
    NSError *error = [OCMArg any];
    
    OCMStub([mock dataTaskWithRequest:OCMOCK_ANY completionHandler:([OCMArg invokeBlockWithArgs: data,response,error, nil])]);
    
    OCMReject([delegate globalCapitalizationDidLoad:OCMOCK_ANY withData:OCMOCK_ANY]);
    
    [self.exmo getGlobalCapitalization];
    
    OCMVerifyAll(delegate);
}

- (void)testGetGlobalCapitalizationNonNilData
{
    id delegate = OCMClassMock([CEWalletViewModel class]);
    OCMStub(self.exmo.delegate).andReturn(delegate);

    OCMStub([self.exmo handleErrorFromDataTask:OCMOCK_ANY withData:OCMOCK_ANY]).andReturn(1);
    
    id mock = OCMClassMock([NSURLSession class]);
    OCMStub(self.exmo.session).andReturn(mock);
    
    NSData *data = [OCMArg any];
    NSURLResponse *response = [OCMArg any];
    NSError *error = [OCMArg any];
    
    OCMStub([mock dataTaskWithRequest:OCMOCK_ANY completionHandler:([OCMArg invokeBlockWithArgs: data,response,error, nil])]);
    
    OCMExpect([delegate globalCapitalizationDidLoad:OCMOCK_ANY withData:OCMOCK_ANY]);
    
    [self.exmo getGlobalCapitalization];
    
    OCMVerifyAll(delegate);
}

- (void)testCreateOrderWithPairNonNilData
{
    NSString *firstCurrence = @"BTC";
    NSString *secondCurrence = @"USD";
    NSString *type = @"sell";
    NSDecimalNumber *quantity = [NSDecimalNumber decimalNumberWithString:@"10"];
    NSDecimalNumber *price = [NSDecimalNumber decimalNumberWithString:@"100"];
    
    id delegate = OCMClassMock([CEWalletViewModel class]);
    OCMStub(self.exmo.delegate).andReturn(delegate);
    
    OCMStub([self.exmo handleErrorFromDataTask:OCMOCK_ANY withData:OCMOCK_ANY]).andReturn(1);
    
    id mock = OCMClassMock([NSURLSession class]);
    OCMStub(self.exmo.session).andReturn(mock);
    
    NSData *data = [OCMArg any];
    NSURLResponse *response = [OCMArg any];
    NSError *error = [OCMArg any];
    
    OCMStub([mock dataTaskWithRequest:OCMOCK_ANY completionHandler:([OCMArg invokeBlockWithArgs: data,response,error, nil])]);
    
    NSDictionary *dict = @{
                           @"order_id" : @123123,
                           @"result" : @"resultString",
                           @"error" : @"errorString"
                           };
    id jsonser = OCMClassMock([NSJSONSerialization class]);
    OCMStub([jsonser JSONObjectWithData:OCMOCK_ANY options:0 error:nil]).andReturn(dict);
    
    OCMExpect([delegate orderDidCreated:OCMOCK_ANY withOrderID:@123123 withResult:@"resultString" withError:@"errorString"]);
    
    [self.exmo createOrderWithPair:firstCurrence and:secondCurrence withType:type withQuantity:quantity withPrice:price];
    
    OCMVerifyAll(delegate);
}

- (void)testCreateOrderWithPairNilData
{
    NSString *firstCurrence = @"BTC";
    NSString *secondCurrence = @"USD";
    NSString *type = @"sell";
    NSDecimalNumber *quantity = [NSDecimalNumber decimalNumberWithString:@"10"];
    NSDecimalNumber *price = [NSDecimalNumber decimalNumberWithString:@"100"];
    
    id delegate = OCMClassMock([CEWalletViewModel class]);
    OCMStub(self.exmo.delegate).andReturn(delegate);
    
    OCMStub([self.exmo handleErrorFromDataTask:OCMOCK_ANY withData:OCMOCK_ANY]).andReturn(0);
    
    id mock = OCMClassMock([NSURLSession class]);
    OCMStub(self.exmo.session).andReturn(mock);
    
    NSData *data = [OCMArg any];
    NSURLResponse *response = [OCMArg any];
    NSError *error = [OCMArg any];
    
    OCMStub([mock dataTaskWithRequest:OCMOCK_ANY completionHandler:([OCMArg invokeBlockWithArgs: data,response,error, nil])]);
    
    OCMReject([delegate orderDidCreated:OCMOCK_ANY withOrderID:OCMOCK_ANY withResult:OCMOCK_ANY withError:OCMOCK_ANY]);
    
    [self.exmo createOrderWithPair:firstCurrence and:secondCurrence withType:type withQuantity:quantity withPrice:price];
    
    OCMVerifyAll(delegate);
}

- (void)testGetArrayOfUserOpenOrdersNilData
{
    id delegate = OCMClassMock([CEWalletViewModel class]);
    OCMStub(self.exmo.delegate).andReturn(delegate);
    
    OCMStub([self.exmo handleErrorFromDataTask:OCMOCK_ANY withData:OCMOCK_ANY]).andReturn(0);
    
    id mock = OCMClassMock([NSURLSession class]);
    OCMStub(self.exmo.session).andReturn(mock);
    
    NSData *data = [OCMArg any];
    NSURLResponse *response = [OCMArg any];
    NSError *error = [OCMArg any];
    
    OCMStub([mock dataTaskWithRequest:OCMOCK_ANY completionHandler:([OCMArg invokeBlockWithArgs: data,response,error, nil])]);
    
    OCMReject([delegate openOrdersDidLoad:OCMOCK_ANY withOpenOrders:OCMOCK_ANY]);
    
    [self.exmo getArrayOfUserOpenOrders];
    
    OCMVerifyAll(delegate);
}

- (void)testGetArrayOfUserOpenOrdersNonNilData
{
    id delegate = OCMClassMock([CEWalletViewModel class]);
    OCMStub(self.exmo.delegate).andReturn(delegate);
    
    OCMStub([self.exmo handleErrorFromDataTask:OCMOCK_ANY withData:OCMOCK_ANY]).andReturn(1);
    
    id mock = OCMClassMock([NSURLSession class]);
    OCMStub(self.exmo.session).andReturn(mock);
    
    NSData *data = [OCMArg any];
    NSURLResponse *response = [OCMArg any];
    NSError *error = [OCMArg any];
    
    OCMStub([mock dataTaskWithRequest:OCMOCK_ANY completionHandler:([OCMArg invokeBlockWithArgs: data,response,error, nil])]);
    
    NSDictionary *dict = @{
                           @"order_id" : @123123,
                           };
    id jsonser = OCMClassMock([NSJSONSerialization class]);
    OCMStub([jsonser JSONObjectWithData:OCMOCK_ANY options:0 error:nil]).andReturn(dict);
    
    OCMExpect([delegate openOrdersDidLoad:OCMOCK_ANY withOpenOrders:dict]);
    
    [self.exmo getArrayOfUserOpenOrders];
    
    OCMVerifyAll(delegate);
}

- (void)testDeleteUserOrderWithNilData
{
    id delegate = OCMClassMock([CEWalletViewModel class]);
    OCMStub(self.exmo.delegate).andReturn(delegate);
    
    OCMStub([self.exmo handleErrorFromDataTask:OCMOCK_ANY withData:OCMOCK_ANY]).andReturn(0);
    
    id mock = OCMClassMock([NSURLSession class]);
    OCMStub(self.exmo.session).andReturn(mock);
    
    NSData *data = [OCMArg any];
    NSURLResponse *response = [OCMArg any];
    NSError *error = [OCMArg any];
    
    OCMStub([mock dataTaskWithRequest:OCMOCK_ANY completionHandler:([OCMArg invokeBlockWithArgs: data,response,error, nil])]);
    
    OCMReject([delegate orderDidDelete:OCMOCK_ANY WithResult:OCMOCK_ANY withError:OCMOCK_ANY]);
    
    [self.exmo deleteUserOrder:@"123456"];
    
    OCMVerifyAll(delegate);
}

- (void)testDeleteUserOrderNonNilData
{
    id delegate = OCMClassMock([CEWalletViewModel class]);
    OCMStub(self.exmo.delegate).andReturn(delegate);
    
    OCMStub([self.exmo handleErrorFromDataTask:OCMOCK_ANY withData:OCMOCK_ANY]).andReturn(1);
    
    id mock = OCMClassMock([NSURLSession class]);
    OCMStub(self.exmo.session).andReturn(mock);
    
    NSData *data = [OCMArg any];
    NSURLResponse *response = [OCMArg any];
    NSError *error = [OCMArg any];
    
    OCMStub([mock dataTaskWithRequest:OCMOCK_ANY completionHandler:([OCMArg invokeBlockWithArgs: data,response,error, nil])]);
    
    NSDictionary *dict = @{
                           @"result" : @"resultString",
                           @"error" : @"errorString"
                           };
    id jsonser = OCMClassMock([NSJSONSerialization class]);
    OCMStub([jsonser JSONObjectWithData:OCMOCK_ANY options:0 error:nil]).andReturn(dict);
    
    OCMExpect([delegate orderDidDelete:OCMOCK_ANY WithResult:@"resultString" withError:OCMOCK_ANY]);
    
    [self.exmo deleteUserOrder:@"123456"];
    
    OCMVerifyAll(delegate);
}

- (void)testGetHistoryForPairNonNilData
{
    id delegate = OCMClassMock([CEWalletViewModel class]);
    OCMStub(self.exmo.delegate).andReturn(delegate);
    
    OCMStub([self.exmo handleErrorFromDataTask:OCMOCK_ANY withData:OCMOCK_ANY]).andReturn(1);
    
    id mock = OCMClassMock([NSURLSession class]);
    OCMStub(self.exmo.session).andReturn(mock);
    
    NSData *data = [OCMArg any];
    NSURLResponse *response = [OCMArg any];
    NSError *error = [OCMArg any];
    
    OCMStub([mock dataTaskWithRequest:OCMOCK_ANY completionHandler:([OCMArg invokeBlockWithArgs: data,response,error, nil])]);
    
    NSDictionary *dict = @{
                           @"USD" : @[
                                   @1,
                                   @2,
                                   @3],
                           @"error" : @"errorString"
                           };
    id jsonser = OCMClassMock([NSJSONSerialization class]);
    OCMStub([jsonser JSONObjectWithData:OCMOCK_ANY options:0 error:nil]).andReturn(dict);
    
    OCMExpect([delegate historyDidLoad:OCMOCK_ANY withHistoryArray:dict[@"USD"] error:dict[@"error"]]);
    
    [self.exmo getHistoryForPair:@"USD" amount:@56 offset:@10];
    
    OCMVerifyAll(delegate);
}

- (void)testGetHistoryForPairNilData
{
    id delegate = OCMClassMock([CEWalletViewModel class]);
    OCMStub(self.exmo.delegate).andReturn(delegate);
    
    OCMStub([self.exmo handleErrorFromDataTask:OCMOCK_ANY withData:OCMOCK_ANY]).andReturn(0);
    
    id mock = OCMClassMock([NSURLSession class]);
    OCMStub(self.exmo.session).andReturn(mock);
    
    NSData *data = [OCMArg any];
    NSURLResponse *response = [OCMArg any];
    NSError *error = [OCMArg any];
    
    OCMStub([mock dataTaskWithRequest:OCMOCK_ANY completionHandler:([OCMArg invokeBlockWithArgs: data,response,error, nil])]);
    
    OCMReject([delegate historyDidLoad:OCMOCK_ANY withHistoryArray:OCMOCK_ANY error:OCMOCK_ANY]);
    
    [self.exmo getHistoryForPair:@"USD" amount:@10 offset:@1];
    
    OCMVerifyAll(delegate);
}

- (void)testGetCurrencyHistoryForPairNillData
{
    id delegate = OCMClassMock([CEWalletViewModel class]);
    OCMStub(self.exmo.delegate).andReturn(delegate);
    
    OCMStub([self.exmo handleErrorFromDataTask:OCMOCK_ANY withData:OCMOCK_ANY]).andReturn(0);
    
    id mock = OCMClassMock([NSURLSession class]);
    OCMStub(self.exmo.session).andReturn(mock);
    
    NSData *data = [OCMArg any];
    NSURLResponse *response = [OCMArg any];
    NSError *error = [OCMArg any];
    
    OCMStub([mock dataTaskWithRequest:OCMOCK_ANY completionHandler:([OCMArg invokeBlockWithArgs: data,response,error, nil])]);
    
    OCMReject([delegate currencyHistoryDidLoad:OCMOCK_ANY withHistory:OCMOCK_ANY forPair:OCMOCK_ANY]);
    
    NSArray *pair = @[@"USD",@"BTC"];
    
    [self.exmo getCurrencyHistoryForPair:pair withParametr:@"parametr" withLimit:@5];
    
    OCMVerifyAll(delegate);
}

- (void)testGetCurrencyHistoryForPairNonNillData
{
    id delegate = OCMClassMock([CEWalletViewModel class]);
    OCMStub(self.exmo.delegate).andReturn(delegate);
    
    OCMStub([self.exmo handleErrorFromDataTask:OCMOCK_ANY withData:OCMOCK_ANY]).andReturn(1);
    
    id mock = OCMClassMock([NSURLSession class]);
    OCMStub(self.exmo.session).andReturn(mock);
    
    NSData *data = [OCMArg any];
    NSURLResponse *response = [OCMArg any];
    NSError *error = [OCMArg any];
    
    OCMStub([mock dataTaskWithRequest:OCMOCK_ANY completionHandler:([OCMArg invokeBlockWithArgs: data,response,error, nil])]);
    
    NSDictionary *dict = @{
                           @"USD" : @[
                                   @1,
                                   @2,
                                   @3],
                           @"error" : @"errorString"
                           };
    id jsonser = OCMClassMock([NSJSONSerialization class]);
    OCMStub([jsonser JSONObjectWithData:OCMOCK_ANY options:0 error:nil]).andReturn(dict);
    
    NSArray *pair = @[@"USD",@"BTC"];
    
    OCMExpect([delegate currencyHistoryDidLoad:OCMOCK_ANY withHistory:dict forPair:pair]);
    
    [self.exmo getCurrencyHistoryForPair:pair withParametr:@"param" withLimit:@5];
    
    OCMVerifyAll(delegate);
}

- (void)testGetNonceUserDefNoGreaterThenCurrentNonce
{
    id userDefaultsMock = OCMClassMock([NSUserDefaults class]);
    OCMStub([userDefaultsMock standardUserDefaults]).andReturn(userDefaultsMock);
    OCMStub([userDefaultsMock objectForKey:@"nonce"]).andReturn(@123);
    OCMStub([userDefaultsMock setObject:OCMOCK_ANY forKey:OCMOCK_ANY]);
    
    NSString *nonce = [self.exmo getNonce];
    
    expect(nonce).toNot.beNil();
}

- (void)testGetNonceGreaterThenCurrentNonce
{
    id userDefaultsMock = OCMClassMock([NSUserDefaults class]);
    OCMStub([userDefaultsMock standardUserDefaults]).andReturn(userDefaultsMock);
    OCMStub([userDefaultsMock objectForKey:@"nonce"]).andReturn(@55555555555555);
    OCMStub([userDefaultsMock setObject:OCMOCK_ANY forKey:OCMOCK_ANY]);
    
    NSString *nonce = [self.exmo getNonce];
    
    expect(nonce).toNot.beNil();
}

- (void)testHmacForKeyKeyNil
{
    NSString *hmac = [self.exmo hmacForKey:nil andData:@"123"];
    expect(hmac).to.beNil();
}

- (void)testHmacForKeyDataNil
{
    NSString *hmac = [self.exmo hmacForKey:@"123" andData:nil];
    expect(hmac).to.beNil();

}

- (void)testHmacForKey
{
    NSString *hmac = [self.exmo hmacForKey:@"123" andData:@"123"];
    expect(hmac).toNot.beNil();
}

- (void)testCreatePostRequest
{
    OCMStub([self.exmo getNonce]).andReturn(@"123");
    NSURLRequest *request = [self.exmo createPostRequestFor:@{} method:@"method"];
    
    expect(request).toNot.beNil();
}

- (void)testHandleErrorFromDataTaskWithErrorTimeOut
{
    id error = OCMClassMock([NSError class]);
    NSDictionary *userInfo = @{
                               @"NSLocalizedDescription" : @"The request timed out."//@"The Internet connection appears to be offline.";
                               };
    OCMStub([error userInfo]).andReturn(userInfo);
    int returnInt = [self.exmo handleErrorFromDataTask:error withData:[OCMArg any]];
    
    expect(returnInt).equal(0);
}

- (void)testHandleErrorFromDataTaskWithErrorNoInternet
{
    id error = OCMClassMock([NSError class]);
    NSDictionary *userInfo = @{
                               @"NSLocalizedDescription" : @"The Internet connection appears to be offline."
                               };
    OCMStub([error userInfo]).andReturn(userInfo);
    int returnInt = [self.exmo handleErrorFromDataTask:error withData:[OCMArg any]];
    
    expect(returnInt).equal(0);
}

- (void)testHandleErrorFromDataTaskNilData
{
    id error = OCMClassMock([NSError class]);
    NSDictionary *userInfo = @{
                               @"NSLocalizedDescription" : @"okay"
                               };
    OCMStub([error userInfo]).andReturn(userInfo);
    int returnInt = [self.exmo handleErrorFromDataTask:error withData:nil];
    
    expect(returnInt).equal(0);
}

- (void)testHandleErrorFromDataTask
{
    id error = OCMClassMock([NSError class]);
    NSDictionary *userInfo = @{
                               @"NSLocalizedDescription" : @"okay"
                               };
    OCMStub([error userInfo]).andReturn(userInfo);
    int returnInt = [self.exmo handleErrorFromDataTask:error withData:[OCMArg any]];
    
    expect(returnInt).equal(1);
}

- (void)testURLSessionDataTaskWithNilData
{
    id prepareForCoreDataMock = OCMClassMock([CEPrepareDataForCoreData class]);
    OCMStub(self.exmo.prepareForInterfaceBuilder).andReturn(prepareForCoreDataMock);
    
    OCMReject([prepareForCoreDataMock saveInCoreDataWith:OCMOCK_ANY withCurrenciesNameData:OCMOCK_ANY]);
    
    [self.exmo URLSession:[OCMArg any] dataTask:[OCMArg any] didReceiveData:nil];
    
    OCMVerifyAll(prepareForCoreDataMock);
}


@end
